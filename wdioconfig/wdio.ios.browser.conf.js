const { config } = require('./wdio.shared.conf')

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities

;(config.capabilities = [
  {
    // The defaults you need to have in your config
    mobileEmulation: { deviceName: 'Nexus 5' },
    browserName: 'safari',
    platformName: 'iOS',
    maxInstances: 1,
    'appium:deviceName': 'iPhone 8 Plus',
    'appium:platformVersion': '14.4',
    'appium:orientation': 'PORTRAIT',
    'appium:automationName': 'XCUITest',
    'appium:newCommandTimeout': 240,
  },
]),
  (config.services = [
    [
      'appium',
      {
        // For options see
        // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
        args: {
          // For arguments see
          // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
        },
        //command: 'appium',
      },
    ],
  ]),
  (config.port = 4723),
  (config.path = '/wd/hub'),
  (exports.config = config)
