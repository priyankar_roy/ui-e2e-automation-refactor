const { config } = require('./wdio.shared.conf')

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities

;(config.capabilities = [
  // {
  //   // The defaults you need to have in your config
  //   platformName: 'Android',
  //   browserName: 'chrome',
  //   maxInstances: 1,
  //   'appium:deviceName': 'Pixel XL',
  //   'appium:platformVersion': '11',
  //   'appium:orientation': 'PORTRAIT',
  //   'appium:automationName': 'UiAutomator2',
  //   'appium:newCommandTimeout': 240,
  //   'goog:chromeOptions': {
  //     w3c: true,
  //     args: ['--no-first-run'],
  //   },
  // }
  // capabilities:
  {
    // browserName: 'android',
    // browserVersion: '10.0',
    // 'selenoid:options': {
    //   enableVNC: true,
    //   enableVideo: false,
    // },

    browserName: 'android',
    platformName: 'Android',
    app: 'test.apk',
    appActivity: 'cz.bsc.rc.activity.Splash',
    version: '10.0',
    platform: 'ANY',
    // enableVNC: true,
    name: 'this.test.is.launched.by.curl',
    // sessionTimeout: 3000,
  },
]),
  // (config.services = [
  //   [
  //     'appium',
  //     {
  //       // For options see
  //       // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
  //       args: {
  //         // For arguments see
  //         // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
  //       },
  //       //command: 'appium',
  //     },
  //   ],
  // ]),
  (config.port = 4444),
  (config.path = '/wd/hub'),
  (exports.config = config)
