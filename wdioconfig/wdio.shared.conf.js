const path = require('path')
const { hooks } = require('../src/support/hooks')
const fs = require('fs')

require('dotenv').config()

exports.config = {
  //runner: 'local',
  hostname: 'localhost',
  //comment this to run in local
  //hostname: 'selenoid',
  //comment this to run in local
  //
  //specs: ['./src/features/**/S7-W - Edit address.feature'],
  specs: ['./src/features/**/*.*'],
  exclude: ['./src/features/**/Base.feature'],
  logLevel: 'trace',
  outputDir: path.join(__dirname, '/logs'),
  bail: 0,
  baseUrl: '',
  waitforTimeout: 20000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,

  framework: 'cucumber',

  reporters: ['allure', 'spec'],
  reporterOptions: {
    outputDir: 'allure-results',
    disableWebdriverStepsReporting: true,
    disableWebdriverScreenshotsReporting: true,
  },

  // beforeSuite: function (suite) {
  //   console.log('I am in before test', suite.name)
  //   // global.allure = allure
  //   // allure.addFeature(suite.name)
  //   // allure.addDescription('generating Allure reports ' + suite.name)
  // },

  // beforeTest: function (test) {
  //   console.log('I am in before test')
  //   // allure.addEnvironment('BROWSER', browser.capabilities.browserName)
  //   // allure.addEnvironment('BROWSER_VERSION', browser.capabilities.version)
  //   // allure.addEnvironment('PLATFORM', browser.capabilities.platform)
  //   // allure.addDescription('generating Allure reports' + test.title)
  //   // allure.addLabel(
  //   //   'label' + +today.toISOString().replace(/[^\w]/g, '') + '.png'
  //   // )
  // },
  // before: function () {
  //   console.log('****I AM IN BEFOTE METHOD****')
  //   rm()
  //   function rm() {
  //     const directory = `${process.cwd()}/allure-results`
  //     //console.log(directory)
  //     fs.readdir(directory, (err, files) => {
  //       if (err) throw err
  //       for (const file of files) {
  //         if (file == '.gitkeep') {
  //         } else {
  //           fs.unlink(path.join(directory, file), (err) => {
  //             if (err) throw err
  //           })
  //         }
  //       }
  //     })
  //   }
  // },

  afterStep: function (uri, step, stepData, test, context) {
    const dir = `screenshots/${uri.step.scenario.name}`
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir)
    }
    browser.saveScreenshot(`${dir}/${uri.step.step.text}.png`)
  },

  cucumberOpts: {
    backtrace: true,
    requireModule: ['@babel/register'],
    failAmbiguousDefinitions: true,
    failFast: false,
    ignoreUndefinedDefinitions: false,
    name: [],
    snippets: true,
    source: true,
    profile: [],
    require: [
      './src/steps/cucumber/given.js',
      './src/steps/cucumber/when.js',
      './src/steps/cucumber/then.js',
      './src/steps/*.js',
    ],
    snippetSyntax: undefined,
    strict: true,
    // <string> (expression) only execute the features or scenarios with
    // tags matching the expression, see
    // https://docs.cucumber.io/tag-expressions/
     tagExpression: '@warehouse-cancellations',
    // <boolean> add cucumber tags to feature or scenario name
    tagsInTitle: false,
    timeout: 8 * 200000,
  },
  ...hooks,
}
