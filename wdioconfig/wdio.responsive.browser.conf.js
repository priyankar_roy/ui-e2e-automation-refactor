const { config } = require('./wdio.shared.conf')
require('dotenv').config()

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
;(config.capabilities = [
  {
    maxInstances: 10,
    browserName: 'chrome',
    'goog:chromeOptions': {
      args: [
        '--disable-gpu',
        '--no-sandbox',
        //'--headless',
        '--window-size=768,800',
      ],
    },
    'selenoid:options': {
      enableVNC: true,
      enableVideo: true,
    },
  },
  {
    maxInstances: 10,
    browserName: 'chrome',
    'goog:chromeOptions': {
      args: [
        '--disable-gpu',
        '--no-sandbox',
        //'--headless',
        '--window-size=320,800',
      ],
    },
    'selenoid:options': {
      enableVNC: true,
      enableVideo: false,
    },
  },
]),
  //(config.hostname = 'marketplace-selenoid.modanisatest.com'), // selenoid aws instance
  (config.port = 4444),
  (config.path = '/wd/hub'),
  (exports.config = config)
