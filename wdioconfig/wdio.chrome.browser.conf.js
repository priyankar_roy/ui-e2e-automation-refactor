const { config } = require('./wdio.shared.conf')
require('dotenv').config()
;(config.capabilities = [
  {
    browserName: 'chrome',
    'goog:chromeOptions': {
      args: [
        '--disable-gpu',
        '--no-sandbox',
        '--window-size=1920,1080',
        '--disable-notifications',
      ],
    },
    'selenoid:options': {
      enableVNC: true,
      enableVideo: false,
      sessionTimeout: '5m',
    },
  },
]),
  (config.hostname = 'marketplace-selenoid.modanisatest.com'), // selenoid gcp instance
  (config.port = 4444),
  (config.path = '/wd/hub'),
  (exports.config = config)
