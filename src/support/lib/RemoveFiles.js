const fs = require('fs')
const path = require('path')

function removingFiles() {
  const directory = `${process.cwd()}/allure-results`
  fs.readdir(directory, (err, files) => {
    if (err) throw err
    for (const file of files) {
      if (file == '.gitkeep') {
      } else {
        fs.unlink(path.join(directory, file), (err) => {
          if (err) throw err
        })
      }
    }
  })
}

module.exports = removingFiles()
