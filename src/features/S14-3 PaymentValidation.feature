Feature: Select payment method

    As Esen
    I want to see my incorrect inputs while adding credit/debit card information
    So That I can see and correct my credit/debit card information.

    @S14-3 @payment-mf
    Scenario: Esen proceeds to make order without filling mandatory fields
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "vojaswwin.ap@thoughtworks.com" and "Modanisa321"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen should see error_messages for fields in payment page
            | cardFields     | errorMessages          |
            | cardHolderName | This field is required |
            | cardNumber     | This field is required |
            | expiryMonth    | This field is required |
            | expiryYear     | This field is required |
            | cvvNumber      | This field is required |

    @S14-3 @payment-mf
    Scenario: Esen should see different error messages for different errors
        Then Esen enters cardNumber "4111 1123 4567 8904" and cvvNumber "1234"
        When Esen places order
        Then Esen should see error_messages for fields in payment page
            | cardFields     | errorMessages            |
            | cardHolderName | This field is required   |
            | cardNumber     | Card number is incorrect |
            | expiryMonth    | This field is required   |
            | expiryYear     | This field is required   |
            | cvvNumber      | CVV is incorrect         |

    @S14-3 @payment-mf
    Scenario Outline:  Esen should see the non-numeric characters removed while they are entered in the card Number field
        When Esen adds "<cardNumber>" as creditcard number she should see "<corrected Value>"
        Examples:
            | cardNumber              | corrected Value     |
            | 5890 0400 0000 0016     | 5890 0400 0000 0016 |
            | 5890040000000016        | 5890 0400 0000 0016 |
            | abcds%^&*&ertyu         | **** **** **** **** |
            | abc 0400 0000 0016 abcd | 0400 0000 0016      |
            | 58900400000000169999    | 5890 0400 0000 0016 |

    @S14-3 @payment-mf
    Scenario Outline:  Esen should see the non-numeric characters removed while they are entered in the cvv Number field
        When Esen adds "<cvvNumber>" as CVV she should see "<corrected Value>"
        Examples:
            | cvvNumber        | corrected Value |
            | abcdefghsd       | •••             |
            | 5890040000000016 | 5890            |
            | 103              | 103             |









