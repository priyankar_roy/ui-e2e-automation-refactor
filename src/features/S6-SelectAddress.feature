Feature: Manage Delivery Adress

    As Esen
    I want to view pre-added addresses and my latest used address should be pre-selected
    So That I can save time of adding a new address and instead pick from the already added one

    @S6 @address-mf
    Scenario: Esen fills the card Details with the correct values and proceeds to the OrderSummary Page
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen click "Login" from home page
        Given Esen add "vojaswwin.ap@thoughtworks.com" and "Modanisa321"
        Given Esen change shipping country as "Australia"
        Given Esen add "MpProduct1" in the search field
        Given Esen removes all address from the account
        Given Esen is in homepage
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Given Esen click add new address button
        Given Esen add the address in address form for "Login"

    @S6 @address-mf
    Scenario: Esen fills the address form for the first time and sees it selected
        Given Esen is in homepage
        Given Esen change shipping country as "Turkey"
        Given Esen removes all address from the account
        Given Esen is in homepage
        When Esen proceeds checkout
        When Esen clicks add new address button
        When Esen fills all required fields
        Then Esen enters the address title as "TurkeyAddress1"
        When Esen clicks on save and continue button
        Then Esen should see the latest address being selected with title as "TurkeyAddress1"
        Then Esen "should not see" change address button


    @S6-1 @address-mf
    Scenario: Esen fills another address, Esen should see change address button
        When Esen clicks add new address button
        When Esen fills all required fields
        Then Esen enters the address title as "TurkeyAddress2"
        When Esen clicks on save and continue button
        Then Esen "should see" change address button
        Then Esen should see the latest address being selected with title as "TurkeyAddress2"


    @S6-1 @address-mf
    Scenario: Esen should see all and only  Turkish sites available to be changed
        When Esen clicks change address button
        Then Esen should see address with titles
            | TurkeyAddress1 |
            | TurkeyAddress2 |
        Then Esen selects address with title "TurkeyAddress1"
        Then Esen clicks "save" option in address form
        Then Esen should see the latest address being selected with title as "TurkeyAddress1"
        Given Esen is in homepage
        When Esen proceeds checkout
        When Esen clicks change address button
        Then Esen selects address with title "TurkeyAddress2"
        Then Esen clicks "save" option in address form
        Then Esen should see the latest address being selected with title as "TurkeyAddress2"


    @S6-1 @address-mf
    Scenario: Esen clicks close option
        When Esen clicks change address button
        Then Esen selects address with title "TurkeyAddress1"
        Then Esen clicks "close" option in address form
        Then Esen should see the latest address being selected with title as "TurkeyAddress2"


    @S6-1 @address-mf
    Scenario: Esen should see edit option for all adresses
        Then Esen should see edit button for selected addresses
        When Esen clicks change address button
        Then Esen should see edit button for all addresses







