Feature: Missing Service Integration - cancellation of order/order items by supplier

    As a Esen
    I want to See the supplier canceled Items in my Orders
    So that  I do not Expect their delivery

    #need to change the cancel api details

    @S80-01
    Scenario: Full cancelation of the order

        Given Esen opens the site for "qa" environment on "desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "vojaswwin.ap@thoughtworks.com" and "Modanisa321"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | Muni Muni |
            | Muni Muni |
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen should order success page with payment summary details
        Then Tolga gets the order Id
        Then Tolga cancels "2" product ids
        Then Esen should see the Order "Order cancelled." in my Orders page

    @S80-02
    Scenario: Partial cancelation of the order

        Given Esen opens the site for "qa" environment on "desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | Muni Muni |
            | Muni Muni |
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen should order success page with payment summary details
        Then Tolga gets the order Id
        Then Tolga cancels "1" product ids
        Then Esen should see the Order "Dispatching" in my Orders page
        Then Esen should see "1" products in my Orders
        










