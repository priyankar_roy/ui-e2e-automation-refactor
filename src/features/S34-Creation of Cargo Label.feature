Feature: Creation of cargo label errors

    As Ömer
    I want to Create cargo label for orders where automatic cargo label creation failed by correcting order information
    So That the order can be processed

    @cargoError
    Scenario: Identify order with cargo label error - 1
        Given Omer has incomplete order to fullfill with id "62351396"
        Given Omer opens the site for "depo yonetim test" environment on "Desktop"
        Given Omer add login details "test-yonetici" and "test98"
        When Omer click "cargo error" from "Monitor" menu
        Then Omer views orders with cargo errors
        Then Omer click error link from cargo error table
        Then Omer should be able to see the failed marketplace "62351396" in the list of orders
        Then Omer logouts

    @cargoError
    Scenario: Re-trigger cargo label creation - done by Cron Job - 3
        Given Omer has incomplete order to fullfill with id "62351396"
        Given Omer opens the site for "depo yonetim test" environment on "Desktop"
        Given Omer add login details "test-yonetici" and "test98"
        When Omer click "cargo error" from "Monitor" menu
        When Omer views orders with cargo errors
        When Omer click error link from cargo error table
        When Omer should be able to see the failed marketplace "62351396" in the list of orders
        When Omer edits "62351396" in existing panel and saves them even it is incomplete
        When Omer has corrected order details for an order which failed cargo label creation
        When Omer click "cargo error" from "Monitor" menu
        When Omer views orders with cargo errors
        When Omer click error link from cargo error table
        Then Omer is able to see new cargo error for order "62351396"
        Then Omer logouts

    @cargoError
    Scenario: Correct order information - 2
        Given Omer has incomplete order to fullfill with id "62351396"
        Given Omer opens the site for "depo yonetim test" environment on "Desktop"
        Given Omer add login details "test-yonetici" and "test98"
        When Omer click "cargo error" from "Monitor" menu
        When Omer views orders with cargo errors
        When Omer click error link from cargo error table
        When Omer should be able to see the failed marketplace "62351396" in the list of orders
        When Omer edits "62351396" in existing panel and saves them with complete
        Then Omer should not be able to order on the list if cargo label creation is done successfully
        Then Omer logouts

    @cargoError
    Scenario: Follow Success cases - 4
        Given Omer has incomplete order to fullfill with id "62351396"
        Given Omer opens the site for "depo yonetim test" environment on "Desktop"
        Given Omer add login details "test-yonetici" and "test98"
        When Omer click "cargo error" from "Monitor" menu
        When Omer views orders with cargo errors
        When Omer click error link from cargo error table
        When Omer should be able to see the failed marketplace "62351396" in the list of orders
        When Omer edits "62351396" in existing panel and saves them with complete
        When Omer proceeds to order cargo page related to order "62351396"
        Then Omer should be able to see the cargo label information is placed to order details page.
        Then Omer logouts