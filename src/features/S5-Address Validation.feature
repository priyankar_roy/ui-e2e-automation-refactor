Feature: Manage Delivery Address

     As Esen
     I want to add correct shipping address
     So That I can receive delivery of the package at that address

     @S5-1 @address-mf
     Scenario: Esen encounters incomplete address error message for all blank adress fields
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Turkey"
          Given Esen click "Login" from home page
          Given Esen add "noaddress@modanisa.com" and "Modanisa123"
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen clicks on save and continue button
          Then Esen encounters incomplete address error message for "UK"

     #This for Turkey
     #@S5-2
     # Scenario: Esen encounters incomplete address error message for each blank adress fields
     # Given Esen is in homepage
     # Given Esen change shipping country as "Turkey"
     # When Esen proceeds checkout
     # When Esen clicks add new address button
     # When Esen clicks on save and continue button
     # Then Esen encounters incomplete address error message for "TR"

     @S5-3 @address-mf
     Scenario: Esen encounters incomplete address error message for each blank adress fields
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen clicks on an address fields and presses tab
          Then Esen encounters incomplete address error message for "UK"

     @S5-4 @address-mf
     Scenario: Esen sees validation in mandatory fields
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen clicks on and enters to check for regex
               | phoneNumber | 22223332         |
               | email       | test@c.c.test^   |
               | address     | test address &^& |
          Then Esen encounters address regex "<message>"
               | Only Latin characters are allowed due to restrictions of global delivery. |
               | Please enter a valid mobile number.                                       |
               | Please enter a valid email address.                                       |


     @S5-5 @address-mf
     Scenario: Esen encounters proper message for incorrect mobile number and email address entered
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen enters minimum string length to respective fields
               | name    | 1  |
               | surname | 1  |
               | address | 11 |
          Then Esen encounters warning message for minimum length "<message>"
               | Name must be at least 2 characters long.     |
               | Surname must be at least 2 characters long.  |
               | Address must be at least 12 characters long. |

     @S5-6 @address-mf
     Scenario: Esen checks for address field regex expressions
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen enters address string in address text box
               | test: Address 09/90; Street-1st, Club: Test Landmark (test)            |                                                                           |
               | test wrong: Address^ 09%90; $Street-1st, ~Club: Test & Landmark (test) | Only Latin characters are allowed due to restrictions of global delivery. |

     @S5-7 @address-mf
     Scenario: Esen encounters proper message for incorrect mobile number and email address entered
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen enters maximum string length to respective fields
               | address | address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address addresss |
               | name    | test firstname check for 75 characters test firstname check for 75 alphabett                                                                                                                                                                                     |
               | surname | test surname check for 75 characters test firstname check for 75 alphabetttt                                                                                                                                                                                     |
          Then Esen verifies the maximum string entered
               | address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address |
               | test firstname check for 75 characters test firstname check for 75 alphabet                                                                                                                                                                                     |
               | test surname check for 75 characters test firstname check for 75 alphabettt                                                                                                                                                                                     |




