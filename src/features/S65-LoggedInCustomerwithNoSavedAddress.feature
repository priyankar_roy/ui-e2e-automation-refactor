Feature: Logged In Customer with No Saved Address

        @S65 @address-mf
        Scenario: Existing proceed
                Given Esen opens the site for "qa" environment on "Desktop"
                Given Esen change shipping country as "Turkey"
                Given Esen click "Login" from home page
                Given Esen add "supplier-test@modanisa.com" and "Modanisa123"
                Given Esen removes all products from the basket
                Given Esen removes all address from the account
                Given Esen add "ModanisaProduct1" in the search field
                When Esen proceeds checkout
                When Esen clicks Place order button
                Then Esen should see warning message in old address page

        @S65 @address-mf
        Scenario: Redirection to Adding address form- New checkout
                Given Esen is in homepage
                Given Esen removes all products from the basket
                Given Esen removes all address from the account
                Given Esen add "MpProduct1" in the search field
                When Esen proceeds checkout
                Then address form should be open
                When Esen fills all required fields
                When Esen clicks on save and continue button
                Then Esen should be navigated to "new" checkout page

        @S65 @address-mf
        Scenario: Closing Adding address form - close button
                Given Esen is in homepage
                Given Esen removes all products from the basket
                Given Esen removes all address from the account
                Given Esen add "MpProduct1" in the search field
                When Esen proceeds checkout
                Then address form should be open
                When Esen clicks on close button
                Then Esen should be navigated to "new" checkout page

        @S65 @address-mf
        Scenario: Closing Adding address form - cross icon button
                Given Esen is in homepage
                Given Esen removes all products from the basket
                Given Esen removes all address from the account
                Given Esen add "MpProduct1" in the search field
                When Esen proceeds checkout
                Then address form should be open
                When Esen clicks on close form
                Then Esen should be navigated to "new" checkout page