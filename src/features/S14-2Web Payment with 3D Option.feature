Feature: Payment with 3D option

    As Esen
    I want to pay by credit/debit with 3D option in the checkout process
    So That I can pay in a secured environment

    @142 @payment-mf
    Scenario Outline: Proceed with Secure Payment
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        Then Esen selects 3D secure payment checkbox
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen will be navigated to 3D secure page and enter "correct" OTP
        Then Esen clicks "Submit" button
        Then Esen able to see order "successfull" page
        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @142 @payment-mf
    Scenario Outline: Opening 3D page and click on cancel button
        Given Esen is in homepage
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        Then Esen selects 3D secure payment checkbox
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen clicks "Cancel" button
        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @142 @payment-mf
    Scenario Outline: Opening 3D page and click on submit without entering OTP
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        Then Esen selects 3D secure payment checkbox
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen clicks "Submit" button
        Then Esen sees error message in payment page
        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @142 @payment-mf
    Scenario Outline: Opening 3D page and entering incorrect OTP number.
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        Then Esen selects 3D secure payment checkbox
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen will be navigated to 3D secure page and enter "incorrect" OTP
        Then Esen clicks "Submit" button
        Then Esen sees error message in payment page
        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @142 @payment-mf
    Scenario: Esen enters details of invalid cards and finds appropriate error messages
        Given Esen is in homepage
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        Then Esen selects 3D secure payment checkbox
        Then Esen fills Card details with card Number and enable 3D payment click on "Submit" with "correct" OTP option and sees the appropriate Messages
            | cardNumber          | messages                                                                 |
            | 4111 1111 1111 1129 | Insufficient fund. Please try with another card.                         |
            | 4124 1111 1111 1116 | Invalid card details. Please check information you entered.              |
            | 4130 1111 1111 1118 | We could not fulfil the payment at the moment. Please try again later.   |
            | 4125 1111 1111 1115 | Your card is expired. Please try with another card or contact your bank. |
            | 4121 1111 1111 1119 | Payment failed. Please try with another card or contact your bank.   |
            | 4130 1111 1111 1118 | We could not fulfil the payment at the moment. Please try again later.   |


