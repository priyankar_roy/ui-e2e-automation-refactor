Feature: Order Confirmation

     As Esen
     I want to see the exact order details of the successful orders on the “order success” page
     So That I know that I will receive the orders

     @S69-01
     Scenario: Verify cargo label details, estimated delivery date, payment and address details in order success page
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Turkey"
          Given Esen click "Login" from home page
          Given Esen add "priyankar.roy@thoughtworks.com" and "priyankar.roy"
          Given Esen removes all products from the basket
          Given Esen add products in the search field
               | Muni Muni |
               | Muni Muni |
          Given Esen click active basket button
          When Esen clicks secure payment button
          When Esen should be navigated to "new" checkout page
          When Esen clicks add new address button
          When Esen fills all required fields
          When Esen clicks on save and continue button
          When Esen click "Continue to payment" button
          When Esen click "Terms and Condition" button
          When Esen click "Place order" button
          Then Esen should see "<message>" in order success page
               | Thank you for your order!                                                |
               | Your order will be shipped to your shipping address as soon as possible. |
               | You can see your order details on My Orders page.                        |
          Then Esen should order success page with payment summary details
          Then Esen should order success page with payment method details
          Then Esen should order success page with shipping address details
          Then Esen should see order estimated delivery
               | Standard Delivery |
          Then Esen should see product group splits as to 1



     @S69-02
     Scenario: Verify cargo label details, estimated delivery date, payment and address details in order success page
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Turkey"
          Given Esen click "Login" from home page
          Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
          Given Esen removes all products from the basket
          Given Esen add "Muni Muni" in the search field
          Given Esen click active basket button
          When Esen clicks secure payment button
          When Esen should be navigated to "new" checkout page
          When Esen clicks add new address button
          When Esen fills all required fields
          When Esen clicks on save and continue button
          When Esen click "Continue to payment" button
          When Esen click "Terms and Condition" button
          When Esen click "Place order" button
          Then Esen should see "<message>" in order success page
               | Thank you for your order!                                                |
               | Your order will be shipped to your shipping address as soon as possible. |
               | You can see your order details on My Orders page.                        |
          Then Esen should see appropirate pages when clicking "<links>" in order success page
               | My Orders          |
               | Countinue shopping |