Feature: Basic flow for an Esen

    @refactor @Base
    Scenario: Esen as a guest to complete the order process
        Given Esen opens the site for "Prod" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen add "Refka Tunic" in the search field
        #Given Esen add "Lc Waikiki Blouses" in the search field
        Given Esen proceeds checkout
        Given Esen click "Continue As Guest" from login page
        Given Esen add the address in address form for "Guest"
        When Esen completes the payment process
        Then Esen clicks terms and condition checkbox
        Then Esen clicks Place order button
        Then Esen should see the order confirmation page

    @registerFlow
    Scenario: Esen able to register in modanisa website
        Given 1Esen opens the site for "Prod" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Sign Up" from home page
        When Esen add registration details for sign up account
        Then Esen sees home page of modanisa
        Given Esen add "Refka Tunic" in the search field
        Given Esen proceeds checkout
        Given Esen click add new address button
        Given Esen add the address in address form for "Login"
        When Esen completes the payment process
        Then Esen clicks terms and condition checkbox
        Then Esen clicks Place order button
        Then Esen should see the order confirmation page

    @testforgermany
    Scenario: Esen able to change browser location
        Given Esen set browser location as "Germany"
        Given Esen opens the site for "Prod" environment on "Desktop"
        Given Esen click on the welcome popup
        Given Esen click "Sign Up" from home page

    @orderpage
    Scenario: Esen able to see orders page
        Given Esen opens the site for "Prod" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        #Given Esen click on the welcome popup
        Given Esen click "Login" from home page
        Given Esen add "melektural@hotmail.com" and "Melek123"
        Given Esen click on the login button
        When Esen click "Orders" link from my account page
        Then Esen able to see "Orders" page
        Then Esen able to see orders information


