Feature: Tracking of shipment status

    As Esen
    I want to know if my order is shipped by MP Supplier
    So That I can know when I can expect the package

    @Tracking
    Scenario: 1 - Track orders which are placed in old Checkout on desktop
        Given that Esen is a logged in customer on "MobileWeb"
        #Given that Esen’s status of the order in the my order page is "Dispatching"
        Given Esen has an "Modanisa" order shipped to address inside "Turkey" and its status is "Dispatching"
        When Cargo company had collected Esen’s orders from "Modanisa"
        When Esen goes to my order page
        Then Esen should be able to see the order status "Shipped" for "Modanisa" shipped order
        Then Esen should be able to see tracking id for her order

    # @TrackingT
    # Scenario: 2 - Cargo company has not collected the order
    #     Given that Esen is a logged in customer
    #     Given that Esen’s status of the order in the my order page is "Dispatching"
    #     Given Esen has an "marketplace" enabled "CIVIL" order shipped to address inside Turkey
    #     When the Cargo company has NOT collected Esen’s orders from CIVIL Supplier
    #     Then Esen should NOT be able to see tracking id “678676578" for the order in the "My order" page
    #     Then Esen should NOT be able to see the order status "Shipped" for marketplace enabled CIVIL supplier order

    # @TrackingT
    # Scenario: 3 - Track orders which are placed in new Checkout
    #     Given that Esen is a logged in customer
    #     Given that Esen’s status of the order in the my order page is "Dispatching"
    #     Given Esen has an marketplace enabled CIVIL order shipped to address inside Turkey
    #     When Cargo company had collected Esen’s orders from CIVIL supplier
    #     Then Esen should be able to see the order status "Shipped" for marketplace enabled CIVIL supplier order 
    #     Then Esen should be able to see tracking id “678676578" for the order in the My order's page

    # @TrackingT
    # Scenario: 4 - Combination of “marketplace enabled" and Modanisa shipped orders
    #     Given that Esen is a logged in customer
    #     Given that Esen’s status of the order in the my order page is "Dispatching"
    #     Given Esen has an a marketplace enabled CIVIL supplier and modanisa shipped order to address inside Turkey
    #     Given Cargo company had collected Esen’s orders from CIVIL
    #     When Esen goes to my order page  
    #     Then Esen should be able to see the order status "Shipped" for marketplace enabled CIVIL supplier and modanisa shipped order
    #     Then Esen should be able to see tracking id “678676578" for the CIVIL supplier order 
    #     Then Esen should be able to see tracking id “678676579" for the Modanisa orders
