Feature: Directing MP customer to new checkout process

    As Esen
    I want to:  be directed to the Sign up/login page  when I proceed to checkout as a guest user and be able to find my products in the basket after signing up
    So That I Do not lose my pre-selected Items and be able to continue my shopping as a registered logged in User

    @S62-1 @checkout
    Scenario: Proceeding as guest through existing checkout process
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen add "ModanisaProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should see option to sign up
        Then Esen should see option to log in
        Then Esen should see option to continue as guest

    @S62-2 @checkout
    Scenario: Not Proceeding as guest through new checkout process
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should see option to sign up
        Then Esen should see option to log in
        Then Esen should not see option to continue as guest


    @S62-3 @checkout
    Scenario: Proceeding as registered customer to find basket filled with preselected items
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Given Esen click "Sign Up" from home page
        When Esen add registration details for sign up account
        Then Esen should have her preselected items in the basket


    @S62-4 @checkout
    Scenario: Proceeding as logged customer to find basket with preselected items
        Given Esen is in homepage
        Given Esen click "Login" from home page
        Given Esen add "tulay.basoglu@modanisa.com" and "Tulay3007"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | ModanisaProduct1 |
            | ModanisaProduct2 |
        Given Esen goes to basket page to verify products
        Then Esen click on logout link
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Given Esen add "tulay.basoglu@modanisa.com" and "Tulay3007"
        Then Esen should see orders from previous login
        Then Esen click on logout link


    @S62-5 @checkout
    Scenario: Proceeding as guest through existing checkout process other country
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen change shipping country as "Australia"
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should see option to sign up
        Then Esen should see option to log in
        Then Esen should see option to continue as guest
















