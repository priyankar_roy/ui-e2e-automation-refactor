Feature: Delete Returm

    As Esen
    I want to delete my return request
    So That I can keep the product if I change the my mind

    @delete_return
    Scenario: Delete Return Request for items not dropped off to the Cargo Company
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Tolga makes product as returnable for "new"
        Then Esen has selected order id for return on easy return center page
        Then Esen has selected items to be returned
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code
        Then Esen should see "You should drop products to cargo company till date" and exipry date
        Then Esen goes back to "show my past returns page" and see the return id
        Then Esen delete the return request
        Then Esen should be able to see "Process complete!"
        Then Omer has view deleted return date in table
        Then Esen cannot use the deleted return id to return items
        Then Esen click on logout link

    @dropped_cargo
    Scenario: Delete option not shown if customers return process complete for the return record
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        When Esen goes to easy returns page
        Then Esen click dropped return id from my retrun page
        Then Esen tries to delete return request
        Then Esen should be not see the delete button when return process completed
        Then Esen click on logout link
