Feature: Creation of cargo label errors

    As Esen
    I want to I want to initiate a return for my product
    So That I am able to return my product

    @S44 @createReturnID
    Scenario: Create return id for modanisa returns
        Given Esen opens the site for "qa" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "4157920000000002"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Tolga makes product as returnable for "new"
        Then Esen has selected order id for return on easy return center page
        Then Esen has selected items to be returned
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code
        Then Esen should see "You should drop products to cargo company till date" and exipry date
        Then Tolga verifies cargo number in returns table

    @S44 @createReturnID_old
    Scenario: Create return id for modanisa returns
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "ModanisaProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        When Esen completes the payment process
        Then Esen clicks terms and condition checkbox
        Then Esen clicks Place order button
        Then Esen should see the order confirmation page for legacy checkout
        Then Esen goes to My orders page for "old"
        Then Tolga makes product as returnable for "old"
        Then Esen has selected order id for return on easy return center page
        Then Esen has selected items to be returned
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code
        Then Esen should see "You should drop products to cargo company till date" and exipry date
        Then Tolga verifies cargo number in returns table

    @S44 @ExpiryReturn
    Scenario: Expired Return id on Return record page
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Tolga makes product as returnable for "new"
        Then Esen has selected order id for return on easy return center page
        Then Esen has selected items to be returned
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code
        Then Esen should see "You should drop products to cargo company till date" and exipry date
        Then Esen goes back to "show my past returns page" and see the return id
        Then Returnable product date is exipred
        Then Esen should not able to see the claim id in "show my past returns page"

    @S44 @Regeneration
    Scenario: Regeneration of return ID
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Tolga makes product as returnable for "new"
        Then Esen has selected order id for return on easy return center page
        Then Esen has selected items to be returned
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code
        Then Esen should see "You should drop products to cargo company till date" and exipry date
        Then Esen goes back to "show my past returns page" and see the return id
        Then Esen delete the return request
        Then Esen should be able to see "Process complete!"
        Then Esen goes back easy return home page
        Then Esen shoule able to create a return for same order id
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code

    @S44 @returnperiodexpiry
    Scenario:  RETURN PERIOD FOR THE PRODUCTS- Existing feature
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Tolga makes product as returnable for "new"
        Then Esen has selected order id form my orders page
        Then Esen should "see" the return items in my orders page
        Then Tolgs expires the return date as 90 days before
        Then Esen should "not see" the return items in my orders page

    @S44 @disablecreation
    Scenario: Disable creation of return id if already created for the same items
        Given Esen opens the site for "dev" environment on "MobileWeb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "return-qatest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Tolga makes product as returnable for "new"
        Then Esen has selected order id for return on easy return center page
        Then Esen has selected items to be returned
        Then Esen sees return summary page and confirm return process
        Then Esen should be able to see "process complete!" page with unique return code
        Then Esen should see "You should drop products to cargo company till date" and exipry date
        Then Esen goes back easy return home page
        Then Esen goes to create new return request page
        Then Esen should not see order number which already raised a return request
