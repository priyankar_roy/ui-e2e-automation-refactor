Feature: Differentiate MP Orders from Warehouse Orders

     As Warehouse Operator (Persona?)
     I want MP orders not to be added to Warehouse Operations
     So That My warehouse operation flow will not be affected.

     @S78-1
     Scenario: Modanisa product entry in Modanisa WMS
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Turkey"
          Given Esen click "Login" from home page
          Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
          Given Esen removes all products from the basket
          Given Esen add "Refka" in the search field
          When Esen proceeds checkout
          #When Esen completes the payment process
          #Then Esen clicks terms and condition checkbox
          Then Esen enters the payment details for other country
          Then Esen clicks Place order button
          Then Esen should see the order confirmation page for legacy checkout
          Then Warehouse operator picks order id
          Then Warehouse operator navigates to Modanisa WMS site
               | test-yonetici | test98 |
          Then Warehouse operator able to see order id and details in Modanisa WMS

     @78-2
     Scenario: No entry for MP product in modanisa WMS
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Turkey"
          Given Esen click "Login" from home page
          Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
          Given Esen removes all products from the basket
          Given Esen add "Muni Muni" in the search field
          Given Esen click active basket button
          Then Esen clicks secure payment button
          Then Esen should be navigated to "new" checkout page
          Then Esen click "Continue to payment" button
          Then Esen click "Terms and Condition" button
          Then Esen click "Place order" button
          Given Warehouse operator picks order id
          Then Warehouse operator navigates to Modanisa WMS site
          Then Warehouse operator should not see warehouse log for the order id

     @78-3
     Scenario: Order with both Modanisa and MP products
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Turkey"
          Given Esen click "Login" from home page
          Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
          Given Esen removes all products from the basket
          Given Esen add products in the search field
               | Muni Muni |
               | Refka     |
          Given Esen click active basket button
          Then Esen clicks secure payment button
          Then Esen should be navigated to "new" checkout page
          Then Esen click "Continue to payment" button
          Then Esen click "Terms and Condition" button
          Then Esen click "Place order" button
          Given Warehouse operator picks order id
          Then Warehouse operator navigates to Modanisa WMS site
     #Then Warehouse operator should not see warehouse log for the order id

     @S78-4
     Scenario: Other country
          Given Esen opens the site for "qa" environment on "Desktop"
          Given Esen change shipping country as "Canada"
          Given Esen click "Login" from home page
          Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
          # Given Esen removes all products from the basket
          Given Esen add products in the search field
               | Muni Muni |
               | Refka     |
          When Esen proceeds checkout
          #When Esen completes the payment process
          #Then Esen clicks terms and condition checkbox
          Then Esen enters the payment details for other country
          Then Esen clicks Place order button
          Then Esen should see the order confirmation page for legacy checkout
          Given Warehouse operator picks order id
          Then Warehouse operator navigates to Modanisa WMS site
          Then Warehouse operator able to see order id and details in Modanisa WMS