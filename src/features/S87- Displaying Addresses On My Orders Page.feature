Feature: Displaying Addresses On My Orders Page

    As Esen
    I want see my added addresses on my orders page
    So That I will be able to see which order was sent to which address

    @S87-01 @order-success-page
    Scenario Outline: Displaying Address on My Orders Page
        Given Esen opens the site for "qa" environment on "mobileweb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
        When Esen proceeds checkout
        When Esen clicks add new address button
        When Esen fills all required fields
        When Esen clicks on save and continue button
        When Esen captures saved address for delivery
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        When Esen "accepts" distance sales agreement
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        #Then Esen should see the selected address on my orders page along with the placed order

        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @S87-02 @order-success-page
    Scenario Outline:  Update Address
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
        When Esen proceeds checkout
        When Esen clicks on edit address
        When Esen edit updates title field in address box
            | Update title |
        When Esen clicks on save and continue button
        When Esen captures saved address for delivery
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        When Esen "accepts" distance sales agreement
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        Then Esen should see the selected address on my orders

        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @S87-3 @order-success-page
    Scenario Outline: Change in shipping address during checkout- applicable for desktop and mobile web
        Given Esen is in homepage
        # Given Esen opens the site for "qa" environment on "mobileweb"
        # # Given Esen change shipping country as "Turkey"
        # Given Esen click "Login" from home page
        # Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
        When Esen proceeds checkout
        # When Esen clicks change button
        When Esen captures saved address for delivery
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        When Esen "accepts" distance sales agreement
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen goes to My orders page for "new"
        #Then Esen should see the selected address on my orders page along with the placed order

        Examples:
            | CardNumber       |
            | 5526080000000006 |
