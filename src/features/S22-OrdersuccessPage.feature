Feature: Order Confirmation

    As Esen
    I want to see the list of successful orders, for which I have made the payment, in the order success page
    So That I know that I will receive the orders

    @S22-01 @order-success-page
    Scenario Outline: Verify Marketplace enabled order success page
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen should see option for selecting Standard Delivery for all Orders
        Then Esen should see estimated delivery for all orders
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen should see "<message>" in order success page
            | Thank you for your order!                                                |
            | Your order will be shipped to your shipping address as soon as possible. |
            | You can see your order details on My Orders page.                        |
        Then Esen should order success page with payment summary details
        Then Esen should see product group splits as to 1
        Then Tolga gets the order Id
        Then Tolga searches for order in database
        Then Esen goes to My orders page for "new"
        Then Esen should see placed order details in my orders page
        Given Esen is in homepage
        Given Esen click active basket button
        Then Esen should see empty basket
        Then Esen click on logout link

        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @S22-02 @order-success-page
    Scenario Outline: Verify Marketplace enabled order success page
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen should see option for selecting Standard Delivery for all Orders
        Then Esen should see estimated delivery for all orders
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page
        Then Esen should order success page with payment summary details
        Then Esen should see product group splits as to 3
        Then Esen goes to My orders page for "new"
        Then Esen should see placed order details in my orders page
        Then Esen click on logout link

        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @S22-03 @order-success-page
    Scenario: Verify Marketplace enabled order success page - when adding coupon code
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        When Esen apply coupon code for purchase with discounts "I have another coupon"
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen should see "<message>" in order success page
            | Thank you for your order!                                                |
            | Your order will be shipped to your shipping address as soon as possible. |
            | You can see your order details on My Orders page.                        |
        Then Esen should order success page with payment summary details
        Then Esen should see product group splits as to 1
        Then Esen click on logout link

    @S22-04 @order-success-page @order-summary-mf
    Scenario Outline: Verify Marketplace enabled order success page - when adding fixed price coupon
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | MpProduct2       |
            | MpProduct3       |
            | MODANISAPRODUCT1 |
        Given Esen click active basket button
        When Esen apply coupon code for purchase with discounts "yuzde10s93"
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen should see the order summary section on the checkout payment
        Then Esen should see the estimated delivery date for Modanisa order
        Then Esen should see the brand name, product image, product description, product size, product quantity and the total price of each product
        Then Esen should see the total sell price of items on SubTotal summary in review order summary
        Then Esen should the Discount section with the discount_amount in review order summary
            | should see     | FixedPercentage |
            | should not see |                 |
        Then Esen should see the total order amount in review order summary
        Then Esen fills Card details with card number "<CardNumber>"
        #Then Esen click "Terms and Condition" button
        When Esen "accepts" distance sales agreement
        Then Esen click "Place order" button
        Then Esen should see "<message>" in order success page
            | Thank you for your order!                                                |
            | Your order will be shipped to your shipping address as soon as possible. |
            | You can see your order details on My Orders page.                        |
        Then Esen should order success page with payment summary details
        Then Esen should see product group splits as to 1
        Then Esen click on logout link

        Examples:
            | CardNumber       |
            | 5526080000000006 |

    @S22-05 @order-success-page
    Scenario: Esen sees option to continue shopping and My order link - when adding Buy3get1 Coupon
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1 |
            | MpProduct1 |
            | MpProduct1 |
            | MpProduct1 |
        Given Esen click active basket button
        When Esen apply coupon code for purchase with discounts "BUY3GET1"
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen should see order summary in address selection page
        Then Esen click "Continue to payment" button
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen should see appropirate pages when clicking "<links>" in order success page
            | My Orders          |
            | View               |
            | OrderId            |
            | Countinue shopping |
        Then Esen should order success page with payment summary details
        Then Esen should see product group splits as to 1
        Then Esen click on logout link

    @S22-06 @order-success-page
    Scenario: Esen sees option to continue shopping and My order link - when adding SpendXGet30 Coupon
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | MpProduct2       |
            | ModanisaProduct1 |
            | ModanisaProduct1 |
            | ModanisaProduct2 |
        Given Esen click active basket button
        When Esen apply coupon code for purchase with discounts "SPENDXGET30"
        Then Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen click "Continue to payment" button
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen should see appropirate pages when clicking "<links>" in order success page
            | My Orders          |
            | View               |
            | OrderId            |
            | Countinue shopping |
        Then Esen should order success page with payment summary details
        Then Esen should see product group splits as to 3
        Then Esen click on logout link


    @S22-07 @order-success-page
    Scenario: Esen as a guest to complete the order process
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "ModanisaProduct1" in the search field
        Given Esen proceeds checkout
        #Given Esen click add new address button
        #Given Esen add the address in address form for "Login"
        When Esen completes the payment process
        Then Esen clicks terms and condition checkbox
        Then Esen clicks Place order button
        Then Esen should see the order confirmation page for legacy checkout
        Then Esen click on logout link


    @S22-08 @order-success-page
    Scenario: Esen as a guest to complete the order process
        #Given Esen is in homepage
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eTestAutomation@modanisa.com" and "Modanisa123"
        Given Esen change shipping country as "Australia"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen proceeds checkout
        #Given Esen click add new address button
        #Given Esen add the address in address form for "Login"
        Then Esen enters the payment details for other country
        Then Esen clicks terms and condition checkbox
        Then Esen clicks Place order button
        Then Esen should see the order confirmation page for legacy checkout
        Then Esen click on logout link