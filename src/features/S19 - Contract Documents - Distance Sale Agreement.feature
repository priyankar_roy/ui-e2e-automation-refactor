Feature: Contract Documents - Distance Sale Agreement and Preliminary Information

    As Esen
    I want to view applicable distance sale agreement and preliminary for MP orders
    So That I can be aware of the statutory/Compliance information

    @S19-1
    Scenario: Esen selects terms and condition and proceeds
        Given Esen opens the site for "qa" environment on "mobileweb"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "tulay.basoglu@modanisa.com" and "Tulay3007"
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen able to see order "successfull" page

    @S19-2
    Scenario: Esen does not confirm terms and condition and tries to proceed
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen "rejects" distance sales agreement
        Then Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen able to see order "successfull" page

    @S19-3
    Scenario: Esen does not confirm distance sales agreement and tries to proceed
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen "rejects" distance sales agreement
        When Esen places order
        Then Esen able to see order "payment" page

    @S19-4
    Scenario: Esen does not confirm distance sales agreement and tries to proceed
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen "accepts" distance sales agreement
        When Esen "deselect" distance sales agreement
        When Esen places order
        Then Esen able to see order "payment" page

    @S19-5
    Scenario: Esen views distance agreement details
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        When Esen click on "distance sale agreement" link
        Then Esen is directed to "distance sales agreement" in "new" page
    #Then Esen is able to see "distance sales agreement" details

    @S19-6
    Scenario: Esen views distance agreement details old checkout
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Refka" in the search field
        When Esen proceeds checkout
        Then Esen click on Distance Sale Agreement link
        Then Esen is directed to "distance sales agreement" in "old" page

    @S19-7
    Scenario: Esen views Preliminary Information details
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        When Esen click on "preliminary information" link
        Then Esen is directed to "Preliminary Information" in "new" page

    @S19-8
    Scenario: Esen views Preliminary Information details old checkout
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "Refka" in the search field
        When Esen proceeds checkout
        When Esen click on Preliminary Information link
        Then Esen is directed to "preliminary information" in "old" page

    @S19-9
    # Last three lines are commented because in payment page few fields are preselected
    Scenario Outline: Esen selects distance agreement and proceeds in EXISTING checkout for other countries
        #Given Esen is in homepage
        Given Esen is in homepage
        Given Esen change shipping country as "<CountryName>"
        Given Esen removes all products from the basket
        Given Esen add "Muni Muni" in the search field
        When Esen proceeds checkout
        Then Esen should be navigated to "old" checkout page
        When Esen enters the payment details for other country
        Then Esen clicks terms and condition checkbox
        Then Esen clicks Place order button
        Then Esen should see the order confirmation page for legacy checkout
        Then Esen click on logout link

        Examples:
            | CountryName    |
            | United Kingdom |




