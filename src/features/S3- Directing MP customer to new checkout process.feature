Feature: Directing MP customer to new checkout process

    As Esen
    I want to to be directed to the new checkout system when I add MP items/products in the basket
    So That I can receive my items faster

    #OldCheckout
    @S3 @checkout
    Scenario: Esen proceeds through Modanisa EXISTING checkout
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen removes all products from the basket
        Given Esen add "ModanisaProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should be navigated to "old" checkout page

    #NewCheckout
    @S3 @checkout
    Scenario: Esen proceeds through NEW MP checkout when both MP and Modanisa products are present in the basket
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
        When Esen proceeds checkout
        Then Esen should be navigated to "new" checkout page

    @S3 @checkout
    Scenario: Esen proceeds through NEW MP checkout for a single Modanisa product
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should be navigated to "new" checkout page


    #NewCheckoutMulti
    @S3 @checkout
    Scenario: Esen proceeds through Modanisa NEW MP checkout if all Modanisa products are removed from the basket
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | MpProduct3       |
            | ModanisaProduct1 |
            | ModanisaProduct1 |
        When Esen proceeds checkout
        When Esen clicks add new address button
        When Esen fills all required fields
        When Esen clicks on save and continue button
        Then Esen removes products from basket
            | ModanisaProduct1 |
            | ModanisaProduct1 |
        When Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page

    #RemoveItems
    @S3 @checkout
    Scenario: Esen proceeds through Modanisa EXISTING checkout if all MP products are removed from basket
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct3       |
            | ModanisaProduct1 |
        When Esen proceeds checkout
        Then Esen should be navigated to "new" checkout page
        Then Esen removes products from basket
            | MpProduct3 |
        When Esen clicks secure payment button
        Then Esen should be navigated to "old" checkout page

    @S3 @checkout
    Scenario Outline: Esen proceeds through Modanisa EXISTING checkout for international Orders
        Given Esen is in homepage
        Given Esen change shipping country as "<CountryName>"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should be navigated to "old" checkout page
        Examples:
            | CountryName              |
            | Australia                |
            | United States of America |
            | Germany                  |




