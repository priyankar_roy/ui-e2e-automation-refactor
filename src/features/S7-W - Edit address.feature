Feature: Edit Address

    As Esen
    I want to edit the already added address
    So That I don't spend time in adding it as a new address again

    @S7-1 @address-mf
    Scenario:  Edit address selection
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "shoban.manohar@thoughtworks.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        When Esen clicks on edit address
        Then Esen can see address block present with values pre-filled

    @S7-2 @address-mf
    Scenario: Esen validates the change in the first address
        # Given Esen opens the site for "dev" environment on "Desktop"
        # Given Esen change shipping country as "Turkey"
        # Given Esen click "Login" from home page
        # Given Esen add "testAndroid1@modanisa.com" and "asdfgh"
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        When Esen clicks on edit address
        When Esen edit updates title field in address box
            | titleA |
        When Esen clicks on save and continue button
        Then Esen navigates to registered address page
        Then Esen validates the change in the first address

    @S7-3 @address-mf
    Scenario: Esen compares the first name and the registered first name
        # Given Esen opens the site for "qa" environment on "Desktop"
        # #And Esen change shipping country as "Turkey"
        # And Esen click "Login" from home page
        # And Esen add "shoban.manohar@thoughtworks.com" and "Modanisa123"
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen navigates to registered address page
        And Esen updates name of a registered address
        And Esen proceeds checkout
        Then Esen clicks change address button
        And Esen looks for first name in change address window
        And Esen compares the first name and the registered first name


    @S7-4 @address-mf
    Scenario: Esen confirms the checkout title not be changed
        # Given Esen opens the site for "qa" environment on "Desktop"
        # #Given Esen change shipping country as "United Kingdom"
        # Given Esen click "Login" from home page
        # Given Esen add "shoban.manohar@thoughtworks.com" and "Modanisa123"
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        And Esen clicks on edit address
        And Esen reads title of current address
        And Esen edit updates title field in address box
            | Update title |
        Then Esen clicks on back button
        And Esen confirms the checkout title not be changed


