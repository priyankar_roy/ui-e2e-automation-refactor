Feature: View Orders and select delivery option

    As Esen
    I want to view my products in groups , which will be in the same delivery package
    So That I may choose the right shipping methodFeature Description

    @S9-SingleGroup @checkout
    Scenario: Esen will view product groups based on Order splitting rules
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "vojaswwin.ap@thoughtworks.com" and "Modanisa321"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        Given Esen click active basket button
        When Esen apply coupon code for purchase with discounts "fixedAmount1"
        When Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen should see order split groups as 1
        Then Esen should see discounted price for 1 products in address selection page
        Then Esen click "Continue to payment" button
        Then Esen should see discounted price for 1 products in payment page


    @S9-ThreeGroups-MPandModanisaEnabled @checkout
    Scenario: Esen will view product groups based on Order splitting rules

        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
            | MpProduct2       |
        When Esen proceeds checkout
        Then Esen should be navigated to "new" checkout page
        Then Esen should see order split groups as 3
        Then Esen should see discounted price for 2 products in address selection page
        Then Esen click "Continue to payment" button
        Then Esen should see discounted price for 2 products in payment page

    #Added product 491030 as it does not have discount price

    @S9-ThreeGroups @checkout
    Scenario: Esen will view product groups based on Order splitting rules
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
            | MpProduct3       |
        Given Esen click active basket button
        When Esen apply coupon code for purchase with discounts "fixedAmount1"
        When Esen clicks secure payment button
        Then Esen should be navigated to "new" checkout page
        Then Esen should see order split groups as 3
        Then Esen should see discounted price for 2 products in address selection page
        Then Esen click "Continue to payment" button
        Then Esen should see discounted price for 2 products in payment page

    #Then Esen click on logout link

    @S9-OneGroup @checkout
    Scenario: Esen will view product groups based on Order splitting rules
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen should be navigated to "new" checkout page
        Then Esen should see order split groups as 1

    #test for verify itemlevel discount S68

    @S9-OtherCountry @checkout
    Scenario Outline: Esen will view product groups based on Order splitting rules
        Given Esen is in homepage
        Given Esen change shipping country as "<CountryName>"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1       |
            | ModanisaProduct1 |
            | MpProduct2       |
        When Esen proceeds checkout
        Then Esen should be navigated to "old" checkout page
        Examples:
            | CountryName              |
            | Australia                |
            | United States of America |
            | Germany                  |









