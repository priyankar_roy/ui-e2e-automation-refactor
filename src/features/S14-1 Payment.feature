Feature: Select payment method

    As Esen
    I want to pay by credit/debit without 3D option
    So That I can avoid the additional verification step

    @S141 @payment-mf
    Scenario: Esen sees the card Details in the animation when she enters the card Details
        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "vojaswwin.ap@thoughtworks.com" and "Modanisa321"
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526 0800 0000 0006" and sees the card Details in animation

    @S141 @payment-mf
    Scenario: Esen enters details of invalid cards and finds appropriate error messages
        Then Esen fills Card details with card Number and sees the appropriate Messages
            | cardNumber          | messages                                                                 |
            | 4111 1111 1111 1129 | Insufficient fund. Please try with another card.                         |
            | 4124 1111 1111 1116 | Invalid card details. Please check information you entered.              |
            | 4130 1111 1111 1118 | We could not fulfil the payment at the moment. Please try again later.   |
            # | 4125 1111 1111 1115 | Your card is expired. Please try with another card or contact your bank. |
            # | 4121 1111 1111 1119 | We could not fulfil the payment at the moment. Please try again later.   |
            # | 4130 1111 1111 1118 | We could not fulfil the payment at the moment. Please try again later.   |

    @S141 @payment-mf
    Scenario Outline: Esen fills the card Details with the correct values and proceeds to the OrderSummary Page
        Given Esen is in homepage
        Given Esen removes all products from the basket
        Given Esen add "MpProduct1" in the search field
        When Esen proceeds checkout
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "<CardNumber>"
        When Esen "accepts" distance sales agreement
        When Esen places order
        Then Esen able to see order "successfull" page
        Examples:
            | CardNumber       |
            | 5526080000000006 |
            | 4910050000000006 |
            | 5170410000000004 |


#end to end tets case for payment deatils and validate in the order success page