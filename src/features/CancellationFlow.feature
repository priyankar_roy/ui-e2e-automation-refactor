Feature: Basic Cancellation flows

    @supplier-cancellations
    Scenario: Coupons should be Added when MP product is cancelled

        Given Esen opens the site for "qa" environment on "Desktop"
        Given Esen change shipping country as "Turkey"
        Given Esen click "Login" from home page
        Given Esen add "e2eAutomationTest@modanisa.com" and "Modanisa123"
        Given Esen removes all products from the basket
        Given Esen add products in the search field
            | MpProduct1 |
        Given Esen click active basket button
        Then Esen clicks secure payment button
        Then Esen click "Continue to payment" button
        Then Esen fills Card details with card number "5526080000000006"
        Then Esen click "Terms and Condition" button
        Then Esen click "Place order" button
        Then Esen able to see order "successfull" page

        Then Admin sees no Valid apology coupon available
        Then Tolga gets the order Id
        Then Tolga cancels "1" product ids
        Then Admin "should see" new Coupon in coupon's table
        Then Admin should see shipping fees "returned"

    @warehouse-cancellations
    Scenario: Coupon and Gifts should be added when Modanisa product is cancelled

        # Given Esen opens the site for "qa" environment on "Desktop"
        # Given Esen is in home page
        # Given Esen removes all products from the basket
        # Given Esen add products in the search field
        #     | ModanisaProduct1 |
        #     | ModanisaProduct1 |
        # Given Esen click active basket button
        # Then Esen clicks secure payment button
        # Then Esen should be navigated to "new" checkout page
        # When Esen clicks add new address button
        # When Esen fills all required fields
        # When Esen clicks on save and continue button
        # Then Esen click "Continue to payment" button
        # Then Esen fills Card details with card number "5526080000000006"
        # Then Esen click "Terms and Condition" button
        # Then Esen click "Place order" button
        # Then Esen able to see order "successfull" page
        # Then Admin sees no Valid apology coupon available
        Then stock "reservation" cron should be run
        Then stock "depo" cron should be run
        Then stock "warehouse" cron should be run
        # Then Admin should see order in DB with status "11"
        # And  Admin waits for order to change to "10"
        # Then Admin cancels "1" product ids in Modanisa order in warehouse
        # Then Admin should see gift item added in Modanisa order
        # Then Admin "should" see new coupon in coupon's table
        # Then Admin should see "1" row in cancels table for ModanisaOrder












# Scenario: Shipping fees should be returned only after full cancellation

#     Given Esen opens the site for "qa" environment on "Desktop"
#     Given Esen is in home page
#     Given Esen removes all products from the basket
#     Given Esen add products in the search field
#         | ModanisaProduct1 |
#     Given Esen click active basket button
#     Then Esen clicks secure payment button
#     Then Esen should be navigated to "new" checkout page
#     Then Esen click "Continue to payment" button
#     Then Esen fills Card details with card number "5526080000000006"
#     Then Esen click "Terms and Condition" button
#     Then Esen click "Place order" button
#     Then Esen able to see order "successfull" page
#     Then Admin should see the 2 orders in DB with status "11" and "10"
#     When Admin cancels "1" product ids in Modanisa order
#     Then Admin should see gift item added in Modanisa order
#     Then Admin "should" see new coupon in coupon's table
#     Then Admin should see "1" row in cancels table for ModanisaOrder




