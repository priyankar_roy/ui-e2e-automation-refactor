Feature: Manage Delivery Address

     As Esen
     I want to add the shipping address
     So That I can receive delivery of the package at that address

     @S01 @address-mf
     Scenario: Esen sees all required fields in address form
          Given Esen opens the site for "qa" environment on "Desktop"
          # Given Esen change shipping country as "İngiltere"
          Given Esen click "Login" from home page
          Given Esen add "tulay.basoglu@modanisa.com" and "Tulay3007"
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          Then Esen should see all fields in address form

     #end to end to test for checking adding address in order succes page and DB and registered address page
     @S01-02 @address-mf
     Scenario Outline: Esen fills address form and click on save and continue
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen fills all required fields
          When Esen clicks on save and continue button
          When Esen captures saved address for delivery
          Then Esen click "Continue to payment" button
          Then Esen fills Card details with card number "<CardNumber>"
          When Esen "accepts" distance sales agreement
          Then Esen click "Place order" button
          Then Esen should see correct Address in order success page
          Examples:
               | CardNumber       |
               | 5526080000000006 |


     @S01 @address-mf
     Scenario: Esen sees country, city, district dropdown
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          Then Esen can see Country dropdown with pre-selected value
               | Turkey |
          Then Esen can select city dropdown with value
               | stanbul |
          Then Esen should see the list of districts in dropdown
               | stanbul | Adalar  |
               | Ankara  | Bala    |
               | zmir    | Bergama |

     @S01 @address-mf
     Scenario: Esen saved address and navigates to checkout page
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen fills all required fields
          When Esen clicks on save and continue button
          Then Esen lands on checkout page


     @S01 @address-mf
     Scenario: Esen clicks on back button
          Given Esen is in homepage
          Given Esen removes all products from the basket
          Given Esen add "MpProduct1" in the search field
          When Esen proceeds checkout
          When Esen clicks add new address button
          When Esen clicks on close button
          Then Esen lands on checkout page

