import openWebsite from '../support/action/openWebsite'
import { snakeCase } from 'snake-case'

export default class BaseProject {
  environment

  constructor(environment) {
    this.environment = environment
    let environmentProject = `${this.constructor.name} ${environment}`

    let baseURL = snakeCase(environmentProject).toUpperCase()
    browser.options.baseUrl = process.env[baseURL]
  }

  openPage(page = '') {
    openWebsite(null, page)
    browser.pause(2000)
    return this
  }

  getEnvironment() {
    return this.environment
  }
}
