export const Environment = Object.freeze({
  TEST: 'Test',
  STAGE: 'Stage',
  PROD: 'Prod',
});

export const Language = Object.freeze({
  EN: 'en',
  TR: 'tr'
});
