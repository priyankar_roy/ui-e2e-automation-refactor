import click from "../support/action/clickElement"
import BaseProject from "./BaseProject";
import clickElement from "../support/action/clickElement";
import setInputField from "../support/action/setInputField";

export default class DepoYonetim extends BaseProject {
  constructor(environment) {
    super(environment)

    this.selectorElements = {
      myAccountButton: "#pageHeader-myaccountIcon",
      openLanguageModalButton: ".menuPageContainer > #accountMenu-currencyPopupOpener .accountMenuCurrencySection-country",
      languageSelect: ".languages",
      changeLanguageButton: "#step-1-button",

      loginStep: {
        loginWithEmailButton: "#login-form > div.row.type-select-container > label:nth-child(2) > input",
        emailInput: ".login-form #email",
        passwordInput: ".login-form #password",
        loginSubmitButton: "#login-form .confirm"
      }
    }
  }

  openSignInPage() {
    this.openPage('membership/login/')

    return this
  }

  selectLanguage = (language = "en") => {
    const languageSelector = `${this.selectorElements.languageSelect} .${language}`

    if ($(languageSelector).isExisting()) {
      click('click', 'element', languageSelector)
      click('click', 'element', this.selectorElements.changeLanguageButton)
    }

    browser.pause(5000)

    return this
  }

  loginWithEmailAndPassword = (email, password) => {
    clickElement('click', 'select', this.selectorElements.loginStep.loginWithEmailButton)

    setInputField('add', email, this.selectorElements.loginStep.emailInput)
    setInputField('add', password, this.selectorElements.loginStep.passwordInput)
    clickElement('click', 'element', this.selectorElements.loginStep.loginSubmitButton)
  }
}