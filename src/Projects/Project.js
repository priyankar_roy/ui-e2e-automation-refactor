import MobileWeb from "./MobileWeb";
import Desktop from "./Desktop";

export default class Project {

  projectName = ""
  environment = ""

  projects = {
    'MobileWeb': MobileWeb,
    'Desktop': Desktop
  }

  constructor(projectName) {
    if (! this.projects.hasOwnProperty(projectName)) {
      throw new Error("Project not found or not allowed.")
    }

    this.projectName = projectName
  }

  /**
   *
   * @param environment
   * @returns {BaseProject|MobileWeb}
   */
  getProject(environment) {
    this.environment = environment

    return new this.projects[this.projectName](environment)
  }

}