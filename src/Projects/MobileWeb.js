import click from '../support/action/clickElement'
import selectOption from '../support/action/selectOption'
import BaseProject from './BaseProject'
import clickElement from '../support/action/clickElement'
import setInputField from '../support/action/setInputField'
import openwebsite from '../support/action/openWebsite'
import waitforpageload from '../support/lib/waitforScript'
import waitforScript from '../support/lib/waitforScript'
import CountryCodes from '../libraries/Countries'

export default class MobileWeb extends BaseProject {
  constructor(environment) {
    super(environment)

    this.selectorElements = {
      myAccountButton: '#pageHeader-myaccountIcon',
      openLanguageModalButton:
        '#pageContainer #accountMenu-currencyPopupOpener div .accountMenuCurrencySection-country',
      languageSelect: '.foreignPopupContent-selectLanguage',
      countrySelect:
        '.foreignPopupContent-select.foreignPopupContent-selectCountry',
      changeLanguageButton: '.foreignPopupContent-applyButton',

      loginStep: {
        loginWithEmailButton: 'label[for="loginPageTabHead-label-email"]',
        emailInput: '#loginRegisterPage-formEmail',
        passwordInput: '#loginRegisterPage-formPassword',
        loginSubmitButton: '#loginRegisterPage-formButtonSubmit',
      },

      menuItems: {
        orderPageButton: '.accountMenu-itemLink-orders',
      },
    }
  }

  openSignInPage() {
    this.openPage('membership/login/')

    return this
  }

  selectLanguage(language = 'en', country) {
    console.log('I am Selecting ', country)
    browser.pause(2000)
    //browser.saveScreenshot('screenshots/homepage.png')
    click('click', 'element', this.selectorElements.myAccountButton)
    click('click', 'element', this.selectorElements.openLanguageModalButton)
    selectOption(
      'value',
      CountryCodes[country],
      this.selectorElements.countrySelect
    )
    selectOption('value', language, this.selectorElements.languageSelect)
    click('click', 'element', this.selectorElements.changeLanguageButton)
    browser.pause(5000)
    return this
  }

  loginWithEmailAndPassword = (email, password) => {
    clickElement(
      'click',
      'select',
      this.selectorElements.loginStep.loginWithEmailButton
    )

    setInputField('add', email, this.selectorElements.loginStep.emailInput)
    setInputField(
      'add',
      password,
      this.selectorElements.loginStep.passwordInput
    )
    clickElement(
      'click',
      'element',
      this.selectorElements.loginStep.loginSubmitButton
    )

    return this
  }

  openOrdersPage() {
    clickElement('click', 'element', this.selectorElements.myAccountButton)
    clickElement(
      'click',
      'element',
      this.selectorElements.menuItems.orderPageButton
    )

    return this
  }
}
