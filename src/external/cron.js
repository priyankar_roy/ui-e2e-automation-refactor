import legacyDB from '../external/legacy_database'
import cron_lock_id from '../libraries/cron_lock_id'
import { expect } from 'chai'

let counter = 0
let counterDate = 0
let cronLock
let ordersResult
let orderNumber = '77056870'

class cron {
  async IsReservationCronCompleted(cron_name) {
    ordersResult = await legacyDB.getOrdersData(orderNumber)
    //   stockInfo.getCaptureOrderId().toString()
    while (true) {
      cronLock = await legacyDB.getActiveCronLockData(
        cron_lock_id[cron_name.toUpperCase()]
      )
      console.log('status of cron: ' + cronLock.is_running)
      console.log(cronLock.date_start)
      console.log(ordersResult.date_add)
      if (counter == 5) break
      if (counterDate == 5) {
        throw err('Tried with two time not proc')
      }
      if (cronLock.is_running == 1) {
        console.log('Cron is inprogress')
        await browser.pause(30000)
        counter++
      } else if (cronLock.is_running == 0) {
        console.log('Cron is not running')
        if (new Date(cronLock.date_start) > new Date(ordersResult.date_add)) {
          console.log('I am satisfied with condition')
          ordersResult = await legacyDB.getOrdersData(orderNumber)
          // stockInfo.getCaptureOrderId().toString()
          expect(ordersResult.reservation_status).to.be.equal(1)
          break
        }
        await browser.pause(30000)
        counterDate++
      }
    }
    counter = 0
    counterDate = 0
  }

  async IsDepoCronCompleted(cron_name) {
    console.log(cron_name)
    ordersResult = await legacyDB.getOrdersData(orderNumber)
    while (true) {
      cronLock = await legacyDB.getActiveCronLockData(
        cron_lock_id[cron_name.toUpperCase()]
      )
      console.log(cronLock.id)
      console.log(cronLock.is_running)
      console.log(cronLock.date_start)
      console.log(ordersResult.date_add)
      if (counter == 5) break
      if (counterDate == 5) {
        throw err('Tried with two time not proc')
      }
      if (cronLock.is_running == 1) {
        console.log('Cron is inprogress')
        await browser.pause(30000)
        counter++
      } else if (cronLock.is_running == 0) {
        console.log('Cron is completed')
        if (new Date(cronLock.date_start) > new Date(ordersResult.date_add)) {
          console.log('I am satisfied with condition')
          ordersResult = await legacyDB.getOrdersData(orderNumber)
          expect(ordersResult.status).to.be.equal(11)
          break
        }
        await browser.pause(30000)
        counterDate++
      }
    }
    counter = 0
    counterDate = 0
  }

  async IsWarehouseCronCompleted(cron_name) {
    ordersResult = await legacyDB.getOrdersData(orderNumber)
    while (true) {
      cronLock = await legacyDB.getActiveCronLockData(
        cron_lock_id[cron_name.toUpperCase()]
      )
      console.log(cronLock.is_running)
      console.log(cronLock.date_start)
      console.log(ordersResult.date_add)
      if (counter == 5) break
      if (counterDate == 5) {
        throw err('Tried with two time not proc')
      }
      if (cronLock.is_running == 1) {
        console.log('Cron is inprogress')
        await browser.pause(30000)
        counter++
      } else if (cronLock.is_running == 0) {
        console.log('Cron is completed')
        if (new Date(cronLock.date_start) > new Date(ordersResult.date_add)) {
          console.log('I am satisfied with condition')
          ordersResult = await legacyDB.getOrdersData(orderNumber)
          expect(ordersResult.status).to.be.equal(10)
          break
        }
        await browser.pause(30000)
        counterDate++
      }
    }
    counter = 0
    counterDate = 0
  }
}

const cronLogic = new cron()

export default cronLogic
