// Destroy MySQL connection
// We need to close connection when the process is done
module.exports = (connection) => {
  connection.destroy()
}
