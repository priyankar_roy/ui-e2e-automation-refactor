// import JsonSql from 'json-sql'

// import MySQL from './../../libraries/Mysql'
// import Timer from './../../libraries/Timer'

// import orderJson from './resources/order.json'
// import orderProductsJson from './resources/order_products.json'
// import orderCargoDataJson from './resources/order_cargo_data.json'
// import orderCargoStatusJson from './resources/cargo_status.json'

// const HOST = process.env.MODANISA_DB_HOST
// const DB_NAME = process.env.MODANISA_DB_NAME
// const USERNAME = process.env.MODANISA_DB_USERNAME
// const PASSWORD = process.env.MODANISA_DB_PASSWORD
// const jsonSql = new JsonSql()

// jsonSql.configure({ separatedValues: false })
// jsonSql.setDialect('mysql')

// const modanisaDB = new MySQL(HOST, DB_NAME, USERNAME, PASSWORD)
// modanisaDB.connect()
// modanisaDB.query()

// const createOrder = async () => {
//   orderJson.date_add = Timer.getDate()
//   orderJson.date_comp = Timer.getDate()

//   const create_order_sql = jsonSql.build({
//     type: 'insert',
//     table: 'orders',
//     values: orderJson,
//   }).query

//   const results = await modanisaDB.query(create_order_sql)

//   const orderId = results.insertId

//   console.log(orderId)

//   for (let orderProductJson of orderProductsJson) {
//     orderProductJson.parent = orderId
//     orderProductJson.date_add = Timer.getDate()
//   }

//   const create_order_items_sql = jsonSql.build({
//     type: 'insert',
//     table: 'order_products',
//     values: orderProductsJson,
//   }).query

//   await modanisaDB.query(create_order_items_sql)

//   return orderId
// }

// const createOrderCargoData = async (orderId) => {
//   orderCargoDataJson.order_id = orderId
//   orderCargoDataJson.created_at = Timer.getDate()

//   const create_order_cargo_data_sql = jsonSql.build({
//     type: 'insert',
//     table: 'order_cargo_data',
//     values: orderCargoDataJson,
//   }).query

//   const results = await modanisaDB.query(create_order_cargo_data_sql)
//   return results.insertId
// }

// const createOrderCargoStatus = async (orderId) => {
//   orderCargoStatusJson.order_id = orderId
//   orderCargoStatusJson.date_updated = Timer.getDate()
//   orderCargoStatusJson.date_delivery_to_cargo = Timer.getDate()
//   orderCargoStatusJson.date_delivery_to_customer = Timer.getDate()

//   const create_order_cargo_status_sql = jsonSql.build({
//     type: 'insert',
//     table: 'cargo_status',
//     values: orderCargoStatusJson,
//   }).querys

//   const results = await modanisaDB.query(create_order_cargo_status_sql)
//   return results.insertId
// }

// module.exports = {
//   modanisaDB,
//   createOrder,
//   createOrderCargoData,
//   createOrderCargoStatus,
// }
