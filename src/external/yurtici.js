const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest

const yurticiSoap = function () {
  this.username = 'TESTMODANISAMRK'
  this.password = '08gaWh74G86m851x'

  this.cancelOrder = (orderId) => {
    var xmlhttp = new XMLHttpRequest()
    xmlhttp.open(
      'POST',
      'https://testwebservices.yurticikargo.com/KOPSWebServices/NgiShipmentInterfaceServices?wsdl',
      true
    )

    var soapRequest = `
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ngis="http://yurticikargo.com.tr/NgiShipmentInterfaceServices">
                <soapenv:Header/>
                <soapenv:Body>
                <ngis:cancelNgiShipment>
                    <wsUserName>${this.username}</wsUserName>
                    <wsPassword>${this.password}</wsPassword>
                    <wsUserLanguage>TR</wsUserLanguage>
                    <ngiCargoKey>${orderId}</ngiCargoKey>
                    <ngiDocumentKey>${orderId}</ngiDocumentKey>
                    <cancellationDescription>test</cancellationDescription>
                </ngis:cancelNgiShipment>
                </soapenv:Body>
            </soapenv:Envelope>`

    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          console.dir(xmlhttp.responseText)
        }
      }
    }

    xmlhttp.setRequestHeader('Content-Type', 'text/xml')
    xmlhttp.send(soapRequest)
  }
}

module.exports = new yurticiSoap()
