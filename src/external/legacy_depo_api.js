const request = require('request')

const legacyDepoApi = function () {
  const options = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      MPU: 'MdnsHeaderPickingUser',
      MPTS: '1505134352',
      MPT: '0e78b52e95c09f8e475025bf1e0907ef',
      token: '34717e7aff487b653a54a9f33605a1a8',
      warehouseId: '1',
      agentId: '267',
    },
  }

  this.shipment = (orderId, cargoName) => {
    let shipmentEndpoint = `http://depo-api.localhost/cargo/s/${cargoName}/${orderId}/shipment`

    request.post(shipmentEndpoint, options, function (err, res, body) {
      console.log('body: \n')
      console.dir(body)
      console.log('error: \n')
      console.dir(err)
    })
  }
}

module.exports = new legacyDepoApi()
