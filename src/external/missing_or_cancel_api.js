import legacydatabase from '../external/legacy_database'
const axios = require('axios')
const baseApi = require('../external/baseAPI')
const expect = require('chai').expect

const cancelproducts = function () {
  let orderID
  var cancel_header_res
  var cancel_body

  const token_body = {
    customerId: 7407887,
  }

  const token_headers = {
    'Accept-Language': 'EN',
    'Content-Type': 'application/json',
    Cookie: '__cfduid=d03a46648d0167518982f2f05b0fad1561607922011',
  }

  let setProductId = async (productIdCount) => {
    cancel_body = {
      orderProductIds: await getProductId(productIdCount),
    }
    console.log(cancel_body)
    return cancel_body
  }

  let getProductId = async (productIdCount) => {
    let productIds = await legacydatabase.getModanisaOrderProductIdFromOrderProductTable(
      orderID,
      productIdCount
    )
    return productIds
  }

  let getToken = async () => {
    console.log('i am in get token')
    let tokenEndPoint = `http://marketplace-auth-api-staging.modanisa.net/token`
    const res = await baseApi.post(tokenEndPoint, token_body, token_headers)
    expect(res.status).to.be.equal(201)

    return res.data['token']
  }

  let creteHeaderForCancel = async () => {
    console.log('i am in createtoken header')
    const cancel_header = {
      'Accept-Language': 'EN',
      Authorization: 'Bearer ' + (await getToken()),
      'Content-Type': 'application/json',
      Cookie: '__cfduid=d71ed97b079f90a040d63987979f9bf261606942126',
    }
    return cancel_header
  }

  let getCancelRes = async () => {
    cancel_header_res = await creteHeaderForCancel()
    return cancel_header_res
  }

  this.cancelOrderFromSupplier = async (orderIdfromUI, productIDCount) => {
    orderID = orderIdfromUI
    let cancelEndPoint = `http://marketplace-order-management-api-staging.modanisa.net/cancel/${orderID}/products`
    const cancelRes = await baseApi.put(
      cancelEndPoint,
      await setProductId(productIDCount),
      {
        header: await getCancelRes(),
      }
    )
    expect(cancelRes.status).to.be.equal(200)
  }

  this.expiryReturn = async (orderID, claimID) => {
    let expiryReturnHeader = {
      agentid: '266',
      'cache-control': 'no-cache',
      'content-type': 'application/json',
      mpt: '0e78b52e95c09f8e475025bf1e0907ef',
      mpts: '1505134352',
      mpu: 'MdnsHeaderPickingUser',
      token: 'ab6d335f6f03ad9aaa21c526e78ef17f',
      warehouseid: '1',
      Cookie: 'PHPSESSID=91eske4crqtq0b84ps4ohg31cb',
    }

    let expiryReturnBody = {}

    let exipryEndPoint = `http://marketplace-depo-api-test.modanisatest.com/cargo/expiry/yurtici/${orderID}/${claimID}`
    const expiryResponse = await baseApi.post(
      exipryEndPoint,
      expiryReturnBody,
      expiryReturnHeader
    )
    browser.pause(2000)
    console.log(expiryResponse.data)
    expect(expiryResponse.status).to.be.equal(200)
  }
}
module.exports = new cancelproducts()
