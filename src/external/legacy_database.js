import { Environment } from '../Projects/Environment'
import JsonSql from 'json-sql'
import Timer from '../libraries/Timer'
import orderCargoStatusJson from './legacy/resources/cargo_status.json'
import moment from 'moment'
var mysql = require('mysql')
const expect = require('chai').expect

const legacyDatabase = function () {
  const jsonSql = new JsonSql()
  jsonSql.configure({ separatedValues: false })
  jsonSql.setDialect('mysql')

  let productID
  this.connection
  this.credentials = {
    Stage: {
      host: 'master-db-staging.modanisatest.com',
      user: 'developer',
      password: '/U!e=YbRN6',
      database: 'modanisa',
    },
    Test: {
      host: 'master-db-test.modanisatest.com',
      user: process.env.DB_EX_USERNAME || 'developer',
      password: process.env.DB_EX_PASSWORD || '/U!e=YbRN6',
      // user: 'gtestdev172',
      // password: 'anhurpxp34*2322xA2',
      database: 'modanisa',
    },
  }

  this.openConnection = async () => {
    this.connection = await mysql.createConnection(this.credentials.Test)
    console.log('***CONNECTION OPENED!!***')
    await this.connection.connect((err) => {
      if (err) throw err
    })
  }

  this.closeConnection = async () => {
    if (
      (await this.connection.state) === 'connected' ||
      (await this.connection.state) === 'authenticated'
    ) {
      await this.connection.destroy()
      console.log('***CONNECTION CLOSED!!***')
    }
  }

  this.updateOrderShipCityIdAndCellNumber = async (
    orderId,
    cellNumber,
    cityId
  ) => {
    this.openConnection()

    const order = [cellNumber, cityId, orderId]
    this.connection.query(
      'UPDATE orders SET cell_number = ?, ship_city = ? WHERE id = ? LIMIT 1',
      order,
      (err, res) => {
        if (err) throw err
        console.log(`Changed ${res.changedRows} row(s)`)
      }
    )

    this.closeConnection()
  }

  this.closeCargoErrors = async (orderId) => {
    await this.openConnection()
    await this.connection.query(
      'UPDATE mo_cargo_error_order SET status = 1 WHERE order_id = ? and status = 0',
      [orderId],
      (err, res) => {
        if (err) throw err

        console.log(`CargoError Changed ${res.changedRows} row(s)`)
      }
    )

    await this.closeConnection()
  }

  this.updateOrderStatus = async (orderId, statusId) => {
    await this.openConnection()
    await this.connection.query(
      'UPDATE orders SET status = ? WHERE id = ? LIMIT 1',
      [statusId, orderId],
      (err, res) => {
        if (err) throw err
        console.log(
          `Order Status Changed ${res.changedRows} row(s) for orderID: ${orderId} statusID: ${statusId}`
        )
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
  }

  this.updateCargoName = async (orderId, cargoName) => {
    await this.openConnection()
    await this.connection.query(
      'UPDATE orders SET cargo = ? WHERE id = ? LIMIT 1',
      [cargoName, orderId],
      (err, res) => {
        if (err) throw err

        console.log(
          `Order Status Changed ${res.changedRows} row(s) for orderID: ${orderId} cargoName: ${cargoName}`
        )
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
  }

  this.getModanisaOrderProductIdFromOrderProductTable = async (
    orderid,
    productIdCount
  ) => {
    let productIds = []
    console.log(orderid)
    await this.openConnection()

    await this.connection.query(
      `SELECT id FROM order_products WHERE parent = ${orderid} LIMIT ${productIdCount}`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        console.log(JSON.stringify(res) + ' response')

        for (let i = 0; i < productIdCount; i++) {
          productIds[i] = parseInt(JSON.stringify(res[i].id))
        }
      }
    )
    await browser.pause(2000)
    await this.closeConnection()

    console.log('product id:' + productIds)
    return productIds
  }

  this.checkCancelledProductIDIsPresentInCancelTable = async (
    orderid,
    productCount
  ) => {
    await this.openConnection()

    await this.connection.query(
      `SELECT COUNT(order_product_id) as Id FROM cancel_products  WHERE order_id=${orderid}`,
      (err, res) => {
        if (err) {
          console.log(err)
        }

        expect(res[0].Id).to.be.equal(parseInt(productCount))
      }
    )

    await browser.pause(10000)
    await this.closeConnection()
  }

  this.checkOrderIdInOrdersTable = async (orderid) => {
    console.log(orderid)
    await this.openConnection()
    await this.connection.query(
      `SELECT id FROM orders WHERE id=${orderid}`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        expect(res[0].id.toString()).to.be.equal(orderid)
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
  }
  this.createOrderCargoStatus = async (orderId) => {
    await this.openConnection()
    await browser.pause(5000)
    orderCargoStatusJson.order_id = orderId
    orderCargoStatusJson.date_updated = Timer.getDate()
    orderCargoStatusJson.date_delivery_to_cargo = Timer.getDate()
    orderCargoStatusJson.date_delivery_to_customer = Timer.getDate()

    const create_order_cargo_status_sql = await jsonSql.build({
      type: 'insert',
      table: 'cargo_status',
      values: orderCargoStatusJson,
    }).query

    await this.connection.query(create_order_cargo_status_sql, (err) => {
      if (err) {
        console.log(err)
      }
      console.log('Successfully Inserted a row into cargo_status table')
    })
    await browser.pause(10000)
    await this.closeConnection()
  }

  this.checkClaimIDInReturnsTable = async (claimID, orderID) => {
    await this.openConnection()
    await this.connection.query(
      `SELECT order_id FROM return_form_logs WHERE id=${claimID}`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        expect(res[0].order_id.toString()).to.be.equal(orderID.toString())
      }
    )

    await browser.pause(10000)
    await this.closeConnection()
  }

  this.setOrderStatus = async (orderID, status) => {
    await this.openConnection()
    await this.connection.query(
      `UPDATE orders SET status = ${status} WHERE id = ${orderID}`,

      (err, res) => {
        if (err) {
          console.log(err)
        }
        // expect(res[0].order_id.toString()).to.be.equal(orderID.toString())
      }
    )

    await browser.pause(10000)
    await this.closeConnection()
  }

  this.selectCratedReturnDate = async (claimID) => {
    let response
    await this.openConnection()
    await this.connection.query(
      `SELECT DATE_FORMAT(created_at,'%Y-%m-%d %H:%i:%s') as created_at FROM return_form_logs WHERE id=${claimID}`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        response = res[0].created_at.toString()
        console.log('CREATED DATE' + response)
      }
    )

    await browser.pause(10000)
    await this.closeConnection()
    return response
  }

  this.updateOrderCompletedate = async (orderID) => {
    await this.openConnection()
    let pastDate = get90DaysPastDate()
    console.log(pastDate)
    await this.connection.query(
      'UPDATE orders SET date_comp = ? WHERE id = ? LIMIT 1',
      [pastDate, orderID],
      (err, res) => {
        if (err) throw err

        console.log(
          `Expire Date Changed ${res.changedRows} row(s) for OrderID: ${orderID} date_comp: ${pastDate}`
        )
      }
    )
    //await browser.pause(10000)
    await this.closeConnection()
  }

  function get90DaysPastDate() {
    let date = new Date()
    let exipiryDate = new Date(date)
    exipiryDate.setDate(exipiryDate.getDate() - 90)
    let finalDate = `${exipiryDate.getFullYear()}-${exipiryDate.getMonth()}-${exipiryDate.getDate()} ${exipiryDate.getHours()}:${exipiryDate.getUTCMinutes()}:${exipiryDate.getSeconds()}`
    return finalDate
  }

  this.selectDeleteDateInReturnsTable = async (claimID) => {
    let response
    await this.openConnection()
    await this.connection.query(
      `SELECT deleted_at FROM return_form_logs where id='${claimID}'`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        response = res[0].deleted_at
        console.log('Deleted Date ' + response)
      }
    )

    await browser.pause(10000)
    await this.closeConnection()
    return response
  }

  this.removeAllApologyCoupon = async () => {
    await this.openConnection()
    await this.connection.query(
      `Delete from coupons_codes Where ifcustomer = "9028310";`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
      }
    )
    await this.connection.query(
      `Select Count(*) as count from coupons_codes Where ifcustomer = "9028310";`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        console.log('response', res)
        console.log(res[0].count)
        expect(res[0].count).to.equal(0)
      }
    )

    await browser.pause(10000)
    await this.closeConnection()
  }

  this.shouldSeeApologyCoupon = async () => {
    await this.openConnection()
    await this.connection.query(
      `Select * from coupons_codes Where ifcustomer = "9028310";`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        expect(res.size === 1).to.be.true
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
  }

  this.shouldSeeShippingFees = async (orderID) => {
    await this.openConnection()
    await this.connection.query(
      'Select sprice from cancels Where order_id = "70117404";',
      (err, res) => {
        if (err) {
          console.log('Hello')
          console.log(err)
        }
        expect(res[0].sprice).to.equal(0)
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
  }

  this.prioritizeCreatedBatch = async (pickingID) => {
    // ToBeFinished

    await this.openConnection()

    await this.connection.query(
      'update mo_picking_batch set status = 1 where picking_id IN (195276);',
      (err, res) => {
        if (err) {
          console.log(err)
        }
        expect(res.sprice).to.equal(0)
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
  }

  this.getActiveCronLockData = async (id) => {
    let cronData
    await this.openConnection()
    await this.connection.query(
      `Select * from cron_lock where id=${id} and status=1`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        console.log('***Fetching Cron_Lock Data***')
        cronData = res[0]
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
    return cronData
  }

  this.getOrdersData = async (orderId) => {
    let orderData
    await this.openConnection()
    await this.connection.query(
      `Select * from orders where id=${orderId}`,
      (err, res) => {
        if (err) {
          console.log(err)
        }
        console.log('***Fetching Orders Data***')
        orderData = res[0]
      }
    )
    await browser.pause(10000)
    await this.closeConnection()
    return orderData
  }
}

module.exports = new legacyDatabase()
