const axios = require('axios')

class API {
  async get(reqUrl) {
    return await axios.get(reqUrl, { validateStatus: false, headers })
  }

  async post(reqUrl, body, headers) {
    return await axios.post(reqUrl, body, { validateStatus: false, headers })
  }

  async put(reqUrl, body, headers) {
    return await axios.put(reqUrl, body, { headers })
  }

  async delete(reqUrl, body) {
    return await axios.delete(reqUrl, { validateStatus: false, headers })
  }
}

module.exports = new API()
