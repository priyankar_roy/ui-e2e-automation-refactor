import {Given, When, Then} from "cucumber";
import checkElementExists from "../support/check/checkElementExists";
import Project from "../Projects/Project";
import DeviceLib from "../libraries/Device";
import legacy_database from "../external/legacy_database";
import {Environment, Language} from "../Projects/Environment";

const selectors = {
  DispatchingStatusSelector: "//div[@class='orderItem-container']/p[@class='orderItem-status'][contains(text(),'Status: {orderStatus}')]",
  DispatchingOrderIdSelector: "/../a[@class='orderItem-link']/p[@class='orderItem-title'][contains(text(),'Review Order: #{orderId}')]"
}

var dummyData = {
  userCredentials: {
    email: "melektural@hotmail.com",
    password: "Melek123",
  },
  orderId: 62636621
}

/** @var trackingProject MobileWeb|Project **/
var trackingProject;

Given(/^that Esen is a logged in customer on "([^"]*)?"$/, (platform) => {
    DeviceLib.setDevice(platform);

    trackingProject = new Project(platform).getProject(Environment.STAGE)
    trackingProject.openPage()
               .selectLanguage(Language.EN)
               .openSignInPage()
               .loginWithEmailAndPassword(dummyData.userCredentials.email, dummyData.userCredentials.password)
  }
);

Given(/^Esen has an "([^"]*)?" order shipped to address inside "([^"]*)?" and its status is "([^"]*)?"$/,
  (orderSource, countryName, customerOrderStatus) => {

    // Prepare order for Dispatching
    legacy_database.openConnection(trackingProject.getEnvironment())
    legacy_database.updateOrderStatus(dummyData.orderId, 11);

    trackingProject.openOrdersPage()

    legacy_database.updateOrderStatus(dummyData.orderId, 99);
    legacy_database.closeConnection()

    let orderStatusSelector = selectors.DispatchingStatusSelector.replace(/{orderStatus}/i, customerOrderStatus)
    let firstOrderIdSelector = `${orderStatusSelector} ${selectors.DispatchingOrderIdSelector}`.replace(/{orderId}/i, dummyData.orderId)
    checkElementExists("an", firstOrderIdSelector);


  }
);

When(/^Cargo company had collected Esen’s orders from "([^"]*)?"$/,
  (orderSource) => {

  }
);

When(/^Esen goes to my order page$/,
  () => {

  }
);

When(/^Esen should be able to see the order status "([^"]*)?" for "([^"]*)?" shipped order$/,
  (customerOrderStatus, orderSource) => {

  }
);

When(/^Esen should be able to see tracking id for her order$/,
  () => {

  }
);
