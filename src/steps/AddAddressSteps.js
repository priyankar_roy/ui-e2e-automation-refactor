import { Given, When, Then } from 'cucumber'
import address from '../pages/address'
import newAddress from '../pages/newAddressPage'
import ordersuccesspage from '../pages/ordersuccesspage'
const expect = require('chai').expect

When(/^Esen clicks add new address button$/, { timeout: 100000 }, () => {
  browser.pause(3000)
  newAddress.clickNewAddressButton()
  browser.pause(3000)
})

Then(/^Esen clicks on save and continue button$/, () => {
  newAddress.clickSaveButton()
  browser.pause(5000)
})

Then(/^Esen clicks on back button$/, () => {
  newAddress.clickBackButton()
  browser.pause(5000)
})

Then(/^Esen clicks on close button$/, () => {
  newAddress.clickCloseButton()
  browser.pause(5000)
})

Then(/^Esen can select city dropdown with value$/, (city) => {
  const city_to_select = city.raw()
  newAddress.selectCity(city_to_select)
})

Then(/^Esen should see the list of districts in dropdown$/, (field) => {
  const city_district = field.rowsHash()
  // newAddress.enterAddressFieldData(city_district)                                   ///////////
  newAddress.getDistricts(city_district)
})

Then(/^Esen lands on checkout page$/, () => {
  // const city_to_select = city.raw()
  newAddress.confirmNavigateToCheckoutPage()
})

Then(/^Esen can see Country dropdown with pre-selected value$/, (country) => {
  const country_to_check = country.raw()
  newAddress.checkCountryPreSelected(country_to_check)
})

Then(
  /^Esen encounters incomplete address error message for "([^"]*)?"$/,
  (contryCode) => {
    if (contryCode == 'UK') {
      newAddress.checkFieldErrorsForAllBlank()
    } else if (contryCode == 'TR') {
      newAddress.checkFieldErrorsForAllBlank_TR()
    }
  }
)

Then(/^Esen clicks on an address fields and presses tab$/, () => {
  newAddress.clickAddressFieldsAndPressTab()
})

Then(/^Esen clicks on and enters to check for regex$/, (field) => {
  const fieldsArray = field.rowsHash()
  newAddress.enterAddressFieldData(fieldsArray)
})

Then(/^Esen encounters address regex "<message>"$/, (messages) => {
  const messagearray = messages.raw()
  newAddress.validateMessageForWrongEntryInAddressForm(messagearray)
})

When(/^Esen enters minimum string length to respective fields$/, (field) => {
  const fieldsArray = field.rowsHash()
  newAddress.validateMinimumInputLength(fieldsArray)
})

Then(
  /^Esen encounters warning message for minimum length "<message>"$/,
  (messages) => {
    const messagearray = messages.raw()
    newAddress.validateMessageForMinimumLengthEntryInAddressForm(messagearray)
  }
)

Then(/^Esen should see all fields in address form$/, () => {
  newAddress.checkAddressFormForAllRequiredFields()
  newAddress.checkCountryFieldIsDisabled()
})

When(/^Esen fills all required fields$/, { timeout: 100000 }, () => {
  console.log('filling fields')
  newAddress.enterAddressdetails()
})

When(/^Esen enters address string in address text box$/, (address) => {
  const addressArray = address.rowsHash()
  newAddress.testWrongAddress(addressArray)
})

When(/^Esen enters maximum string length to respective fields$/, (field) => {
  const fieldsArray = field.rowsHash()
  newAddress.validateMaximunInputLength(fieldsArray)
})

Then(/^Esen verifies the maximum string entered$/, (messages) => {
  const messagearray = messages.raw()
  newAddress.CheckMaximumFieldLength(messagearray)
})

Given(/^Esen removes all address from the account$/, () => {
  console.log('inside given step')
  address.navigateToRegisteredAddress()
  address.removeAllSavedAddress()
})

Then(/^Esen should see warning message in old address page$/, () => {
  address.oldAddressPage()
})

When(/^address form should be open$/, () => {
  newAddress.addressFormDefaultOpen()
})

When(/^Esen clicks on close form$/, () => {
  newAddress.closeAddressForm()
})

Then(/^Esen clicks on edit address$/, () => {
  browser.pause(8000)
  newAddress.editAddress()
})

Then(/^Esen can see address block present with values pre-filled$/, () => {
  newAddress.checkAddressBlockPreEnteredValues()
})

Then(/^Esen edit updates title field in address box$/, (field) => {
  const title_field_to_update = field.raw()
  newAddress.updateAddressField_addressTitle(title_field_to_update)
})

Then(/^Esen clicks on change address$/, () => {
  newAddress.changeAddress()
})

Then(/^Esen confirms the checkout title not be changed$/, () => {
  let checkoutTitle = newAddress.readAddressTitleInCheckoutPage
  console.log(checkoutTitle)
  let titleAsRead = newAddress.addressTitleAsRead
  expect(checkoutTitle).to.equal(titleAsRead)
})

Then(/^Esen looks for first name in change address window$/, () => {
  newAddress.getFirstNameFromChangeAddressWindow()
})

Then(/^Esen reads title of current address/, () => {
  newAddress.readAddressTitle()
})

Then(/^Esen compares the first name and the registered first name$/, () => {
  let changeAddressName = newAddress.getFirstNameFromChangeAddressWindow()
  let registeredAddressName = address.registeredName
  expect(changeAddressName).to.equal(registeredAddressName)
})
When(/^Esen captures saved address for delivery$/, () => {
  newAddress.captureAddedAddress()
})

When(/^Esen should see correct Address in order success page$/, () => {
  ordersuccesspage.orderSuccessPageAddressValidation()
})
Then(/^Esen enters the address title as "([^"]*)?"$/, (title) => {
  newAddress.fillAddressTitle(title)
})

Then(
  /^Esen should see the latest address being selected with title as "([^"]*)?"$/,
  (title) => {
    newAddress.validateIfAddressIsUpdatedInCheckout(title)
  }
)

Then(/^Esen "([^"]*)?" change address button$/, (action) => {
  newAddress.CheckIfChangeOptionIsAvailable(action)
})

Given(/^Esen add address in old checkout$/, () => {
  address.fillNewAddressFormInOldCheckout()
})

Then(/^Esen should see edit button for (all|selected) addresses$/, (action) => {
  console.log(newAddress.checkIfEditOptionIsAvailable(action))
})

When(/^Esen clicks change address button$/, () => {
  newAddress.clickChangeAddressButton()
})

Then(/^Esen should see address with titles$/, (title) => {
  let titles = title.raw()

  newAddress.checkIfAddresswithTitlesExist(titles)
})

Then(/^Esen selects address with title "([^"]*)?"$/, (title) => {
  newAddress.selectRequiredAddress(title)
})

Then(/^Esen clicks "([^"]*)?" option in address form$/, (button) => {
  newAddress.clickButtonInAddress(button)
})
