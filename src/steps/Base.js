import { Given, When, Then } from 'cucumber'
import registartionForm from '../pages/registraion'
import welcomePage from '../pages/welcome'
import DeviceLib from '../libraries/Device'
import openWebsite from '../support/action/openWebsite'
import homepage from '../pages/homepage'
import legacyapi from '../external/missing_or_cancel_api'
import legacydatabase from '../external/legacy_database'
import newAddress from '../pages/newAddressPage'
import payment from '../pages/payment'
import Timer from '../libraries/Timer'
import productList from '../libraries/ProductDetail'
import ProductList from '../libraries/ProductDetail'
const addproducts = require('../pages/addproducts')
const checkout = require('../pages/checkout')
const userlogin = require('../pages/userlogin')
const address = require('../pages/address')
//const payment = require('../pages/payment')
const ordersSuccessPage = require('../pages/ordersuccesspage.js')
const basketPage = require('../pages/basketpage')
const newAddressPage = require('../pages/newAddressPage.js')
const orderspage = require('../pages/orderspage')
const expect = require('chai').expect
//var orderid

Given(/^Esen opens the (url|site) "([^"]*)?"$/, (type, url) => {
  openWebsite(type, url)
})

Given(
  /^(Esen|Omer) opens the (url|site) for "([^"]*)?" environment on "([^"]*)?"$/,
  (usertype, type, environment, device) => {
    DeviceLib.setDevice(device)
    userlogin.openWebsite(type, environment)
  }
)

Given(/^Esen selects "(tr|en|fr)" language$/, (language) => {
  welcomePage.selectLanguage(language)
})

Given(/^Esen click on the welcome popup$/, () => {
  userlogin.acceptWelcomePopup()
})

Given(/^Esen (add|set) products in the search field$/, (method, value) => {
  const productName = value.raw()

  productName.forEach((products) => {
    let supplierProduct = products.toString()
    addproducts.searchProducts(
      method,
      productList[supplierProduct.toUpperCase()]
    )
    addproducts.clickSearchButton()
    addproducts.selectProduct()
    addproducts.selectSizeFromDropdown()
    addproducts.clickAddToBasketButton()
  })
})

Given(/^Esen (add|set) "([^"]*)?" in the search field$/, (method, value) => {
  let supplierProduct = value.toUpperCase()
  addproducts.searchProducts(method, ProductList[supplierProduct])
  addproducts.clickSearchButton()
  addproducts.selectProduct()
  addproducts.selectSizeFromDropdown()
  addproducts.clickAddToBasketButton()
})

When(/^Esen proceeds checkout$/, () => {
  checkout.clickBasketButton()
  basketPage.getProductIDsBeforeSignin()
  checkout.clickSecurePaymentButton()
})

Given(/^Esen click active basket button$/, () => {
  checkout.clickBasketButton()
})

When(/^Esen clicks secure payment button$/, () => {
  if (DeviceLib.isMobile()) {
    checkout.clickSecurePaymentButton()
  } else {
    checkout.checkBasketOutValues()
    checkout.clickSecurePaymentButton()
  }
})

Given(
  /^Esen (click|doubleclick) "([^"]*)?" from login page$/,
  (method, logintype) => {
    userlogin.clickGuestLink(method, logintype)
  }
)

Given(
  /^Esen (add|set) the address in address form for "([^"]*)?"$/,
  (method, userType) => {
    if (userType == 'Guest') {
      address.enterAddressdetails(method)
      address.clickSaveButton()
    } else {
      address.enterAddressDetailsForLogin(method)
      address.clickSaveButtonForLogin()
      console.log('Entering address')
      browser.pause(10000)
    }
  }
)

When(/^Esen completes the payment process$/, () => {
  payment.clickCODOption()
})

When(/^Esen completes the payment process for other country$/, () => {
  payment.creditCardDetails('add')
})

Then(/^Esen clicks terms and condition checkbox$/, () => {
  payment.checkTermsAndConditions()
})

Then(/^Esen clicks Place order button$/, () => {
  browser.pause(16000)
  checkout.clickPlaceOrderButton()
})

//registration page
Given(/^Esen (click) "([^"]*)?" from home page$/, (method, type) => {
  if (type === 'Sign Up') {
    registartionForm.clickSignUpButton()
  } else {
    userlogin.clickLoginOrSignUpLink(method, type)
  }
})

When(/^Esen (add) registration details for sign up account$/, (method) => {
  registartionForm.enterRegistrationDetails(method)
  registartionForm.clickTermsAndCondition()
  registartionForm.clickConfirmSignUpButton()
})

Then(/^Esen sees home page of modanisa$/, () => {
  registartionForm.verifyAccountLink()
})

Given(/^Esen (click) add new address button$/, (method) => {
  address.clickNewAddressButton(method)
})

Given(/^Esen set browser location as "([^"]*)?"$/, (country) => {
  userlogin.setGeoLocation(country)
})

Given(/^Esen change shipping country as "([^"]*)?"$/, (country) => {
  userlogin.acceptWelcomePopup(country)
  userlogin.verifyDiscountPopup()
})

Given(
  /^Esen (add) "([^"]*)?" and "([^"]*)?"$/,
  (method, userEmail, userPassword) => {
    userlogin.loginWithEmail()
    userlogin.enterLoginDetails(method, userEmail, userPassword)
    userlogin.clickLoginButton()
  }
)

Given(/^Esen (click) on the login button$/, (method) => {
  userlogin.loginWithEmail(method)
})

When(/^Esen (click) "([^"]*)?" link from my account page$/, (method, page) => {
  homepage.myAccountButton()
  homepage.orderDetails()
})

Then(/^Esen able to see "([^"]*)?" page$/, (pagename) => {
  orderspage.orderReviewPage()
})

Then(/^Esen able to see orders information$/, () => {
  orderspage.orderInformation()
})

Then(/^Esen removes all products from the basket$/, () => {
  checkout.clickBasketButton()
  addproducts.removeAllProductsFromBasket()
})

Then(/^Esen click on logout link$/, () => {
  homepage.navigateToHomePage()
  homepage.myAccountButton()
  homepage.logout()
})

Given(/^Esen is in homepage$/, () => {
  homepage.navigateToHomePage()
})

Then(/^Esen apply coupon code for purchase with discounts$/, () => {
  checkout.enterCounponCode()
})

Then(
  /^Esen apply coupon code for purchase with discounts "([^"]*)?"$/,
  (coupon) => {
    browser.pause(3000)
    checkout.checkForAlreadyAppliedCoupon()
    browser.pause(3000)
    checkout.enterCounponCodeByType(coupon)
  }
)

Then(
  /^Esen should (click) on Continue as Guest link to continue as Guest$/,
  (method) => {
    userlogin.clickGuestLink(method, 'Continue As Guest')
  }
)

Then(/^Esen should see option to sign up$/, () => {
  userlogin.verifySignupButton()
})

Then(/^Esen should see option to log in$/, () => {
  userlogin.verifyLoginButton()
})

Then(/^Esen should see option to continue as guest$/, () => {
  userlogin.verifyContinueAsGuestLink()
})

Then(/^Esen should not see option to continue as guest$/, () => {
  console.log('Entered Step')

  userlogin.verifyContinueAsGuestLinkisNotVisible()
})

Then(/^Esen should have her preselected items in the basket$/, () => {
  basketPage.getProductIDsAfterSignIn()
  basketPage.checkifbaskethaspreselectedproducts()
})

Given(/^Esen goes to basket page to verify products$/, () => {
  checkout.clickBasketButton()
  basketPage.getProductIDsBeforeSignin()
})

Then(/^Esen should see orders from previous login$/, () => {
  basketPage.getProductIDsAfterSignIn()
  basketPage.checkifbaskethaspreselectedproducts()
})

Then(/^Esen should see the Order "([^"]*)?" in my Orders page$/, (status) => {
  console.log(status)
  orderspage.navigateToMyOrder()
  let actualStatus = orderspage.validateStatusOfLatestOrder()
  console.log(actualStatus)
  expect(actualStatus).to.be.equal(status)
})

Then(
  /^Esen should see "([^"]*)?" products in my Orders$/,
  (productsAvailableCount) => {
    let actualCount = orderspage.reviewAvailableProducts()
    expect(actualCount).to.be.equal(parseInt(productsAvailableCount))
  }
)

Then(
  /^Esen should see the order summary section on the checkout payment$/,
  () => {
    payment.checkBasketOutValues()
  }
)

Given(/^Esen opens checkout$/, () => {
  browser.url('')
})

Then(
  /^Esen should see discounted price for (-?\d+) products in (address selection|payment) page$/,
  (productCount, page) => {
    if (page == 'address selection') {
      newAddressPage.checkForDiscountInOrders(productCount)
    } else {
      payment.checkForDiscountInOrders(productCount)
    }
  }
)
Then(/^Esen should see empty basket$/, () => {
  basketPage.checkBasketIsEmpty()
})

Then(/^Esen removes all "([^"]*)?" products$/, (brandName) => {
  addproducts.removeProductsByBrand(brandName)
})

Then(/^Esen navigates to registered address page$/, () => {
  address.navigateToRegisteredAddress()
})

Then(/^Esen validates the change in the first address$/, () => {
  let firstRegisteredAddressTitle = address.getAddressTitleOfFirstRegisteredAddress()
  let enteredTitleInCheckoutAddress = newAddress.addressTitleChanged
  console.log(firstRegisteredAddressTitle)
  console.log(enteredTitleInCheckoutAddress)
  expect(firstRegisteredAddressTitle.toString()).to.equal(
    enteredTitleInCheckoutAddress
  )
})

Then(/^Esen updates name of a registered address$/, () => {
  address.updateFirstRegisteredAddressName()
})
