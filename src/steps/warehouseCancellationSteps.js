import legacydatabase from '../external/legacy_database'
import { Given, When, Then } from 'cucumber'
import depo_picking from '../pages/depo_picking'
import ordersPage from '../pages/orderspage'
import clickElement from '../support/action/clickElement'

Then(
  /^Admin cancels "([^"]*)?" product ids in Modanisa order in warehouse$/,
  (product) => {
    ordersPage.createXLSXSsheet()
    depo_picking.openDepoPicking()
    depo_picking.createNewBatch()
    browser.pause(30000)

    depo_picking.prioritizeCreatedBatch()
    depo_picking.markAsMissing()
    depo_picking.markAsEscaped()
  }
)
Then(
  /^ Admin should see order in DB with status "([^"]*)?" $/, () => {

    orderspage.verifyOrderStatus()
    
  }
)
