import legacydatabase from '../external/legacy_database'
import cronLock from '../external/cron'
import { Given, When, Then } from 'cucumber'

Then(/^stock "([^"]*)?" cron should be run$/, async (cronName) => {
  
  switch (cronName) {
    case 'reservation':
      console.log(cronName + ' is going to run')
      await cronLock.IsReservationCronCompleted(cronName)
      break

    case 'depo':
      console.log(cronName + ' is going to run')
      await cronLock.IsDepoCronCompleted(cronName)
      break

    case 'warehouse':
      console.log(cronName + ' is going to run')
      await cronLock.IsWarehouseCronCompleted(cronName)
      break

    default:
      break
  }
})
