import { Given, When, Then } from 'cucumber'
import payment from '../pages/payment'
import openWebsite from '../support/action/openWebsite'

Then(/^Esen "([^"]*)?" distance sales agreement$/, (confirmationType) => {
  payment.checkNewTermsAndCondition(confirmationType)
  browser.pause(3000)
})

Then(/^Esen click on payment button$/, () => {
  payment.clickNewCheckoutPayment()
  browser.pause(2000)
})
When(/^Esen places order$/, () => {
  payment.clickPlaceOrder()
  browser.pause(3000)
})

Then(/^Esen able to see order "([^"]*)?" page$/, (orderPageType) => {
  payment.validationClickPlaceOrder(orderPageType)
  browser.pause(3000)
})

When(/^Esen click on ^([^"]*)?" link$/, (linkType) => {
  payment.navigateToNewAgreement(linkType)
})

When(/^Esen click on "([^"]*)?" link$/, (linkType) => {
  payment.navigateToNewAgreement(linkType)
})

Then(/^Esen is directed to "([^"]*)?" page$/, (pageType) => {
  payment.vaidateNewAgreementPage(pageType)
})

Then(
  /^Esen is directed to "([^"]*)?" in "([^"]*)?" page$/,
  (docType, pageType) => {
    if (pageType === 'old') {
      payment.vaidateOldAgreementPage(docType)
    } else if (pageType === 'new') {
      payment.vaidateNewAgreementPage(docType)
    } else {
      console.log('Wrong  Checkout Type')
    }
  }
)

When(/^Esen click on Distance Sale Agreement link$/, () => {
  payment.navigateToOldAgreement('distance sale agreement')
  browser.pause(5000)
})

When(/^Esen click on Preliminary Information link$/, () => {
  payment.navigateToOldAgreement('preliminary information')
  browser.pause(5000)
})

Then(/^Esen is able to see "([^"]*)?" details$/, (agreementType) => {})

When(/^Esen enters the payment details for other country$/, () => {
  payment.otherCountryCreditCardDetails()
})

Then(/^Esen should see the estimated delivery date for Modanisa order$/, () => {
  payment.checkEstimatedDeliveryDate()
})

Then(
  /^Esen should see the brand name, product image, product description, product size, product quantity and the total price of each product$/,
  () => {
    payment.checkProductDetails()
    payment.checkBasketOutValues()
  }
)

Then(
  /^Esen should see the total sell price of items on SubTotal summary in review order summary$/,
  () => {
    payment.checkSubtotalSummary()
  }
)

Then(
  /^Esen should the Discount section with the discount_amount in review order summary$/,
  (field) => {
    const discount_applied = field.rowsHash()
    payment.checkForDiscountApplied(discount_applied)
  }
)

Then(/^Esen should see the total order amount in review order summary$/, () => {
  payment.checkSubtotalSummary()
})

Then(
  /^Esen fills Card details with card number "([^"]*)?" and sees the card Details in animation$/,
  (cardNumber) => {
    payment.enterCardDetailsAndVerifyIfReflectedInAnimation(cardNumber)
    // payment.enterValidCardDetails(cardNumber)
  }
)

Then(/^Esen fills Card details with card number "([^"]*)?"$/, (cardNumber) => {
  payment.enterValidCardDetails(cardNumber)
})

Then(/^Esen should see warnings for blank field in payment page$/, () => {
  payment.checkForWarningMessagesForBlankFields()
})

Then(/^Esen should see error message for invalid card$/, () => {
  browser.pause(5000)
  payment.checkForIncorrectCardMessage()
})

Then(
  /^Esen enters cardNumber "([^"]*)?" and cvvNumber "([^"]*)?"$/,
  (cardNumber, cvvNumber) => {
    payment.esenEntersCardNumber(cardNumber)
    payment.esenEntersCVV(cvvNumber)
  }
)

Then(/^Esen should see different error messages for different errors$/, () => {
  payment.esenSeesVariousErrorMessages()
})

When(
  /^Esen adds "([^"]*)?" as creditcard number she should see "([^"]*)?"$/,
  (textEntered, textVisible) => {
    payment.checkIfOnlyNonNumericCharactersAreAllowedForCreditcardNumbers(
      textEntered,
      textVisible
    )
  }
)

When(
  /^Esen adds "([^"]*)?" as CVV she should see "([^"]*)?"$/,
  (textEntered, textVisible) => {
    payment.checkIfOnlyNonNumericCharactersAreAllowedForCVVField(
      textEntered,
      textVisible
    )
  }
)

Then(
  /^Esen fills Card details with card Number and sees the appropriate Messages$/,
  (messages) => {
    const message = messages.hashes()
    message.forEach((input) => {
      console.log(input, input.cardNumber, input.messages)
      payment.enterValidCardDetails(input.cardNumber)
      payment.accectTermsAndContion()
      payment.clickPlaceOrder()
      payment.checkMessagesInLabel(input.messages)
    })
  }
)

Then(
  /^Esen fills Card details with card Number and enable 3D payment click on "([^"]*)?" with "([^"]*)?" OTP option and sees the appropriate Messages$/,
  (type,pinType,messages) => {
    const message = messages.hashes()
    message.forEach((input) => {
      console.log(input, input.cardNumber, input.messages)
      payment.enterValidCardDetails(input.cardNumber)
      payment.click3DSecureCheckBox()
      payment.accectTermsAndContion()
      payment.clickPlaceOrder()
      payment.enterOTPDetails(pinType)
      payment.clickButtonOn3DSecurePage(type)
      payment.checkMessagesInLabel(input.messages)
    })
  }
)

Then(
  /^Esen should see error_messages for fields in payment page$/,
  (fields) => {
    const field = fields.hashes()

    field.forEach((input) => {
      console.log(input, input.cardFields, input.errorMessages)
      payment.checkMessagesUnderFields(input.cardFields, input.errorMessages)
    })
  }
)

Then(/^Esen selects 3D secure payment checkbox$/, () => {
  payment.click3DSecureCheckBox()
})

Then(/^Esen will be navigated to 3D secure page and enter "([^"]*)?" OTP$/, (pinType) => {
  payment.enterOTPDetails(pinType)
})


Then(/^Esen clicks "([^"]*)?" button$/, (type) => {
  payment.clickButtonOn3DSecurePage(type)
})

Then(/^Esen sees error message in payment page$/,() => {
  payment.errorMessageOnPaymentPage()
})
