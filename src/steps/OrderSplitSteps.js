import { Given, When, Then } from 'cucumber'
import ordersplitpage from '../pages/ordersplitpage'
import ProductList from '../libraries/ProductDetail'
const expect = require('chai').expect

Then(/^Esen should be navigated to "([^"]*)?" checkout page$/, (pageType) => {
  ordersplitpage.validationOnCheckoutPage(pageType)
})

Then(/^Esen removes products from basket$/, (value) => {
  browser.back()
  const productToBeRemoved = value.raw()
  productToBeRemoved.forEach((products) => {
    products = products.toString()
    console.log(products)
    ordersplitpage.removeProduct(ProductList[products.toUpperCase()])
    browser.pause(3000)
  })
})

Then(/^Esen removes "([^"]*)?" item$/, (productName) => {
  ordersplitpage.removeProduct(ProductList[productName.toUpperCase()])
})

Then(/^Esen should see order split groups as (-?\d+)$/, (groupCount) => {
  ordersplitpage.verifyShipmentMethodsSection() //groupCount
  ordersplitpage.validateSupplierGroups(groupCount)
})

Then(
  /^Esen should see option for selecting Standard Delivery for all Orders$/,
  () => {
    expect(
      ordersplitpage.checkIfStandardDeliveryOptionIsAvailable()
    ).to.be.equal(true)
  }
)

Then(/^Esen should see estimated delivery for all orders$/, () => {
  expect(ordersplitpage.checkIfEstimatedDeliveryIsVisible()).to.be.equal(true)
})
