import legacydatabase from '../external/legacy_database'
import { Given, When, Then } from 'cucumber'

Then(/^Admin sees no Valid apology coupon available$/, async () => {
  await legacydatabase.removeAllApologyCoupon()
})

Then(/^Admin "([^"]*)?" new Coupon in coupon's table$/, async (action) => {
  
 await legacydatabase.shouldSeeApologyCoupon()

})

