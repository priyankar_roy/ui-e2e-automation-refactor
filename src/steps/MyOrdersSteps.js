import { Given, When, Then } from 'cucumber'
import orderspage from '../pages/orderspage'
import ordersuccesspage from '../pages/ordersuccesspage'
import payment from '../pages/payment'

When(/^Esen navigates to My order page$/, () => {
  ordersuccesspage.captureOrderID()
  orderspage.navigateToMyOrder()
})

Then(/^Esen should see placed order details in my orders page$/, () => {
  orderspage.validateOrderIDs()
})

Given(/^Warehouse operator picks order id$/, () => {
  var orderID = ordersuccesspage.captureOrderID()
  console.log("orderIDs generated = " + orderID)
})

Then(/^Esen should see order summary in address selection page$/, () => {
  payment.checkBasketOutValues()
})

Then(/^Esen should see the selected address on my orders$/,()=>{
orderspage.ordersPageAddressValidation()
})