import { Given, When, Then } from 'cucumber'
import ordersuccessPage from '../pages/ordersuccesspage'
import legacydatabase from '../external/legacy_database'
import legacyapi from '../external/missing_or_cancel_api'
import ordersPage from '../pages/orderspage'
var orderid

Then(/^Esen (click) "([^"]*)?" button$/, (action, buttonType) => {
  ordersuccessPage.clickCheckoutButtons(action, buttonType)
})

Then(/^Esen should see "<message>" in order success page$/, (messages) => {
  const messagearray = messages.raw()
  ordersuccessPage.validateThanksOrderSection(messagearray)
  ordersuccessPage.validatePaymentMethodSection()
  ordersuccessPage.shippingAddressSection()
})

Then(
  /^Esen should see appropirate pages when clicking "<links>" in order success page$/,
  (links) => {
    const linksarray = links.raw()
    linksarray.forEach((linkType) => {
      console.log('linkType in steps:' + linkType)
      ordersuccessPage.selectLinkType(linkType)
    })
  }
)

Then(/^Esen should order success page with payment summary details$/, () => {
  ordersuccessPage.validateOrderSummarySection()
})

Then(/^Esen should order success page with payment method details$/, () => {
  ordersuccessPage.validateOrderPaymentMethodSection()
})

Then(/^Esen should order success page with shipping address details$/, () => {
  ordersuccessPage.shippingAddressSection()
})

Then(/^Esen should see order estimated delivery$/, (delivery) => {
  const estimatedDelivery = delivery.raw()
  ordersuccessPage.estimatedDeliveryMethod(estimatedDelivery)
})

Then(
  /^Esen should see the order confirmation page for legacy checkout$/,
  () => {
    ordersuccessPage.validateOldCheckOutPage()
    // ordersuccessPage.getCargoReturnNumber()
  }
)

Then(
  /^Esen should see product group splits as to (-?\d+)$/,
  (actualGroupCount) => {
    ordersuccessPage.validateOrderGroup(actualGroupCount)
  }
)

Then(/^Tolga searches for order in database$/, async () => {
  await legacydatabase.checkOrderIdInOrdersTable(orderid.toString())
})

Then(/^Tolga gets the order Id$/, () => {
  orderid = ordersuccessPage.captureOrderID()
  console.log(orderid)
})

Given(/^Tolga cancels "([^"]*)?" product ids$/, async (productIDCount) => {
  console.log('orderid in Tolga cancels: ',orderid[0])
  await legacyapi.cancelOrderFromSupplier(orderid[0], productIDCount)

  await legacydatabase.checkCancelledProductIDIsPresentInCancelTable(
    orderid,
    productIDCount
  )
})

Then(/^Admin should see shipping fees "([^"]*)?"$/, async (action) => {
  await legacydatabase.shouldSeeShippingFees(orderid)
})
