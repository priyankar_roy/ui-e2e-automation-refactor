import { Given, When, Then } from 'cucumber'
import administrationPanel from '../pages/administrationpanelpage'

Given(
  /^Omer has incomplete order to fullfill with id "([^"]*)?"$/,
  (orderId) => {
    console.log('Preparing order with id: ' + orderId)
    administrationPanel.prepareOrderForCargoErrorInvalidCellnumber(orderId)
    administrationPanel.triggerShipmentForOrder(orderId)
  }
)

Given(
  /^Omer (add) login details "([^"]*)?" and "([^"]*)?"$/,
  (method, username, password) => {
    administrationPanel.enterUserDetails(method, username, password)
    administrationPanel.clickLoginButton()
  }
)

When(
  /^Omer (click) "([^"]*)?" from "([^"]*)?" menu$/,
  (method, pageType, menuType) => {
    administrationPanel.clickCargoErrorDropDown(method)
  }
)

When(
  /^Omer proceeds to order cargo page related to order "([^"]*)?"$/,
  (orderId) => {
    browser.url(
      browser.getUrl().toString().split('/')[0] +
        '/order/info/orderCargoList/' +
        orderId
    )
  }
)

Then(/^Omer views orders with cargo errors$/, () => {
  administrationPanel.verifyCargoErrorTableVisible()
})

Then(/^Omer (click) error link from cargo error table$/, (method) => {
  administrationPanel.clickCargoErrorLink(method)
})

Then(
  /^Omer should be able to see the failed marketplace "([^"]*)?" in the list of orders$/,
  (orderId) => {
    administrationPanel.verifyOrderIDFromCargoErrorPage(orderId)
  }
)

Then(/^Omer logouts$/, () => {
  administrationPanel.clickLogoutButton()
})

Then(
  /^Omer edits "([^"]*)?" in existing panel and saves them even it is incomplete$/,
  (orderId) => {
    console.log('Preparing order with id for another error: ' + orderId)
    administrationPanel.prepareOrderForCargoErrorInvalidCity(orderId)
    administrationPanel.triggerShipmentForOrder(orderId)
    browser.pause(3000)
  }
)

Then(
  /^Omer edits "([^"]*)?" in existing panel and saves them with complete$/,
  (orderId) => {
    console.log('Preparing order with id with completion: ' + orderId)
    administrationPanel.prepareOrderWithNoCargoError(orderId)
    administrationPanel.triggerShipmentForOrder(orderId)
    browser.pause(3000)
  }
)

Then(/^The cron job is triggered for order "([^"]*)?"$/, (orderId) => {
  administrationPanel.triggerShipmentForOrder(orderId)
})

Then(
  /^Omer should not be able to order on the list if cargo label creation is done successfully$/,
  () => {
    administrationPanel.noCozumIste()
  }
)

When(
  /^Omer has corrected order details for an order which failed cargo label creation$/,
  () => {
    console.log('Omer done to fix cargo label')
  }
)

Then(
  /^Omer is able to see new cargo error for order "([^"]*)?"$/,
  (orderId) => {
    administrationPanel.seeNewCargoError(orderId)
  }
)

Then(
  /^Omer should be able to see the cargo label information is placed to order details page.$/,
  () => {
    administrationPanel.seeCargoLabelLink()
  }
)
