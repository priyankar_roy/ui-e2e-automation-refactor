import yonetim from '../pages/yonetim'
import { Given, When, Then } from 'cucumber'

const expect = require('chai').expect

Given(/^Warehouse operator navigates to Modanisa WMS site$/, (loginCreds) => {
    const credsArray = loginCreds.rowsHash()
    yonetim.launchYonetim(credsArray)
})

var orderID = '67734442'
Then(/^Warehouse operator able to see order id and details in Modanisa WMS$/, () => {
    yonetim.launchYonetim()
    yonetim.searchOrder(orderID)
    yonetim.navigateToTransactionRecords()
    yonetim.confirmModanisaOrderMessage()
})

