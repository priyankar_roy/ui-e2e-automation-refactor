import { Given, Then, When } from 'cucumber'
import Return from '../pages/return'
import openWebsite from '../support/action/openWebsite'
import getTextFromElement from '../support/action/getTextFromElement'
import Timer from '../libraries/Timer'
import productManifacturer from '../external/legacy/resources/product_manifacturer.json'
import legacy from '../external/legacy_database'
import orderSuccessPage from '../pages/ordersuccesspage'
import myorderspage from '../pages/orderspage'
import easyReturnsPage from '../pages/returnspage'
import legacyAPI from '../external/missing_or_cancel_api'
const expect = require('chai').expect

let orderID
let cargoNumber
let date

const cargoStatus = {
  Dropped: '829814',
  'Picked Up': '829819',
  IN_TRANSIT: '829817',
}

Given(
  /^Esen has return available for "(.*?)" shipped order in her account$/,
  async (supplierName) => {
    openWebsite(null, 'en/membership/account/easy-return/')

    await Return.createModanisaOrder()
    await Return.createOrderCargoData()
    await Return.createOrderCargoStatus()
    await Return.clickCreateNewReturnButton()
    await Return.checkHasReturnableOrder()
  }
)

Given(/^Esen goes to My orders page for "([^"]*)?"$/, (checkoutType) => {
  if (checkoutType == 'new') {
    orderID = orderSuccessPage.getOrderID()
    myorderspage.navigateToMyOrder()
  } else if (checkoutType == 'old') {
    orderID = orderSuccessPage.captureOrderID_Old()
    myorderspage.navigateToMyOrder()
  } else {
    console.error('checkout type is undefined')
  }
})

// Given(/^Esen goes to My orders page for old checkout$/, () => {
//   orderID_Old = orderSuccessPage.captureOrderID_Old()
//   myorderspage.navigateToMyOrder()
// })

Given(
  /^Tolga makes product as returnable for "([^"]*)?"$/,
  async (checkoutType) => {
    await legacy.updateOrderStatus(orderID, '28')
    await legacy.updateCargoName(orderID, 'yurtici')
    await legacy.createOrderCargoStatus(orderID.toString())
  }
)

Then(
  /^Esen has selected order id for return on easy return center page$/,
  () => {
    myorderspage.clickOrderDetails(orderID)
    myorderspage.clickReturnItems()
  }
)

Then(/^Esen has selected order id form my orders page$/, () => {
  myorderspage.clickOrderDetails(orderID)
})

Then(
  /^Esen should "([^"]*)?" the return items in my orders page$/,
  (viewType) => {
    easyReturnsPage.verifyReturnsButton(viewType)
  }
)

Given(/^Esen has selected items to be returned$/, () => {
  easyReturnsPage.clickListItems(orderID)
  easyReturnsPage.selectProductsAndReason()
  easyReturnsPage.clickNextButton()
})

Given(/^Esen sees return summary page and confirm return process$/, () => {
  easyReturnsPage.verifyReturnFormPage()
  easyReturnsPage.checkAndEnterIBANDetails()
  easyReturnsPage.clickConfirmButton()
})

Then(
  /^Esen should be able to see "([^"]*)?" page with unique return code$/,
  (text) => {
    date = Timer.getDate()
    console.log('DATE IS', date)
    easyReturnsPage.assertText(text)
    easyReturnsPage.verifyReturnCodeSection()
  }
)

Then(/^Esen should be able to see "([^"]*)?"$/, (text) => {
  easyReturnsPage.assertText(text)
})

Then(/^Esen should see "([^"]*)?" and exipry date$/, (cargoMessage) => {
  easyReturnsPage.verifyReturnExpiryDate(cargoMessage)
  cargoNumber = easyReturnsPage.getCargoReturnNumber()
  console.log(cargoNumber + ' Cargo number is')
})

Then(/^Tolga verifies cargo number in returns table$/, async () => {
  await legacy.checkClaimIDInReturnsTable(cargoNumber, orderID)
})

Then(/^Returnable product date is exipred$/, async () => {
  console.log('I am waiting for expire my return id...')
  await browser.pause(120000) //***Don't remove this wait , this is needed for expire process***
  console.log('Wait is Over!! I am continuing !!!')
  await legacyAPI.expiryReturn(orderID.toString(), cargoNumber.toString())
})

Then(/^Esen goes back to "([^"]*)?" and see the return id$/, (value) => {
  easyReturnsPage.clickCloseReturnPageButton()
  easyReturnsPage.clickShowPastReturnsButton()
  easyReturnsPage.verifyPreviousReturnsPage(cargoNumber, 'true')
})

Then(/^Esen should not able to see the claim id in "([^"]*)?"$/, (value) => {
  easyReturnsPage.verifyPreviousReturnsPage(cargoNumber, 'false')
})

Then(/^Esen delete the return request$/, () => {
  easyReturnsPage.clickReturnID(cargoNumber)
  easyReturnsPage.clickReturnCodeButton()
  easyReturnsPage.clickDeleteReturnButton()
})

Then(/^Esen shoule able to create a return for same order id$/, () => {
  easyReturnsPage.clickCreateNewRequestButton()
  easyReturnsPage.clickListItems(orderID)
  easyReturnsPage.selectProductsAndReason()
  easyReturnsPage.clickNextButton()
})

Then(/^Esen goes back easy return home page$/, () => {
  easyReturnsPage.clickCloseReturnPageButton()
})

Then(/^Tolgs expires the return date as 90 days before$/, async () => {
  await legacy.updateOrderCompletedate(orderID.toString())
})

Then(/^Esen goes to create new return request page$/, () => {
  easyReturnsPage.clickCreateNewRequestButton()
})

Then(
  /^Esen should not see order number which already raised a return request$/,
  () => {
    easyReturnsPage.checkOrderNumberInReturnRequestPage(orderID)
  }
)

Then(/^Omer has view deleted return date in table$/, async () => {
  let deleteValue = await legacy.selectDeleteDateInReturnsTable(cargoNumber)
  console.log(deleteValue)
  expect(deleteValue).to.not.equal(null)
})

Then(/^Esen cannot use the deleted return id to return items$/, () => {
  easyReturnsPage.verifyPreviousReturnsPage(cargoNumber, 'false')
})

Then(/^Esen goes to easy returns page$/, () => {
  easyReturnsPage.navigateToEasyReturnsPage()
})

Then(/^Esen click dropped return id from my retrun page$/, () => {
  easyReturnsPage.clickReturnID(cargoStatus.Dropped)
})

Then(/^Esen tries to delete return request$/, () => {
  easyReturnsPage.clickReturnCodeButton()
})

Then(
  /^Esen should be not see the delete button when return process completed$/,
  () => {
    easyReturnsPage.notShowDeleteButton()
  }
)

Then(
  /^Esen should be able to see return confirmation page with unique return code for shipped items back to "(.*?)"$/,
  (address) => {
    // TODO Address kontrolü yapılması gerekiyor
    const [cargoTrackingNumber] = getTextFromElement(
      '#availableOrdersForReturnResultPage-cargoTrackingNumber'
    )
      .trim()
      .split(/\s+/)
    // TODO Address kontrolü yapılması gerekiyor
    const [cargoAddress] = getTextFromElement(
      '#availableOrdersForReturnResultPage-cargoAddress'
    ).trim()

    const cargoTrackingNumberPattern = new RegExp('^\\d{7}$')

    expect(cargoTrackingNumber).toMatch(cargoTrackingNumberPattern)
    expect(cargoTrackingNumber).not.toMatch('9023433')
    expect(cargoAddress).toMatch(productManifacturer.address)
  }
)

Then(
  /^Esen should be able to see that she should drop the products to the cargo by "(.*?)"$/,
  (expireTime) => {
    const [returnExpireDate] = getTextFromElement(
      '#availableOrdersForReturnResultPage-cargoExpireDate'
    ).trim()
    expireTime = Timer.getDateWithPlaceHolder(expireTime, 'DD-MM-YYYY')
    expect(returnExpireDate).toMatch(expireTime)
  }
)
