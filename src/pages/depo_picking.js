import setInputField from '../support/action/setInputField'
import clickElement from '../support/action/clickElement'
import selectOptionByValue from '../support/action/selectOption'
import waitForDisplayed from '../support/action/waitForDisplayed'
import DeviceLib from '../libraries/Device'
const path = require('path')

const depo_api = function () {
  const username = '#username'
  const password = '#password'
  const submitButton = '.btn-primary'
  const homePageHeader = '.page-header'

  const Toplama = `//*[text()=" Toplama"]`
  const Toplama_Oluster = `//*[contains(text(),"Oluştur")]`
  const fileUploadButton = "//*//input[@name='excelInput']"
  const excelSubmitButton = "//*[@value='Seçimleri Onayla']"
  const Batch_Oluştur = "//*[@value='Batch Oluştur']"

  const Toplama_Gather = `//*[contains(text(),"Topla")][2]`

  this.openDepoPicking = () => {
    browser.url('http://marketplace-picking-depo-test.modanisatest.com')
    // browser.pause(3000)
    waitForDisplayed(username)
    $(username).setValue('melek.tural')
    $(password).setValue('12345')
    clickElement('click', 'element', submitButton)
    waitForDisplayed(homePageHeader)
  }

  this.createNewBatch = () => {
    clickElement('click', 'element', Toplama)
    clickElement('click', 'element', Toplama_Oluster)
    clickElement('click', 'element', `//*[contains(text(),"Custom")]`)
    clickElement('doubleClick', 'element', fileUploadButton)
    browser.pause(5000)
    this.uploadFile()
    clickElement('doubleClick', 'element', excelSubmitButton)
    waitForDisplayed(Batch_Oluştur)
    clickElement('click', 'element', "//input[@value='mp']")
    clickElement('click', 'element', Batch_Oluştur)

  }

  this.uploadFile = () => {
    
    var filePath = path.join('/Users/batcave/Documents/ModanisaE2E/ui-e2e-automation/Orders.xlsx')
    var remoteFilePath = browser.uploadFile(filePath)
    $(fileUploadButton).setValue(remoteFilePath)
  }

  this.prioritizeCreatedBatch = () => {

    pickingID = $('//tr[1]/td').getText()
    // clickElement('click', 'element','//*[text()=" Monitor "]')
    // clickElement('click', 'element','//*[text()="Batch Önceliği"]'
  }

  this.markAsMissing = () => {
    // code to be written
  }

  this.markAsEscaped = () => {
    // code to be written
  }
}

module.exports = new depo_api()
