import setInputField from '../support/action/setInputField'
import selectOptionByIndex from '../support/action/selectOptionByIndex'
import clickElement from '../support/action/clickElement'
import selectOption from '../support/action/selectOption'
import DeviceLib from '../libraries/Device'
import pressButton from '../support/action/pressButton'
import isVisible from '../support/check/isDisplayed'
import isEnabled from '../support/check/isEnabled'
import isDisplayed from '../support/check/isDisplayed'
import scroll from '../support/action/scroll'
import faker from 'faker'
const checkout = require('../pages/checkout')
const expect = require('chai').expect
const assert = require('chai').assert
import waitForDisplayed from '../support/action/waitForDisplayed'

const newAddress = function () {
  const add_new_address_button =
    '//div[contains(text(),"Yeni Adres Ekle") or contains(text(),"Add New")]'
  const add_new_address_form = 'form.v-form'
  const firstName = '//input[@id="name"]'
  const surName = '//input[@id="surname"]'
  const phoneNumber = '//input[@id="phone_number_field"]'
  const deliveryUpdateMessage = '#for_delivery_updates'
  const email = '//input[@id="email"]'
  const addressTitle = '#address_title'
  const address = '#address'
  const country = '#country'
  const country_pre_selection =
    '//div[@id="country_wrapper"]//div[@class="v-select__selections"]/div' // change
  const city = '#city'
  const district = '//input[@id="district"]'
  const district_list = '//div[@role="listbox"and @id="list-29"]'
  // const individual_districts = '//div[@role="listbox"and @id="list-29"]//font'
  const individual_districts =
    '//div[@class="v-application--wrap"]/following-sibling::div[2]//div[@class="v-list-item__title"]//font/font'
  const post_code = '#post_code'
  const save_and_continue_button = '#save_and_continue_desktop'
  const close_button = '#close'
  const backButton = '#back'
  const save_and_contine_mobile =
    '//button[contains(text(),"Save and Continue") or contains(text(),"Kaydet ve Devam Et")]'
  const back_button = '#back'
  const first_name_warning =
    "//input[@id='name']/ancestor::div[@class='v-input__slot']/following-sibling::div//div[@class='v-messages__message']"
  const sur_name_warning =
    "//input[@id='surname']/ancestor::div[@class='v-input__slot']/following-sibling::div//div[@class='v-messages__message']"
  const phone_number_warning =
    "//input[@id='phone_number_field']/ancestor::div[@class='t-flex']/following-sibling::div/span"
  const email_warning =
    "//input[@id='email']/ancestor::div[@class='v-input__slot']/following-sibling::div//div[@class='v-messages__message']"
  const district_warning =
    "//input[@id='district']/ancestor::div[@class='v-input__control']//following::div[@class='v-messages__message']"
  const address_warning =
    "//textarea[@id='address']/ancestor::div[@class='v-input__slot']//following-sibling::div//div[@class='v-messages__message']"
  const city_warning =
    "//input[@id='city']/ancestor::div[@class='v-input__slot']//following-sibling::div//div[@class='v-messages__message']"
  const address_panel = '//div[@id="address-form"]/following-sibling::div'
  const addressForm = '//div[contains(text(),"Add New Address")]'
  const addressPageClose =
    '//div[@id="__layout"]//*[contains(@class,"exit-button")]'
  const addressBlockCheckoutPage = '//div[@id="address-form"]'
  const edit_address = '//button[@id="edit"]'
  const change_address = '//button[contains(text(),"Change")]'
  const change_address_window = '//div[contains(text(),"Change Address")]'
  const change_address_window_firstName = '//div[@id="name"][1]'
  const edit_second_address = '//div[@role="radiogroup"]/div[2]//button'
  const addressTitleReadOnlyInCheckoutPage = '//div[@id="address-title"]/span'

  let addressTitleAsRead
  let addressTitleChanged
  const savedAddressName = '#name'
  const savedAddressFullAddress = '#address'
  const savedAddressPhoneNumber = '#phone-number'
  const basePrice = 'div.base-price-info'
  const discountPrice = 'div.discount-info'

  this.selectedAddressInShippingPage = {
    newCheckoutAddressName: 'xyz',
    newCheckoutAddressFullAddress: '1234 Pavilion',
    newCheckoutAddressPhone: '5550000078',
  }

  const actualDistrictList = []

  const SelectedAddressName = '#name'
  const selectedAddressLineOne = '#address:nth-child(0)'
  const selectedAddressLineTwo = '#address:nth-child(1)'
  const selectedPhoneNumber = '#phone-number'
  const selectedAddressTitle = '#address-title'
  const changeAddressButton = '//button[contains(text(),"Change")]'
  const editButton = '#edit'
  const editButtonsInChageForm =
    '//div[@class = "v-input__control"]//button[contains(text(),"Edit")]'
  const confirmAddressSelection = '#confirm-selection'

  var wrong_entry_warnings = {
    phoneNumber: 'Please enter a valid mobile number.',
    email: 'Please enter a valid email address.',
    address:
      'Only Latin characters are allowed due to restrictions of global delivery.',
    district: 'This field is required.',
  }

  var address_warnings = {
    name: 'This field is required.',
    surName: 'This field is required.',
    phoneNumber: 'This field is required.',
    email: 'Please enter a valid email address.',
    city: 'This field is required.',
    district: 'This field is required.',
    address: 'This field is required.',
  }

  var address_warnings_turkey = {
    name: 'Lütfen bu alanı doldurunuz.',
    surName: 'Lütfen bu alanı doldurunuz.',
    phoneNumber: 'Lütfen bu alanı doldurunuz.',
    email: 'Lütfen geçerli bir email adresi giriniz.',
    city: 'Lütfen bu alanı doldurunuz.',
    district: 'Lütfen bu alanı doldurunuz.',
    address: 'Lütfen bu alanı doldurunuz.',
  }

  var address_minimum_test_input = 'test string'
  var name_minimum_test_input = 'a'
  var surname_minimum_test_input = 'b'

  var latestAddedAddress = []

  this.checkCountryPreSelected = (country_to_check) => {
    var count = 0
    country_to_check.forEach((text) => {
      $(country).waitForDisplayed({ timeout: 40000 })
      assert.equal($(country_pre_selection).getText(), text.toString())
      count++
    })
  }

  this.confirmNavigateToCheckoutPage = () => {
    assert.exists(address_panel)
  }

  this.getDistricts = (city_district) => {
    var cities = Object.keys(city_district)
    var districts = Object.values(city_district)
    browser.pause(3000)
    for (let i = 0; i < cities.length; i++) {
      browser.pause(2000)
      console.log('###' + cities[i])
      clickElement('click', 'element', city)
      clickElement(
        'click',
        'element',
        '//div[contains(text(),"' + cities[i] + '")]'
      )
      browser.pause(1000)
      clickElement('click', 'element', district)
      // var district_to_check = districts.split(", ");
      console.log('@@@' + districts[i])
      browser.pause(1000)
      clickElement(
        'click',
        'element',
        '//div[contains(text(),"' + districts[i] + '")]'
      )
    }

    clickElement('click', 'element', district)
    browser.pause(3000)
    let districtList = $$(individual_districts).length
    // console.log('I am in get productsIds ' + districtList[0].getText())
    console.log('district size ' + districtList)
  }

  this.selectCity = (textCity) => {
    $(city).waitForDisplayed({ timeout: 30000 })
    clickElement('click', 'element', city)
    $('//div[contains(text(),"' + textCity + '")]').waitForDisplayed({
      timeout: 30000,
    })
    clickElement(
      'click',
      'element',
      '//div[contains(text(),"' + textCity + '")]'
    )
    browser.pause(1000)
  }

  this.enterAddressFieldData = (fieldsArray) => {
    var fields = Object.keys(fieldsArray)
    var data = Object.values(fieldsArray)
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == 'phoneNumber') {
        clickElement('click', 'element', phoneNumber)
        $(phoneNumber).setValue(data[0])
        browser.keys('Tab')
        browser.pause(3000)
        assert.equal(
          $(phone_number_warning).getText(),
          wrong_entry_warnings.phoneNumber
        )
      } else if (fields[i] === 'email') {
        $(email).setValue(data[1])
        browser.keys('Tab')
        browser.pause(3000)
        assert.equal($(email_warning).getText(), wrong_entry_warnings.email)
      } else if (fields[i] === 'address') {
        $(address).setValue(data[2])
        browser.keys('Tab')
        browser.pause(3000)
        assert.equal($(address_warning).getText(), wrong_entry_warnings.address)
      }
    }
  }

  this.validateMinimumInputLength = (fieldsArray) => {
    var fields = Object.keys(fieldsArray)
    var data = Object.values(fieldsArray)

    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == 'address') {
        browser.pause(1000)
        clickElement('click', 'element', address)
        $(address).setValue(address_minimum_test_input)
        browser.keys('Tab')
      } else if (fields[i] == 'name') {
        browser.pause(1000)
        clickElement('click', 'element', firstName)
        $(firstName).setValue(name_minimum_test_input)
        browser.keys('Tab')
      } else if (fields[i] == 'surname') {
        browser.pause(1000)
        clickElement('click', 'element', surName)
        $(surName).setValue(surname_minimum_test_input)
        browser.keys('Tab')
      }
    }
    browser.pause(5000)
  }

  this.checkAddressFormForAllRequiredFields = () => {
    expect(isDisplayed(firstName)).to.be.true
    expect(isDisplayed(surName)).to.be.true
    expect(isDisplayed(phoneNumber)).to.be.true
    expect(isDisplayed(email)).to.be.true
    expect(isDisplayed(addressTitle)).to.be.true
    expect(isDisplayed(address)).to.be.true
    expect(isDisplayed(city)).to.be.true
    expect(isDisplayed(district)).to.be.true
    expect(isDisplayed(post_code)).to.be.true
    expect(isDisplayed(deliveryUpdateMessage)).to.be.true
    expect(isDisplayed(country)).to.be.true
  }

  this.checkCountryFieldIsDisabled = () => {
    console.log($(country).isEnabled())
    expect($(country).isEnabled()).to.be.false
  }

  this.enterAddressdetails = (method) => {
    $(firstName).clearValue()
    $(surName).clearValue()
    $(phoneNumber).clearValue()
    $(email).clearValue()
    $(address).clearValue()
    $(post_code).clearValue()
    setInputField(method, faker.name.firstName(), firstName)
    setInputField(method, faker.name.lastName(), surName)
    setInputField(method, faker.phone.phoneNumber('555#######'), phoneNumber)
    setInputField(method, faker.internet.email(), email)
    setInputField(method, faker.address.streetAddress(20), address)
    clickElement('click', 'element', city)
    //selectOptionByIndex(1, city)
    clickElement('click', 'element', '//div[contains(text(),"stanbul")]')
    browser.pause(3000)
    clickElement('click', 'element', district)
    clickElement('click', 'element', '//div[contains(text(),"Adalar")]')
    browser.pause(3000)
    // selectOption('text', 'Akyurt', district)
    setInputField(method, faker.address.zipCode(), post_code)
  }

  this.validateMessageForWrongEntryInAddressForm = (messageArray) => {
    var count = 0
    var textElements = [address_warning, phone_number_warning, email_warning]
    messageArray.forEach((text) => {
      expect($(textElements[count]).getText()).to.be.equal(text.toString())
      count++
    })
  }

  this.validateMaximunInputLength = (fieldsArray) => {
    var fields = Object.keys(fieldsArray)
    var data = Object.values(fieldsArray)
    for (let i = 0; i < fields.length; i++) {
      if (fields[i] == 'address') {
        clickElement('click', 'element', address)
        $(address).setValue(data[0])
        browser.keys('Tab')
        browser.pause(500)
      } else if (fields[i] === 'name') {
        $(firstName).setValue(data[1])
        browser.keys('Tab')
        browser.pause(500)
      } else if (fields[i] === 'surname') {
        $(surName).setValue(data[2])
        browser.keys('Tab')
        browser.pause(500)
      }
    }
  }

  this.CheckMaximumFieldLength = (messagearray) => {
    var count = 0
    var maxiumTextElements = [address, firstName, surName]
    messagearray.forEach((text) => {
      expect($(maxiumTextElements[count]).getValue()).to.be.equal(
        text.toString()
      )
      count++
    })
  }

  this.clickNewAddressButton = () => {
    browser.pause(3000)

    if ($(add_new_address_button).isDisplayed()) {
      clickElement('click', 'element', add_new_address_button)
    }
  }
  browser.pause(7000)

  // this.enterAddressdetails = (method) => {
  //   setInputField(method, faker.name.firstName(), firstName)
  //   setInputField(method, faker.name.lastName(), surName)
  //   setInputField(method, faker.phone.phoneNumber('555#######'), phoneNumber)
  //   setInputField(method, faker.internet.email(), email)
  //   setInputField(method, faker.address.streetAddress(20), address)
  //   selectOptionByIndex(1, city)
  //   browser.pause(3000)
  //   selectOption('text', 'Akyurt', district)
  //   setInputField(method, faker.address.zipCode(), post_code)
  // }

  this.clickSaveButton = () => {
    if (DeviceLib.isMobile()) {
      clickElement('click', 'element', save_and_contine_mobile)
    } else {
      clickElement('click', 'element', save_and_continue_button)
    }
  }

  this.clickChangeAddressButton = () => {
    clickElement('click', 'element', changeAddressButton)
  }

  this.clickBackButton = () => {
    $(backButton).waitForDisplayed({ timeout: 30000 })
    clickElement('click', 'element', backButton)
  }

  this.clickCloseButton = () => {
    $(close_button).waitForDisplayed({ timeout: 30000 })
    clickElement('click', 'element', close_button)
  }

  this.checkFieldErrorsForAllBlank = () => {
    for (var i in address_warnings) {
      if (i === address_warnings.name) {
        expect($(first_name_warning).getText()).to.equal(address_warnings.name)
      }
      if (i === address_warnings.surName) {
        expect($(sur_name_warning).getText()).to.equal(address_warnings.surName)
      }
      if (i === address_warnings.phoneNumber) {
        expect(
          $(phone_number_warning)
            .getText()
            .to.equal(address_warnings.phoneNumber)
        )
      }
      if (i === address_warnings.city) {
        expect($(city_warning).getText()).to.equal(address_warnings.city)
      }
      if (i === address_warnings.district) {
        expect($(district_warning).getText()).to.equal(
          address_warnings.district
        )
      }
    }
  }

  this.checkFieldErrorsForAllBlank_TR = () => {
    for (var i in address_warnings_turkey) {
      if (i === address_warnings_turkey.name) {
        expect($(first_name_warning).getText()).to.equal(
          address_warnings_turkey.name
        )
      }
      if (i === address_warnings_turkey.surName) {
        expect($(sur_name_warning).getText()).to.equal(
          address_warnings_turkey.surName
        )
      }
      if (i === address_warnings_turkey.phoneNumber) {
        expect($(phone_number_warning).getText()).to.equal(
          address_warnings_turkey.phoneNumber
        )
      }
      if (i === address_warnings_turkey.city) {
        expect($(city_warning).getText()).to.equal(address_warnings_turkey.city)
      }
      if (i === address_warnings_turkey.district) {
        expect($(district_warning).getText()).to.equal(
          address_warnings_turkey.district
        )
      }
    }
  }

  this.validateMessageForMinimumLengthEntryInAddressForm = (messageArray) => {
    var count = 0
    var textElements = [first_name_warning, sur_name_warning, address_warning]
    messageArray.forEach((text) => {
      expect($(textElements[count]).getText()).to.be.equal(text.toString())
      count++
    })
  }

  function clickFieldAndPressTab(selector) {
    clickElement('click', 'element', selector)
    browser.keys('Tab')
    browser.pause(2000)
  }

  this.clickAddressFieldsAndPressTab = () => {
    clickFieldAndPressTab(firstName)
    expect($(first_name_warning).getText()).to.equal(address_warnings.name)
    clickFieldAndPressTab(surName)
    expect($(sur_name_warning).getText()).to.equal(address_warnings.surName)
    clickFieldAndPressTab(phoneNumber)
    expect($(phone_number_warning).getText()).to.equal(
      address_warnings.phoneNumber
    )
    clickFieldAndPressTab(email)
    //expect($(email_warning).getText()).to.equal(address_warnings.email)
    clickFieldAndPressTab(address)
    expect($(address_warning).getText()).to.equal(address_warnings.district)
    clickFieldAndPressTab(city)
    expect($(city_warning).getText()).to.equal(address_warnings.city)
    clickFieldAndPressTab(district)
    expect($(district_warning).getText()).to.equal(address_warnings.district)
  }

  this.addressFormDefaultOpen = () => {
    expect($(addressForm).isDisplayed()).to.be.true
  }

  this.closeAddressForm = () => {
    expect($(addressForm).isDisplayed()).to.be.true
    clickElement('click', 'element', addressPageClose)
  }

  this.testWrongAddress = (addressArray) => {
    var fields = Object.keys(addressArray)
    var data = Object.values(addressArray)
    for (let i = 0; i < fields.length; i++) {
      if (!!data[i]) {
        clickElement('click', 'element', address)
        $(address).setValue(fields[i])
        browser.keys('Tab')
        assert.equal($(address_warning).getText(), wrong_entry_warnings.address)
        $(address).clearValue()
      } else {
        clickElement('click', 'element', address)
        $(address).setValue(fields[i])
        browser.keys('Tab')
        let isDisplayed = $(address_warning).isDisplayed()
        assert.isFalse(isDisplayed, 'Warning not found')
        $(address).clearValue()
      }
    }
  }

  this.editAddress = () => {
    $(addressBlockCheckoutPage).waitForDisplayed({ timeout: 40000 })
    expect(isDisplayed(edit_address)).to.be.true
    clickElement('click', 'element', edit_address)
  }

  this.changeAddress = () => {
    $(addressBlockCheckoutPage).waitForDisplayed({ timeout: 40000 })
    expect(isDisplayed(change_address)).to.be.true
    clickElement('click', 'element', change_address)
    $(change_address_window).waitForDisplayed({ timeout: 40000 })
  }

  this.readAddressTitle = () => {
    let addressTitles = $(addressTitle).getText()
    console.log(addressTitles)
    //this.addressTitleAsRead = addressTitles
    return addressTitles
  }

  var address_form_values = [
    firstName,
    surName,
    phoneNumber,
    email,
    addressTitle,
    address,
    district,
    city,
    post_code,
  ]
  this.checkAddressBlockPreEnteredValues = () => {
    browser.pause(2000)
    expect(firstName).to.not.be.null
    expect(surName).to.not.be.null
    expect(phoneNumber).to.not.be.null
    expect(email).to.not.be.null
    expect(addressTitle).to.not.be.null
    expect(address).to.not.be.null
    expect(district).to.not.be.null
    expect(city).to.not.be.null
    expect(district).to.not.be.null
    expect(post_code).to.not.be.null
  }

  this.updateAddressField_addressTitle = (value) => {
    $(addressTitle).waitForDisplayed({ timeout: 30000 })
    // clickElement('click', 'element', addressTitle)
    browser.pause(3000)
    // browser.clearElement(addressTitle)
    // addressTitle.clearElement()
    $(addressTitle).doubleClick()
    browser.keys('Backspace')
    // $(addressTitle).clearValue()
    $(addressTitle).setValue(value.toString())
    browser.pause(3000)
    this.addressTitleChanged = value.toString()
    return value.toString()
  }

  this.getFirstNameFromChangeAddressWindow = () => {
    $(change_address_window).waitForDisplayed({ timeout: 40000 })
    let firstFullName = $(change_address_window_firstName).getText()
    this.firstFullName = firstFullName
    return firstFullName
  }

  this.getAddressTitleReadOnlyInCheckoutPage = () => {
    let readAddressTitleInCheckoutPage = $(
      addressTitleReadOnlyInCheckoutPage
    ).getText()

    return readAddressTitleInCheckoutPage
  }

  this.captureAddedAddress = () => {
    ;(this.selectedAddressInShippingPage.newCheckoutAddressName = $(
      savedAddressName
    ).getText()),
      (this.selectedAddressInShippingPage.newCheckoutAddressFullAddress = $(
        savedAddressFullAddress
      ).getText()),
      (this.selectedAddressInShippingPage.newCheckoutAddressPhone = $(
        savedAddressPhoneNumber
      ).getText())
    console.log(this.selectedAddressInShippingPage)
  }

  this.fillAddressTitle = (title) => {
    $(addressTitle).clearValue()
    setInputField('add', title, addressTitle)
  }

  this.validateIfAddressIsUpdatedInCheckout = (title) => {
    expect($(selectedAddressTitle).getText()).to.equal(title)
  }

  this.CheckIfChangeOptionIsAvailable = (action) => {
    switch (action) {
      case 'should not see':
        console.log($(changeAddressButton).isExisting())
        expect(!$(changeAddressButton).isExisting()).to.be.true
        break
      case 'should see':
        console.log('2' + $(changeAddressButton).isExisting())
        expect($(changeAddressButton).isExisting()).to.be.true
        break
      default:
        console.log('not valid action')
    }
    browser.refresh() //Adding here since add form is not refreshing
  }

  this.checkIfEditOptionIsAvailable = (action) => {
    waitForDisplayed(selectedAddressTitle)
    let noOfEditButtons = $$(editButton).length
    let noOfAddress = $$(selectedAddressTitle).length
    if (noOfAddress == noOfEditButtons) {
      return true
    }
  }

  this.checkIfAddresswithTitlesExist = (titles) => {
    console.log(titles[1])
    let i = 0

    let AvailableAddresses = $$(selectedAddressTitle)
    try {
      AvailableAddresses.forEach((input) => {
        expect($(input).getText()).to.equal(titles[i][0])

        i += 1
      })
    } catch (error) {
      console.log('Miscount in available addresses')
    }
  }

  this.checkIfAddresswithTitlesExist = (titles) => {
    console.log(titles[1])
    let i = 0

    let AvailableAddresses = $$(selectedAddressTitle)
    try {
      AvailableAddresses.forEach((input) => {
        expect($(input).getText()).to.equal(titles[i][0])
        i += 1
      })
    } catch (error) {
      if (AvailableAddresses.length != titles.length) {
        console.log('Miscount in available addresses')
      }
    }
  }

  this.selectRequiredAddress = (title) => {
    clickElement(
      'click',
      'element',
      `//div[@id="address-title"]//span[contains(text(),"${title}")]`
    )
    // clickElement('click', 'element', confirmAddressSelection)
  }

  this.clickButtonInAddress = (button) => {
    if (button == 'close') {
      clickElement(
        'click',
        'element',
        '//button[contains(text(),"Close") or contains(text(),"Kapat")]'
      )
      browser.pause(2000)
    } else {
      clickElement('click', 'element', confirmAddressSelection)
      browser.pause(2000)
    }
  }

  this.checkForDiscountInOrders = (productCount) => {
    let basePrices = $$(basePrice).length
    let discountPrices = $$(discountPrice).length
    console.log('testing' + discountPrices)
    expect(discountPrices).to.equal(productCount)
    // expect(basePrices).to.equal(productCount)
  }
}

module.exports = new newAddress()
