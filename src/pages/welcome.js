import DeviceLib from '../libraries/Device'
import MobileWeb from '../Projects/MobileWeb'
import mobileWeb from '../Projects/MobileWeb'
import click from '../support/action/clickElement'
const path = require('path')
const filepath = path.join('screenshots/homepage.png')
class Welcome {
  constructor() {
    this.languageContainerSelector = '.languages'
    this.countinueButton = '#step-1-button'
  }

  selectLanguage(language = 'en') {
    if (DeviceLib.isMobile) {
      return this.selectLanguageForMobile(language)
    }
    return this.selectLanguageForDesktop(language)
  }

  selectLanguageForDesktop = (language) => {
    const languageSelector = `${this.languageContainerSelector} .${language}`
    if ($(languageSelector).isExisting()) {
      console.log('I am in select language page')
      //browser.saveScreenshot(filepath)
      click('click', 'element', languageSelector)
      click('click', 'element', this.countinueButton)
    } else {
      console.log('Select language and contry page not displayed')
      //browser.saveScreenshot(filepath)
    }
  }

  selectLanguageForMobile = (language, country) => {
    const mobileweb = new MobileWeb()
    mobileweb.selectLanguage(language, country)
  }
}

export default new Welcome()
