import clickElement from '../support/action/clickElement'
import isDisplayed from '../support/check/isDisplayed'
import click from '../support/action/clickElement'
import DeviceLib from '../libraries/Device'
const expect = require('chai').expect

const orderSplitPage = function () {
  const oldCheckoutPage = '.checkoutPage.client-type-desktop-web'
  const newCheckoutPage = 'header.header'
  const deleteItemButton = '#basketList-removePopupRemoveButton'
  const deleteItemButton_Mobile = '.confirm'
  const orderSplitSection = '#order-group'
  const shipmentHeader = 'h2.shippingMethods'
  const shipmentMethodsContainer = '.order-component-container'
  const supplierDetailsSection = '.supplier-details'
  const productsCountDetails = '.title-muted'
  const productDetailsTile = '.slick-slide .card-text'
  const supplierGroup = '.order-component-container .card'
  const orderSplits = '.order-component-container>.no-bullets'
  const standardDelivery = `//div[contains(text(),'Standard Delivery')]`
  const radioInputForDeliveryType = `[role~='radiogroup']`
  const estimatedDeliveryTag = `//div[contains(text(),'Estimated Delivery')]`
  const orderComponenet = '.order-component-container'
  const orderSummarySection = '#order-summary-shipment'

  this.validationOnCheckoutPage = (pageType) => {
    if (pageType == 'old') {
      $(oldCheckoutPage).waitForDisplayed({ timeout: 30000 })
      expect(isDisplayed(oldCheckoutPage)).to.be.true
    } else if (pageType == 'new') {
      $(newCheckoutPage).waitForDisplayed({ timeout: 30000 })
      expect(isDisplayed(newCheckoutPage)).to.be.true
      expect(isDisplayed(orderComponenet)).to.be.true
      expect(isDisplayed(orderSummarySection)).to.be.true
    } else {
      console.warn('The page type is not valid')
    }
  }

  this.removeProduct = (productName) => {
    if (DeviceLib.isMobile()) {
      browser.pause(3000)
      const removeItem = `//*[contains(text(),"${productName}")]//ancestor::section//a[@class="basketProductList-itemDelete "]`
      click('click', 'element', removeItem)
      $(deleteItemButton_Mobile).waitForDisplayed({ timeout: 30000 })
      browser.pause(2000)
      clickElement('click', 'element', deleteItemButton_Mobile)
    } else {
      const removeItem = `//*[contains(text(),"${productName}")]//ancestor::li//a[@class="basketList-removeBasket "]`
      click('click', 'element', removeItem)
      $(deleteItemButton).waitForDisplayed({ timeout: 30000 })
      browser.pause(2000)
      clickElement('click', 'element', deleteItemButton)
    }
    browser.pause(2000)
  }

  this.verifyShipmentMethodsSection = () => {
    browser.pause(7000)
    $(orderSplitSection).waitForDisplayed({ timeout: 30000 })
    expect(isDisplayed(orderSplitSection)).to.be.true
    expect(isDisplayed(shipmentHeader)).to.be.true
    expect(isDisplayed(shipmentMethodsContainer)).to.be.true
    // expect(isDisplayed(supplierDetailsSection)).to.be.true
    expect(isDisplayed(productsCountDetails)).to.be.true
    expect(isDisplayed(productDetailsTile)).to.be.true
  }

  this.validateSupplierGroups = (expectedGroupCount) => {
    const actualGroupCount = $$(supplierGroup).length
    expect(expectedGroupCount).to.equals(actualGroupCount)
  }
  this.checkIfEstimatedDeliveryIsVisible = () => {
    $(orderSplits).waitForDisplayed({ timeout: 30000 })
    let orders = $$(orderSplits)
    console.log(orders.length + 'orderlength')
    browser.pause(3000)
    let estimatedDelivery = $$(estimatedDeliveryTag).length
    console.log(estimatedDelivery + 'estDel')
    browser.pause(6000)
    return orders.length === estimatedDelivery
  }

  this.checkIfStandardDeliveryOptionIsAvailable = () => {
    console.log('I am in checkMethod')
    $(orderSplits).waitForDisplayed({ timeout: 30000 })
    let orders = $$(orderSplits)
    console.log(orders.length + 'orderlength')
    browser.pause(6000)
    return (
      orders.length === $$(radioInputForDeliveryType).length &&
      orders.length === $$(standardDelivery).length
    )
  }
}
module.exports = new orderSplitPage()
