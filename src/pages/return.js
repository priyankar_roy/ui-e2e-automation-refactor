import {
  createOrder,
  createOrderCargoData,
  createOrderCargoStatus,
} from './../external/legacy/create_order'

import clickElement from '../support/action/clickElement'

class Return {
  constructor() {
    this.createdOrderId = null
  }

  async createOrderCargoData(orderId = null) {
    return createOrderCargoData(orderId || this.createdOrderId)
  }

  async createOrderCargoStatus(orderId = null) {
    return createOrderCargoStatus(orderId || this.createdOrderId)
  }

  async createModanisaOrder() {
    const orderId = await createOrder()

    return (this.createdOrderId = orderId)
  }

  async clickCreateNewReturnButton() {
    return (await $('.easyReturnPage-button-primary')).click()
  }

  async checkHasReturnableOrder() {
    const orderId = this.createdOrderId
    const selector = `//*[@class="returnableOrderItems"]//*[contains(text(),"${orderId}")]`

    return (await $(selector)).isExisting()
  }
}

export default new Return()
