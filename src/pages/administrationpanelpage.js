import setInputField from '../support/action/setInputField'
import clickelement from '../support/action/clickElement'
import isVisible from '../support/check/isDisplayed'
import checkElementExist from '../support/check/checkElementExists'
import yurtici from '../external/yurtici'
import legacy_database from '../external/legacy_database'
import legacy_depo_api from '../external/legacy_depo_api'
const expect = require('chai').expect

const administrationPanel = function () {
  const userName = '#username'
  const passWord = '#password'
  const loginButton = '.form-signin button'
  const logoutButton =
    '#navbar > ul.nav.navbar-nav.navbar-right > li.active > a'
  const monitorTab = '#navbar ul:nth-child(1) .dropdown:nth-child(6)'
  const cargoErrorLink = `//a[contains(text(),"Kargo Hataları")]`
  const errorTab = '#errorTab'
  const errorDescLink =
    'body > div.container > div.row > div.panel.panel-primary > div.panel-body > table > tbody > tr:nth-child(2) > td:nth-child(3) > a'
  const orderIdText =
    'body > div.container > div.container-fluid > div:nth-child(5) > table > tbody > tr:nth-child(2) > td:nth-child(1) > a'
  const CozumIste =
    'body > div.container > div.container-fluid > div:nth-child(5) > table > tbody > tr:nth-child(2) > td:nth-child(8) > button'
  const errorStringSelector =
    'body > div.container > div.container-fluid > div:nth-child(5) > table > tbody > tr:nth-child(2) > td:nth-child(6)'
  const cargoLabelLink =
    'body > div > div:nth-child(3) > div.col-lg-12.order-label > div > p:nth-child(3) > a'

  var errorString = ''
  var orderErrorPageLink = ''

  const legacyData = {
    cargoName: 'yurtici',

    validCityId: 17,
    invalidCityId: 0,

    validCellNumber: '905412639143',
    invalidCellNumber: '',
  }

  this.prepareData = (orderId, cellNumber, cityId) => {
    yurtici.cancelOrder(orderId)
    legacy_database.updateOrderShipCityIdAndCellNumber(
      orderId,
      cellNumber,
      cityId
    )
  }

  this.prepareOrderForCargoErrorInvalidCellnumber = (orderId) => {
    legacy_database.closeCargoErrors(orderId)
    this.prepareData(
      orderId,
      legacyData.invalidCellNumber,
      legacyData.validCityId
    )
  }

  this.prepareOrderForCargoErrorInvalidCity = (orderId) => {
    this.prepareData(
      orderId,
      legacyData.validCellNumber,
      legacyData.invalidCityId
    )
  }

  this.prepareOrderWithNoCargoError = (orderId) => {
    this.prepareData(
      orderId,
      legacyData.validCellNumber,
      legacyData.validCityId
    )
  }

  this.triggerShipmentForOrder = (orderId) => {
    legacy_depo_api.shipment(orderId, legacyData.cargoName)
  }

  this.enterUserDetails = (method, name, password) => {
    setInputField(method, name, userName)
    setInputField(method, password, passWord)
  }

  this.clickLoginButton = () => {
    clickelement('click', 'element', loginButton)
  }

  this.clickLogoutButton = () => {
    clickelement('click', 'element', logoutButton)
  }

  this.clickCargoErrorDropDown = (method) => {
    clickelement(method, 'element', monitorTab)
    clickelement(method, 'element', cargoErrorLink)
  }

  this.verifyCargoErrorTableVisible = () => {
    expect(isVisible(errorTab)).to.be.true
  }

  this.clickCargoErrorLink = (method) => {
    clickelement(method, 'element', errorDescLink)
  }

  this.verifyOrderIDFromCargoErrorPage = (orderId) => {
    expect($(orderIdText).getText()).to.be.equal(orderId)
    errorString = $(errorStringSelector).getText()
    orderErrorPageLink = browser.getUrl()
  }

  this.cozumIsteButton = (method) => {
    expect(isVisible(CozumIste)).to.be.true
    browser.pause(2000)
  }

  this.orderErrorDetailLink = (method) => {
    clickelement('click', 'element', orderErrorDetail)
    browser.pause(2000)
  }

  this.noCozumIste = () => {
    browser.url(orderErrorPageLink)
    browser.pause(5000)
    checkElementExist('true', CozumIste)
  }

  this.seeNewCargoError = (orderId) => {
    expect($(orderIdText).getText()).to.be.equal(orderId)
    var newErrorString = $(errorStringSelector).getText()
    expect(newErrorString).not.be.equal(errorString)
  }

  this.seeCargoLabelLink = () => {
    checkElementExist('an', cargoLabelLink)
  }
}

module.exports = new administrationPanel()
