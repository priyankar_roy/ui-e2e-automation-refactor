import setInputField from '../support/action/setInputField'
import clickElement from '../support/action/clickElement'
import waitfordisplayed from '../support/action/waitForDisplayed'
import getTexts from '../support/action/getTextFromElement'
import waitForUntil from '../support/check/waitForUntil'
import DeviceLib from '../libraries/Device'
import homepage from '../pages/homepage'
import waitforuntill from '../support/check/waitForUntil'
const path = require('path')

const addproducts = function () {
  const searchbutton = '#newHeaderFooter-search'
  const firstProductLab =
    '#productsList >li:nth-child(1) .info-container a:nth-child(1)'
  const firstProductLab_mobile =
    '.productListing .productListing-item:nth-child(1) a .productListing-itemBrand'
  const selectSizeDropDownOptions = '#size-box-container select option'
  const selectSizeDropDown = '#size-box-container select'
  // const addToBasketButton = '.basket-button'
  const addToBasketButton =
    "(//*[contains(text(),'Add to Basket') or contains(text(),'Sepete Ekle')])[1]"
  const searchTextBox = '#search.textbox'
  const searchBoxLink_mobile = '#pageHeader-searchOpener'
  const searchTextBox_mobile = '.searchBox-input'
  const searchBoxButton_mobile = '.searchBox-button'
  const activeBasket = '.newHeaderFooter-basket-active'
  const dropDownForOtherCountry = '.productSizeBoxes-tabHeadWrapper'
  const dropdownSizeChartForEU =
    '.productSizeBoxes-tabHeadWrapper .productSizeBoxes-tabHead:nth-child(1)'
  // const selectSizeForOtherCountry = "(//div[@id='size-box-container']//select)[2]/option"
  const removeProductsIcon = '.basketList-removeBasket'
  const removeProductsIcon_mobile = '.basketProductList-itemDelete'
  const deleteItemButton = '#basketList-removePopupRemoveButton'
  const deleteItemButton_mobile = '.confirm'
  const shopMoreButton = '.basketPage-emptyButton'
  const actualProductID = []
  const basketSize = '#basketTitle - itemCount'
  const dropdownOptions_mobile = '#productSizeSlider-select option'
  const dropdownBox_mobile = '#productSizeSlider-select'
  const activeBasket_mobile = '#addedToBasketGoToBasketButton'
  let basketSizeCount = 0
  const modanisaLogo = "//a[contains(@data-action,'Logo')]"

  this.searchProducts = (method, value) => {
    if (DeviceLib.isMobile()) {
      browser.pause(5000)
      waitforuntill(searchBoxLink_mobile)
      clickElement('click', 'element', searchBoxLink_mobile)
      setInputField(method, value, searchTextBox_mobile)
    } else {
      setInputField(method, value, searchTextBox)
    }
  }

  this.clickSearchButton = () => {
    if (DeviceLib.isMobile()) {
      clickElement('click', 'element', searchBoxButton_mobile)
    } else {
      clickElement('click', 'element', searchbutton)
      const filepath = path.join('screenshots/addproductsPage.png')
      //browser.saveScreenshot(filepath)
    }
  }

  this.selectProduct = () => {
    if (DeviceLib.isMobile()) {
      try {
        clickElement('click', 'element', firstProductLab_mobile)
      } catch (error) {
        console.log(error)
      }
    } else {
      waitForUntil(firstProductLab)
      clickElement('click', 'element', firstProductLab)
    }
  }

  this.getBasketSize = () => {
    basketSizeCount = getTexts(basketSize)
    return basketSizeCount
  }

  this.getProductIDs = () => {
    let checkProductID = $$(
      '#basketList-list > li>div.basketList-itemInfo > dl > dd:nth-child(4)'
    )
    //console.log('List ProductIds' + checkProductID[0].getText())

    checkProductID.forEach((productID) => {
      actualProductID.push(productID.getText())
    })
  }

  this.selectSizeFromDropdown = () => {
    browser.pause(5000)
    if (DeviceLib.isMobile()) {
      dynamicDropdownSelection_mobile(
        dropdownOptions_mobile,
        dropdownBox_mobile
      )
    } else {
      if ($(dropDownForOtherCountry).isDisplayed()) {
        clickElement('click', 'element', dropdownSizeChartForEU)
        dynamicDropdownSelection(selectSizeDropDownOptions, selectSizeDropDown)
      } else {
        dynamicDropdownSelection(selectSizeDropDownOptions, selectSizeDropDown)
      }
    }
  }

  let dynamicDropdownSelection = (selector, dropdownSelector) => {
    let size = $$(selector)
    for (let i = 1; i <= size.length; i++) {
      clickElement('click', 'element', dropdownSelector)
      if (size[i].isEnabled() == true) {
        size[i].click()
        break
      } else {
        continue
      }
    }
  }

  let dynamicDropdownSelection_mobile = (selector, dropdownSelector) => {
    let size = $$(selector)
    for (let i = 1; i <= size.length; i++) {
      console.log('Dropdown value' + size[i].getText())
      clickElement('click', 'element', dropdownSelector)
      browser.pause(2000)
      if (size[i].isEnabled() == true) {
        console.log('i am in enabled section')
        size[i].click()
        clickElement('click', 'element', dropdownSelector)
        break
      } else {
        continue
      }
    }
  }

  this.clickAddToBasketButton = () => {
    $(addToBasketButton).waitForDisplayed({ timeout: 30000 })
    // waitForUntil(addToBasketButton)
    clickElement('click', 'element', addToBasketButton)
    //browser.saveScreenshot('screenshots/addtobasketpage.png')
    if (DeviceLib.isMobile()) {
      waitForUntil(activeBasket_mobile, true)
      browser.pause(2000)
      clickElement('click', 'element', modanisaLogo)
      //waitfordisplayed(activeBasket_mobile, true)
    } else {
      // clickElement('click', 'element', 'button.confirm') //temporary addition for The item has not been added to your basket!
      // waitfordisplayed(activeBasket, true)
    }
  }

  this.removeAllProductsFromBasket = () => {
    if (DeviceLib.isMobile()) {
      if ($(removeProductsIcon_mobile).isDisplayed()) {
        let productSize = $$(removeProductsIcon_mobile)
        productSize.forEach((removeItem) => {
          removeItem.click()
          $(deleteItemButton_mobile).waitForDisplayed({ timeout: 30000 })
          browser.pause(2000)
          clickElement('click', 'element', deleteItemButton_mobile)
        })
        clickElement('click', 'element', shopMoreButton)
      } else {
        clickElement('click', 'element', shopMoreButton)
      }
    } else {
      if ($(removeProductsIcon).isDisplayed() == true) {
        let productSize = $$(removeProductsIcon)
        productSize.forEach((removeItem) => {
          removeItem.click()
          $(deleteItemButton).waitForDisplayed({ timeout: 30000 })
          clickElement('click', 'element', deleteItemButton)
          browser.pause(5000)
        })
        clickElement('click', 'element', shopMoreButton)
      } else {
        //browser.saveScreenshot('screenshots/basketPage.png')
        waitForUntil(shopMoreButton)
        clickElement('click', 'element', shopMoreButton)
      }
    }
  }
}
module.exports = new addproducts()
