import clickElement from '../support/action/clickElement'
import openWebsite from '../support/action/openWebsite'
import DeviceLib from '../libraries/Device'
import waitForDisplayed from '../support/action/waitForDisplayed'
import psl from 'psl'

const myAccountSelection = function () {
  const myOrders = '.hover-my-orders'
  const myAccount = '#account'
  const logoutLink = '.hover-logout'
  const myAccountButton_mobil = '#pageHeader-myaccountIcon'
  const logoutLink_mobil = '.accountMenu-itemLink-logout'
  const signInSection = '#sign'

  this.myAccountButton = () => {
    if (DeviceLib.isMobile()) {
      clickElement('click', 'element', myAccountButton_mobil)
    } else {
      waitForDisplayed(myAccount, true)
      clickElement('click', 'element', myAccount)
    }
  }

  this.orderDetails = () => {
    clickElement('click', 'element', myOrders)
  }

  this.logout = () => {
    if (DeviceLib.isMobile()) {
      browser.pause(2000)
      clickElement('click', 'element', logoutLink_mobil)
      waitForDisplayed(myAccountButton_mobil)
    } else {
      browser.pause(2000)
      clickElement('click', 'element', logoutLink)
      waitForDisplayed(signInSection)
    }
  }

  this.navigateToHomePage = () => {
    let url = DeviceLib.getUrl()
    if (DeviceLib.isMobile()) {
      openWebsite(url, '?hide_debug_panel=1')
    } else {
      openWebsite(url, '')
    }
  }
}

module.exports = new myAccountSelection()
