import clickElement from '../support/action/clickElement'
import openWebsite from '../support/action/openWebsite'
import isVisible from '../support/check/isDisplayed'
import ordersuccesspage from '../pages/ordersuccesspage'
import DeviceLib from '../libraries/Device'
import newAddress from '../pages/newAddressPage'
import legacydatabase from '../external/legacy_database'

const expect = require('chai').expect

const XLSX = require('xlsx')

const ordersPageFlow = function () {
  var orderID
  const myOrders = '.hover-my-orders'
  const myAccount = '#account'
  const orderReview =
    '#regkr > div > div.siptable > div:nth-child(6) > div:nth-child(5) > a'
  const orderInfo = '.aratable'
  const updateOrderList = '.orders-refresh-form input'
  const latestOrderStatusInMobile = '.cargo-status-block>span:nth-child(2)'
  const latestOrderStatusInDesktop = 'div .cols:nth-child(2)'
  const reviewlinkForlatestOrder = `//*[contains(text(),'Review')]`
  const returnItemsButton = '.returnableOrderButton-return'
  const myOrderLink = 'a#order-detail-link'
  const ordersPageAddressName =
    '//div[@class="information-box"]//div[2]//child::div[2]//p[1]'
  const ordersPageAddressFullAddress =
    '//div[@class="information-box"]//div[2]//child::div[2]//p[2]'

  this.myAccountButton = () => {
    clickElement('click', 'element', myAccount)
  }

  this.orderDetails = () => {
    clickElement('click', 'element', myOrders)
  }

  this.orderReviewPage = () => {
    clickElement('click', 'element', orderReview)
  }

  this.orderInformation = () => {
    browser.pause(2000)
    expect(isVisible(orderInfo)).toBeTruthy
  }

  this.navigateToMyOrder = () => {
    browser.url(`${DeviceLib.getUrl()}/membership/account/orders/`)
    //openWebsite(null, 'membership/account/orders/')
  }

  this.validateOrderIDs = () => {
    var expectedOrderID = ordersuccesspage.getOrderID()
    //clickElement('click','element',updateOrderList)
    for (let i = 0; i < expectedOrderID.length; i++) {
      var actualOrderID = expectedOrderID[i]
      expect($(`//*[contains(text(),"${actualOrderID}")]`).isDisplayed()).to.be
        .true
    }
  }

  this.validateStatusOfLatestOrder = () => {
    let status
    if (DeviceLib.isMobile()) {
      status = $(latestOrderStatusInMobile).getText()
    } else {
      status = $(latestOrderStatusInDesktop).getText()
    }
    console.log('Status:' + status)

    return status
  }

  this.reviewAvailableProducts = () => {
    clickElement('click', 'element', reviewlinkForlatestOrder)
    browser.pause(3000)
    let noOfProductsAvailable = $$(`tbody>tr>td.name`).length
    return noOfProductsAvailable
  }

  this.clickOrderDetails = (orderId) => {
    clickElement(
      'click',
      'element',
      `//*[contains(text(),"Order Number: #${orderId}")]`
    )
  }

  this.clickReturnItems = () => {
    clickElement('click', 'element', returnItemsButton)
  }
  ////div[@class="returnableOrderItem-detailLine"]//*[text()="Order #70410246"]

  this.ordersPageAddressValidation = () => {
    this.ordersPageSelectedAddress = {}
    this.ordersPageSelectedAddress = newAddress.selectedAddressInShippingPage
    clickElement('click', 'element', reviewlinkForlatestOrder)
    console.log(this.ordersPageSelectedAddress)

    expect($(ordersPageAddressName).getText()).to.be.equal(
      this.ordersPageSelectedAddress.newCheckoutAddressName
    )
    // let ordersPageAddress = []
    // ordersPageAddress = this.ordersPageSelectedAddress.newCheckoutAddressFullAddress.split(
    //   ','
    // )
    // ordersPageAddress = ordersPageAddress[0]
    // console.log(ordersPageAddress)
    expect($(ordersPageAddressFullAddress)).to.be.contains(ordersPageSelectedAddress.newCheckoutAddressFullAddress)
  }

  this.createXLSXSsheet = () => {
   orderID = ordersuccesspage.captureOrderID()
   console.log("orderIDs generated = " + orderID)

    var data = {}
    var workBook = XLSX.utils.book_new()
    data = {
      '!ref': 'A1:A1',
      A1: { v: orderID[0] },
    }
    XLSX.utils.book_append_sheet(workBook, data, 'Sheet1')
    XLSX.writeFile(workBook, 'Orders.xlsx')
  }

  this.verifyOrderStatus = () => {

    legacydatabase.setOrderStatus(orderID[0],11)



  }
}

module.exports = new ordersPageFlow()
