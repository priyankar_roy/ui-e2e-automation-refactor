import setInputField from '../support/action/setInputField'
import clickElement from '../support/action/clickElement'
import selectOptionByValue from '../support/action/selectOption'
import waitForDisplayed from '../support/action/waitForDisplayed'
import DeviceLib from '../libraries/Device'

const checkOutProducts = function () {
  var basketValues
  //  const basketButton = '//*[text()="Sepetim"]'
  const basketButton =
    '(//*[contains(text(),"Basket") or contains(text(),"Sepetim")])[1]'
  const basketButton_web =
    '(//a[contains(text(),"Basket") or contains(text(),"Sepetim")])[1]'
  const activeBasket = '.newHeaderFooter-basket-active'
  //const securePaymentButton = '.basketPage-buyButton'
  const securePaymentButton =
    '//*[text()="Güvenle Satın Al" or text()="Secure Payment"]'
  const placeOrderButton = '.checkoutSinglePageFooter-button'

  //basket total
  const basketTotal =
    '//span[contains(text(),"Sepet Toplam") or contains(text(),"Basket Total")]//following-sibling::span'
  const basketShippingFee =
    '//span[contains(text(),"Kargo Bedeli") or contains(text(),"Shipping Fee")]//following-sibling::span'
  const basketDiscount =
    '//span[contains(text(),"İndirim") or contains(text(),"Discount")]//following-sibling::span'
  const basketTotalAmount =
    "//span[@class='basketSummary-label basketSummary-label-totalPrice' and (contains(text(),'Toplam') or contains(text(),'Total') )]//following-sibling::span"
  const subTotal =
    '//span[contains(text(),"Sepet Toplam") or contains(text(),"Sub Total")]//following-sibling::span'
  const discountCouponDropDown = '.basketCoupon-combobox'
  const discountCouponDropdown_Mobile = '#basketPageContent-couponCodeSelect'
  const counponTextBox = '.basketCoupon-textInput'
  const couponButton = 'a.basketCoupon-button'
  const DiscountLabel = '.basketSummary-label-couponDiscount'
  const couponCancelButton = 'a.basketSummary-couponCodeCancel'
  const couponCancelConfirm = '#basketSummaryRemoveCouponPopup-button-delete'
  const paymentCardName = '#cardOwnerName'

  const getBasketValues = () => {
    const basketValues = {
      subTotal: checkSubtotalValue(),
      couponDiscount: checkDiscountCouponValue(),
      shippingFee: $(basketShippingFee).getText(),
      Total: $(basketTotalAmount).getText(),
    }
    return basketValues
  }

  let checkSubtotalValue = () => {
    let subTotalValue
    if ($(basketDiscount).isDisplayed()) {
      subTotalValue = $(subTotal).getText()
    } else {
      subTotalValue = $(basketTotal).getText()
    }
    return subTotalValue
  }
  let checkDiscountCouponValue = () => {
    let discountValue
    if ($(basketDiscount).isDisplayed()) {
      discountValue = $(basketDiscount).getText()
    } else {
      //discountValue = '0,00 TL'
      discountValue = 'False'
    }
    return discountValue
  }

  this.clickBasketButton = () => {
    if (DeviceLib.isMobile()) {
      clickElement('click', 'element', basketButton)
    } else {
      //waitForDisplayed(activeBasket)
      clickElement('click', 'element', basketButton_web)
    }
  }

  this.checkBasketOutValues = () => {
    basketValues = Object.create(getBasketValues())
  }

  this.getBasketValues = () => {
    return basketValues
  }

  this.clickSecurePaymentButton = () => {
    $(securePaymentButton).waitForDisplayed({ timeout: 30000 })
    clickElement('click', 'element', securePaymentButton)
  }

  this.clickPlaceOrderButton = () => {
    waitForDisplayed(placeOrderButton, true)
    
    clickElement('click', 'element', placeOrderButton)
  }

  let enterCounponCode = (couponCodeType) => {
    selectOptionByValue('text', couponCodeType, discountCouponDropDown)
    setInputField('add', 'OtomasyonYuzde', counponTextBox)
    clickElement('click', 'element', couponButton)
    browser.pause(2000)
  }

  let SelectCouponCode = (couponCodeType) => {
    if (DeviceLib.isDesktop()) {
      selectOptionByValue('text', couponCodeType, discountCouponDropDown)
      clickElement('click', 'element', couponButton)
    } else {
      selectOptionByValue('text', couponCodeType, discountCouponDropdown_Mobile)
    }

    browser.pause(2000)
  }
  this.checkForAlreadyAppliedCoupon = () =>{
    if($(DiscountLabel).isDisplayed())
    {
      clickElement('click','element',couponCancelButton)
      clickElement('click','element',couponCancelConfirm)
    }else{
      console.log('No coupon is applied')
    }
  }

  this.enterCounponCodeByType = (couponCode) => {
    switch (couponCode) {
      case 'I have another coupon':
        enterCounponCode(couponCode)
        break
      case 'FixedPercentage':
        SelectCouponCode(couponCode)
        break
      case 'fixedAmount20':
        SelectCouponCode(couponCode)
        break
      case 'FIXEDPRICE':
        SelectCouponCode(couponCode)
        break
      default:
        console.log('No valid couponcode')
    }
     if($(`//*[contains(@text,'OK')]`).isDisplayed()){
       console.log('Closing Coupon popup')
       clickElement('click','element',`//*[contains(@text,'OK')]`)
}
  }
}
module.exports = new checkOutProducts()
