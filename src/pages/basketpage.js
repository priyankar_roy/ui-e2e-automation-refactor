import { expect } from 'chai'
import getTexts from '../support/action/getTextFromElement'
const chai = require('chai').expect
const emptyBasket = '//*[text()="Shopping basket empty."]'

const basketPage = function () {
  let actualProductID = []
  let productIDsAfterSignIn = []
  let basketSizeCount = 0
  const basketSize = '#basketTitle - itemCount'
  

  this.getBasketSize = () => {
    basketSizeCount = getTexts(basketSize)
    return basketSizeCount
  }

  this.getProductIDsBeforeSignin = () => {
    let checkProductID = $$(
      '#basketList-list > li>div.basketList-itemInfo > dl > dd:nth-child(4)'
    )
    checkProductID.forEach((productID) => {
      actualProductID.push(productID.getText())
    })
    console.log('ProductIDs: ' + actualProductID)
  }

  this.getProductIDsAfterSignIn = () => {
    let checkProductID = $$(
      '#basketList-list > li>div.basketList-itemInfo > dl > dd:nth-child(4)'
    )
    checkProductID.forEach((productID) => {
    productIDsAfterSignIn.push(productID.getText())
    })

    console.log(productIDsAfterSignIn + 'array')
  }

  this.checkifbaskethaspreselectedproducts = () => {
    let result = true
    for (let i = 0; i < actualProductID.length; i++) {
      if (!productIDsAfterSignIn.includes(actualProductID[i])) {
        result = false
      }
      
    }
    expect(result).to.be.true
  }

  this.checkBasketIsEmpty = () =>{
expect($(emptyBasket).isDisplayed()).to.be.true
  }
}
module.exports = new basketPage()
