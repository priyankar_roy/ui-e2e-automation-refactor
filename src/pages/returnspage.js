import clickElement from '../support/action/clickElement'
import selectInputFromDropdown from '../support/action/selectOption'
import getText from '../support/action/getTextFromElement'
import setInputField from '../support/action/setInputField'
import openWebsite from '../support/action/openWebsite'

const expect = require('chai').expect

const easyReturnsPageFlow = function () {
  const selctProductButton = '#availableOrdersForReturnPage-form'
  const selectReason = '.returnableProduct-select'
  const nextButton = '#returnableProduct-nextButton'
  const confirmButton = '#returnableProduct-doReturnButtonNormalText'
  const processCompleteText = '.availableOrdersForReturnResultPage-status'
  const returnCodeSection =
    '#availableOrdersForReturnResultPage-trackingWrapper'
  const returnCode = '#availableOrdersForReturnResultPage-cargoTrackingNumber'
  const cargoLabel = '.availableOrdersForReturnResultPage-logo'
  const sendSMSButton = '#availableOrdersForReturnResultPage-sendBySmsButton'
  const returnFormPage = '.returnFormPage'
  const expiryStatement = '.availableOrdersForReturnResultPage-cargoExpireDate'
  const returnExpiryDate = '.availableOrdersForReturnResultPage-cargoExpireDate'
  const returnFormFieldsSection = '.returnFormPage-formFileds'
  const returnFormName = '#returnFormPage-formInput-fullName'
  const returnFormIBAN = '#returnFormPage-formInput-iban'
  const returnFormBankName = '#returnFormPage-formInput-bankName'
  const returnFormBranchName = '#returnFormPage-formInput-branchName'
  const returnCloseButton = '.availableOrdersForReturnResultPage-close'
  const showPastReturnsbutton =
    '.easyReturnPage-button-secondary.trackEvent:nth-child(2)'
  const viewReturnCodeButton = '#myReturnsDetail-viewReturnCode'
  const deleteRequestButton = '.myReturnsDetail-deleteRequest'
  const confirmButtonDelete = '.confirm'
  const createNewRequestButton = '.easyReturnPage-button-primary'
  const returnItemsButton = '.returnableOrderButton-return'

  let date
  let returnIDSelector

  this.clickListItems = (orderID) => {
    clickElement(
      'click',
      'element',
      `//*[text()='Order #${orderID}']//following::a[1]`
    )
  }

  this.selectProductsAndReason = () => {
    clickElement('click', 'element', selctProductButton)
    selectInputFromDropdown('text', 'Incorrect item delivered', selectReason)
  }

  this.clickNextButton = () => {
    clickElement('click', 'element', nextButton)
  }

  this.verifyReturnFormPage = () => {
    expect($(returnFormPage).isDisplayed()).to.be.true
  }

  this.clickConfirmButton = () => {
    browser.pause(2000)
    clickElement('click', 'element', confirmButton)
  }

  this.assertText = (text) => {
    $(processCompleteText).waitForDisplayed({ timeout: 30000 })
    expect(text.toLowerCase()).to.equal(
      getText(processCompleteText).toLowerCase()
    )
  }

  this.verifyReturnCodeSection = () => {
    $(returnCodeSection).isDisplayed()
    $(returnCode).isExisting()
    $(cargoLabel).isExisting()
    $(sendSMSButton).isExisting()
  }

  this.verifyReturnExpiryDate = (cargoMessage) => {
    if ($(expiryStatement).isDisplayed()) {
      expect(getText(expiryStatement)).to.contain(cargoMessage)
    } else {
      console.error('Date is not displayed')
    }
  }

  this.checkAndEnterIBANDetails = () => {
    try {
      if ($(returnFormFieldsSection).isDisplayed() == true) {
        setInputField('add', 'tester', returnFormName)
        setInputField('add', 'TR 8200 0100 1745 0000 2868 5238', returnFormIBAN)
        setInputField('add', 'ak bank', returnFormBankName)
        setInputField('add', 'testing', returnFormBranchName)
      } else {
        console.warn('No IBAN Details')
      }
    } catch (err) {}
  }

  this.getCargoReturnNumber = () => {
    let cargoNumber = getText(returnCode)
    cargoNumber = cargoNumber.substr(0, 6)
    return cargoNumber
  }

  this.clickCloseReturnPageButton = () => {
    clickElement('click', 'element', returnCloseButton)
  }

  this.clickShowPastReturnsButton = () => {
    clickElement('click', 'element', showPastReturnsbutton)
  }

  this.verifyPreviousReturnsPage = (claimID, bool) => {
    browser.pause(5000)
    returnIDSelector = `//*[@class='myReturnsItem-returnNumberValue' and text()="#${claimID}"]`
    switch (bool) {
      case 'true':
        expect($(returnIDSelector).isExisting()).to.be.true
        break
      case 'false':
        browser.pause(5000)
        browser.refresh()
        expect($(returnIDSelector).isExisting()).to.be.false
        break
      default:
        console.error('No values mathced for assertion')
    }
  }

  this.clickReturnID = (claimID) => {
    clickElement(
      'click',
      'element',
      `//*[text()='#${claimID}']//following::a[1]`
    )
  }

  this.clickReturnCodeButton = () => {
    clickElement('click', 'element', viewReturnCodeButton)
  }

  this.clickDeleteReturnButton = () => {
    browser.pause(3000)
    clickElement('click', 'element', deleteRequestButton)
    browser.pause(3000)
    clickElement('click', 'element', confirmButtonDelete)
  }

  this.clickCreateNewRequestButton = () => {
    clickElement('click', 'element', createNewRequestButton)
  }

  this.verifyReturnsButton = (viewType) => {
    switch (viewType.toLowerCase()) {
      case 'see':
        expect($(returnItemsButton).isExisting()).to.be.true
        break
      case 'not see':
        browser.pause(2000)
        browser.refresh()
        browser.pause(1000)
        expect($(returnItemsButton).isExisting()).to.be.false
        break
      default:
        console.error('View type is undefined')
    }
  }

  this.checkOrderNumberInReturnRequestPage = (orderID) => {
    let notSeenOrderId = `//*[text()='Order #${orderID}']//following::a[1]`
    expect($(notSeenOrderId).isExisting()).to.be.false
  }

  this.navigateToEasyReturnsPage = () => {
    const url = browser.getUrl()
    browser.options.baseUrl = url
    openWebsite('', 'membership/account/my-returns/')
  }

  this.notShowDeleteButton = () => {
    browser.pause(3000)
    expect($(deleteRequestButton).isDisplayed()).to.be.false
  }
}

module.exports = new easyReturnsPageFlow()
