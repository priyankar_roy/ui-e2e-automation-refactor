import faker from 'faker'
import openWebsite from '../support/action/openWebsite'
import click from '../support/action/clickElement'
import setInputField from '../support/action/setInputField'
import checkElementExist from '../support/check/checkElementExists'
import DeviceLib from '../libraries/Device'

class RegistrationForm {
  constructor() {
    this.languageContainerSelector = '.languages'
    this.registerByEmail =
      '#register-form > div> label:nth-child(2) > input[value=email]'
    this.countinueButton = '#step-1-button'
    this.signUpLink = '#headerRegisterUrl'
    this.phoneNumber = '.register-form #phone'
    this.email = ".register-form #email[type='text']"
    this.userFirstName = '.register-form #name'
    this.userSurName = '.register-form #surname'
    this.termsandcondition = '.register-form #term'
    this.signUpButton = '#register-button.confirm'
    this.accountLink = '#account'
    this.loginUserButton = '#headerLoginUrl'
    this.confirmSignUpButton = "input[value='SIGN UP'].confirm"
  }

  enterRegistrationDetails = (method) => {
    click('click', 'element', this.registerByEmail)

    setInputField(method, faker.internet.email(), this.email)
    setInputField(method, faker.name.firstName(), this.userFirstName)
    setInputField(method, faker.name.lastName(), this.userSurName)
  }

  clickTermsAndCondition = () => {
    click('click', 'element', this.termsandcondition)
  }

  clickSignUpButton = () => {
    click('click', 'element', this.signUpButton)
  }

  clickConfirmSignUpButton = () => {
    click('click', 'element', this.confirmSignUpButton)
  }

  verifyAccountLink = () => {
    checkElementExist('an', this.accountLink)
  }
}

export default new RegistrationForm()
