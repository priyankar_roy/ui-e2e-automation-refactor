const orderSuccessPage = require('../pages/ordersuccesspage')
const assert = require('chai').assert


const yonetim = function(){

    const order_search_box = '//input[@id="SearchTextbox"]'
    const user_name = '//input[@name="username"]'
    const password = '//input[@name="password"]'
    const login_button = '//div[contains(@class, "submit button")]'
    const order_table = '//div[@id="dsag"]/table[contains(@class, "table-bordered")]'
    const transaction_records = '//div[@id="dsol"]//a[contains(text(),"İşlem Kayıtları")]'
    const yonetim_order_search_result= '//div[@id="SearchResultInnerContainer"]//div[@class="item clickable active"]'
    const order_reservation_message = '//div[@class="well"]//strong[contains(text(), "numaralı ürüne depoda bulunduğu için rezervasyon eklendi.")]'

    var expected_reservation_message = "numaralı ürüne depoda bulunduğu için rezervasyon eklendi."



    this.searchOrder = (orderID) => {
      var orderIDList = orderSuccessPage.captureOrderID()
      orderIDList.forEach((orderID) => {
        $(order_search_box).waitForDisplayed({ timeout: 30000 });
        $(order_search_box).setValue('@'+orderID)
        $(yonetim_order_search_result).waitForDisplayed({ timeout: 30000 });
        browser.keys("Enter")
        navigateToTransactionRecords()
        confirmModanisaOrderMessage()
      })

        // $(order_search_box).waitForDisplayed({ timeout: 30000 });
        // $(order_search_box).setValue('@'+orderID)
        // $(yonetim_order_search_result).waitForDisplayed({ timeout: 30000 });
        // browser.keys("Enter")
      }



      this.launchYonetim = (credsArray) => {
        var userID = Object.keys(credsArray);
        var pwd = Object.values(credsArray);
        browser.url(process.env.DESKTOP_YONETIM)
        $(user_name).setValue(userID)
        $(password).setValue(pwd)
        $(login_button).click()
      }

      this.navigateToTransactionRecords = () => {
        // browser.pause(5000)
        $(order_table).waitForDisplayed({ timeout: 30000 });
        if($(order_table).isDisplayed()){
          if ($(transaction_records).isDisplayed()){
            $(transaction_records).click()
          }
          else
          console.warn("Transaction records link is not displayed")       
        }
        else
        console.warn("Order table is not displayed")
      }

      this.confirmModanisaOrderMessage = () => {
        var resv_message = $(order_reservation_message).getText()
        assert.include(resv_message, expected_reservation_message)


      }
    
}
module.exports = new yonetim()
