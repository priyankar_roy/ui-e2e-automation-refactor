import clickelement from '../support/action/clickElement'
import getText from '../support/action/getTextFromElement'
import isDisplayed from '../support/check/isDisplayed'
import waitForUntil from '../support/check/waitForUntil'
import DeviceLib from '../libraries/Device'
import newAddress from '../pages/newAddressPage'
const checkout = require('../pages/checkout')
const expect = require('chai').expect
const assert = require('chai').assert

const OrderSuccesspage = function () {
  const continueToPaymentButton = '.continue-payment-button'
  const placeOrderButton = '#place-order-button'
  const termsButton = `//div[contains(@class,'checkout-summary')]//*[@for="contracts_checkbox"]`
  //'#terms-and-conditions'
  const thanksSection = '#thank-you'
  const thanksMessage = '#thankyou-message'
  const orderDescription = '#order-ship-message'
  const termsErrorMessage = '.v-messages__message'
  const shippmentDescription = '#thank-you > div:nth-child(4)'
  const myOrderLink = 'a#order-detail-link'
  const continueShoppingButton =
    '//button[contains(text(),"Continue Shopping")]' //'.continue-shopping-button'
  const paymentSummaryContainer = '//div[@id="payment-summary-container"]' //'#payment-summary-container' 2 results
  const subTotal = '#subtotal'
  const couponDiscount = '#discount'
  const shippingFee = '#shipping-fee'
  const total = '#total'
  const paymentMethodSection = '//div[@id="payment-method-container"]' // '#payment-method-container' 2 results
  const payemntType = '#payment-type'
  const cardDetails = '#card-detail'
  const shippingAddress = '//div[@id="shipping-address-container"]' // '#shipping-address-container' 2 results
  const customerName = '#customer-name'
  const customerAddress = '#customer-address'
  const customerPhoneNumber = '#customer-phone'
  const viewLink = '//a[text()="View"]'
  const ordersLink = '//a[starts-with(text()," Order #")]'
  const ordersuccessPageOld = '.successPageContainer'
  const orderGroup = '#order-group-container'
  const orderSplitInSuccessPage = '#order-group-container>div'
  const standardDelivery =
    '//div[@id="order-group-container"]//div[@id="supplier-name"]'
  const addAddressError =
    '//*[contains(text(),"Please add your address first.")]'
  const orderSuccessAddressName = '#customer-name'
  const orderSuccessAddressFullAddress = '#customer-address'
  const orderSuccessAddressPhone = '#customer-phone'
  const oldOrderSuccessPopup = '.successPageAppLinkPopup>button'
  const orderNumberOld_mobile = '.successSection-jumbotronMessage'
  const oldOrderSuccessPopup_mobile = '.successPageAppLinkPopup>button'

  let orderID = []

  this.clickCheckoutButtons = (action, buttonType) => {
    switch (buttonType) {
      case 'Continue to payment':
        browser.pause(15000)
        $(continueToPaymentButton).waitForDisplayed({ timeout: 30000 })
        clickelement(action, 'element', continueToPaymentButton)
        waitForUntil('#cardOwnerName')
        if ($(addAddressError).isDisplayed()) {
          clickelement(action, 'element', continueToPaymentButton)
        }
        //browser.saveScreenshot('screenshots/continuepayment.png')
        break
      case 'Terms and Condition':
        console.log('terms & cond')
        $(termsButton).waitForDisplayed({ timeout: 30000 })
        //browser.saveScreenshot('screenshots/termsandcondition.png')
        clickelement(action, 'element', termsButton)
        if ($(termsErrorMessage).isDisplayed()) {
          clickelement(action, 'element', termsButton)
        }
        break
      case 'Place order':
        $(placeOrderButton).waitForDisplayed({ timeout: 30000 })
        clickelement(action, 'element', placeOrderButton)
        break
      default:
        console.warn('Button type is not valid')
    }
  }

  this.validateThanksOrderSection = (messageArray) => {
    // waitForUntil(thanksSection)
    $(thanksSection).waitForDisplayed({ timeout: 30000 })
    //browser.saveScreenshot('screenshots/OrderSuccesspage.png')
    let count = 0
    let textElements = [thanksMessage, orderDescription, shippmentDescription]
    if (isDisplayed(thanksSection)) {
      messageArray.forEach((text) => {
        expect(text.toString()).to.be.equal($(textElements[count]).getText())
        count++
      })
    } else {
      console.warn('Order success page is not displayed')
    }
  }

  this.validateOrderSummarySection = () => {
    $(paymentSummaryContainer).waitForDisplayed({ timeout: 30000 })
    if (DeviceLib.isMobile()) {
      if (isDisplayed(paymentSummaryContainer)) {
        // validatePaymentValues need to implement for mobile
      } else {
        console.warn('i am not in payment summary')
      }
    } else {
      if (isDisplayed(paymentSummaryContainer)) {
        validatePaymentValues()
      } else {
        console.warn('i am not in payment summary')
      }
    }
  }

  this.validateOrderPaymentMethodSection = () => {
    $(paymentMethodSection).waitForDisplayed({ timeout: 30000 })

    if (DeviceLib.isMobile()) {
      if (isDisplayed(paymentMethodSection)) {
      } else {
        console.warn('Payment method section is not displayed')
      }
    } else {
      if (isDisplayed(paymentMethodSection)) {
        validatePaymentValues()
      } else {
        console.warn('Payment method section is not displayed')
      }
    }
  }

  let validatePaymentValues = () => {
    const orderPageValues = checkout.getBasketValues()
    expect(getText(subTotal)).to.be.equal(orderPageValues.subTotal)
    console.log(orderPageValues.couponDiscount + '4444444444')
    if (orderPageValues.couponDiscount == 'False') {
      console.log('**There is no coupon applied**')
    } else {
      expect(getText(couponDiscount).replace(/ +/g, '')).to.be.equal(
        orderPageValues.couponDiscount.replace(/ +/g, '')
      )
    }
    expect(getText(shippingFee)).to.be.equal(orderPageValues.shippingFee)
    expect(getText(total)).to.be.equal(orderPageValues.Total)
  }

  this.validatePaymentMethodSection = () => {
    if (isDisplayed(paymentMethodSection)) {
      expect(isDisplayed(payemntType)).to.be.true
      // expect(isDisplayed(cardDetails)).to.be.true currently card details is disabled
    }
  }

  this.shippingAddressSection = () => {
    if (isDisplayed(shippingAddress)) {
      expect(isDisplayed(customerName)).to.be.true
      expect(isDisplayed(customerAddress)).to.be.true
      expect(isDisplayed(customerPhoneNumber)).to.be.true
    }
  }

  this.estimatedDeliveryMethod = (delivery) => {
    if (isDisplayed(standardDelivery)) {
      // const elem = $(standardDelivery);
      assert.include($(standardDelivery).getText(), delivery)
    } else {
      console.warn('Estimated Delivery method is not displayed.')
    }
  }

  //to be implemented once integration done with legacy system
  let validateNavigationOfOrderLink = (selector) => {
    clickelement('click', 'element', selector)
    expect(browser.getUrl()).contains('/membership/account/orders/')
    browser.back()
  }

  let validateNavigationOfContinueShopping = (selector) => {
    clickelement('click', 'element', selector)
    expect(browser.getTitle()).to.be.equal(
      'Modanisa Hijab Fashion &amp; Modest Style Clothing: Jilbabs, Hijabs, Shawls, Abayas, and Scarves'
    )
    // expect(browser.getTitle()).to.be.equal('checkout')
  }

  this.selectLinkType = (linkType) => {
    console.log(linkType + ' Link types')
    switch (linkType.toString()) {
      case 'My Orders':
        validateNavigationOfOrderLink(myOrderLink)
        break
      case 'View':
        validateNavigationOfOrderLink(viewLink)
        break
      case 'OrderId':
        validateNavigationOfOrderLink(ordersLink)
        break
      case 'Continue shopping':
        validateNavigationOfContinueShopping(continueShoppingButton)
        break
      default:
        console.warn('Link type is not valid')
    }
  }

  this.validateOldCheckOutPage = () => {
    browser.pause(3000)
    console.warn('Checking final success page')
    if (DeviceLib.isMobile()) {
      if ($(oldOrderSuccessPopup).isDisplayed()) {
        clickelement('click', 'element', oldOrderSuccessPopup)
      }
    } else {
      if ($(oldOrderSuccessPopup).isDisplayed()) {
        clickelement('click', 'element', oldOrderSuccessPopup)
      }
      expect(isDisplayed(ordersuccessPageOld)).to.be.true
    }
  }

  this.captureOrderID = () => {
    browser.pause(5000)
    var orderCount = $$(ordersLink)
    orderCount.forEach((orderElement) => {
      let orderNumber = orderElement.getText()
      console.log(orderNumber)
      orderNumber = orderNumber.split('#')
      orderNumber = orderNumber[1]
      orderID.push(orderNumber)

      console.log(orderElement + 'orders')
    })

    console.log(orderID + 'Order Id Capture list')
    return orderID
  }

  this.captureOrderID_Old = () => {
    browser.pause(5000)
    var orderCount = $$(orderNumberOld_mobile)
    orderCount.forEach((orderElement) => {
      let orderNumber = orderElement.getText()
      orderNumber = orderNumber.split(':')
      orderNumber = orderNumber[1].trim()
      orderID.push(orderNumber)
    })
    console.log(' ORDER ID NUMBER', orderID)
    return orderID
  }

  this.getOrderID = () => {
    let orderid = this.captureOrderID() 
    return orderid
  }

  this.validateOrderGroup = (actualgroupCount) => {
    if ($(orderGroup).isDisplayed()) {
      let expectedGroupCount = $$(orderSplitInSuccessPage)
      console.log('Group count is ' + expectedGroupCount)
      expect(expectedGroupCount.length).to.be.equal(actualgroupCount)
    }
  }

  this.orderSuccessPageAddressValidation = () => {
    this.selectedAddress = {}
    this.selectedAddress = newAddress.selectedAddressInShippingPage
    $(orderSuccessAddressName).scrollIntoView()
    console.log(this.selectedAddress)
    expect($(orderSuccessAddressName).getText()).to.be.equal(
      this.selectedAddress.newCheckoutAddressName
    )
    expect(
      this.selectedAddress.newCheckoutAddressFullAddress.replace('\n', ' ')
    ).to.be.contains(
      $(orderSuccessAddressFullAddress).getText().replace(', Turkey', '')
    )
    expect($(orderSuccessAddressPhone).getText()).to.be.equal(
      this.selectedAddress.newCheckoutAddressPhone
    )
  }
}

module.exports = new OrderSuccesspage()
