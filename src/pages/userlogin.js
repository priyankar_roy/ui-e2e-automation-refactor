import clickElement from '../support/action/clickElement'
import openWebsite from '../support/action/openWebsite'
import isVisible from '../support/check/isDisplayed'
import switchFrame from '../support/action/switchFrame'
import setInputField from '../support/action/setInputField'
import DeviceLib from '../libraries/Device'
import selectLanguageFromWelcomePage from '../pages/welcome'
import clearValue from '../support/action/clearInputField'
import waitforload from '../support/lib/waitforScript'
import waitforScript from '../support/lib/waitforScript'
const expect = require('chai').expect

const userLoginFlow = function () {
  const guestAsLoginLink = '.continue-as-guest'
  const loginButton = '.login-form .confirm'
  const welcomePopupSection = '.welcomePopupInfoFullBackground-active'
  const confirmLocationButton = '.confirm'
  const countryInputBox = '.textbox-text'
  const discountClosePopup = 'div.element-close-button'
  const discountPopupframe = '.sp-fancybox-iframe'
  const selectCountryLink = '#lang_flag_container'
  const email = '.login-form #email'
  const password = '.login-form #password'
  const confirmButton = '#login-form .confirm'
  const loginUserButton = '#headerLoginUrl'
  const loginUser = '#hoveredHeaderLoginUrl'
  const emailButton =
    '#login-form > div.row.type-select-container > label:nth-child(2) > input'
  const signUpLink = '#headerRegisterUrl'
  const signUpButton = '#register-button'
  const accountMenu_Mobile = '#pageHeader-myaccountIcon'
  const closePopUpButton = '#welcomeContainer-closeButton'
  const loginLink_Mobile = '.accountMenuWelcomeSection-link'
  const loginWithEmailButton = 'label[for="loginPageTabHead-label-email"]'
  const loginWithPhoneButton = 'label[for="loginPageTabHead-label-phone"]'

  this.setGeoLocation = () => {
    browser.cdp('Emulation', 'setGeolocationOverride', {
      latitude: 39.0010756,
      longitude: 30.6770834,
    })
  }

  this.openWebsite = (type, environment) => {
    if (!environment) {
      console.warn('There is no environment match by given string')
      return
    }

    if (DeviceLib.isMobile()) {
      environment = `Mobile Web ${environment}`
    } else {
      environment = `Desktop ${environment}`
    }

    environment = environment.replace(/ /g, '_').toUpperCase()
    if (
      environment === 'DESKTOP_QA' ||
      environment === 'DESKTOP_STAGE' ||
      environment === 'DESKTOP_PROD' ||
      environment === 'DESKTOP_DEV'
    ) {
      browser.options.baseUrl = process.env[environment]
      DeviceLib.setURL(process.env[environment])
      openWebsite(type, '')
      selectLanguageFromWelcomePage.selectLanguageForDesktop('en')
    } else {
      browser.options.baseUrl = process.env[environment]
      DeviceLib.setURL(process.env[environment])
      openWebsite(type, '?hide_debug_panel=1')
    }
  }

  this.clickGuestLink = (method, logintype) => {
    if (logintype === 'Continue As Guest') {
      clickElement(method, 'element', guestAsLoginLink)
    } else if (logintype === 'Login') {
      clickElement(method, 'element', loginButton)
    }
  }

  this.acceptWelcomePopup = (country) => {
    console.log(DeviceLib.getUrl() + 'URL')
    if (DeviceLib.isMobile()) {
      try {
        clickElement('click', 'element', closePopUpButton)
        selectShippingCountry(country)
      } catch (error) {
        selectShippingCountry(country)
        console.log('No Welcome Pop Up For Mobil', error)
      }
    } else {
      if ($(welcomePopupSection).isExisting()) {
        verifyDiscountPopup()
        selectShippingCountry(country)
      } else {
        try {
          console.log('i am in else block for selecting contry')
          verifyDiscountPopup()
          clickElement('click', 'element', selectCountryLink)
          browser.pause(2000)
          selectShippingCountry(country)
        } catch (error) {
          console.log(error)
        }
      }
    }
  }

  let selectShippingCountry = (country) => {
    if (DeviceLib.isMobile()) {
      selectLanguageFromWelcomePage.selectLanguageForMobile('en', country)
    } else {
      console.log('insideShipppingChange')
      clickElement('click', 'element', countryInputBox)
      clickElement(
        'click',
        'element',
        `//*[@class="panel combo-p"]//div[contains(text(),"${country}")]`
      )
      browser.pause(2000)
      clickElement('click', 'element', confirmLocationButton)
      browser.pause(3000)
    }
  }

  this.verifyDiscountPopup = () => {
    try {
      if (discountPopupframe) {
        switchFrame(discountPopupframe)
        clickElement('click', 'element', discountClosePopup)
        browser.switchToParentFrame()
      }
    } catch (error) {
      console.log(error)
    }
  }

  let verifyDiscountPopup = () => {
    try {
      if (isVisible(discountPopupframe)) {
        switchFrame(discountPopupframe)
        clickElement('click', 'element', discountClosePopup)
        browser.switchToParentFrame()
      }
    } catch (error) {
      console.log(error)
    }
  }

  this.loginUserUrl = () => {
    clickElement('click', 'element', loginUserButton)
    clickElement('click', 'element', loginUser)
  }

  this.loginWithEmail = () => {
    const selector = DeviceLib.isMobile() ? mobileSelector() : emailButton
    clickElement('click', 'element', selector)

    function mobileSelector() {
      browser.pause(2000)
      clickElement('click', 'element', loginWithPhoneButton)
      let mobileselector
      return (mobileselector = `label[for="loginPageTabHead-label-email"]`)
    }
  }

  this.enterLoginDetails = (method, userEmail, userPassword) => {
    const emailInputValue = DeviceLib.isMobile()
      ? `#loginRegisterPage-formEmail`
      : email
    const passInputValue = DeviceLib.isMobile()
      ? `#loginRegisterPage-formPassword`
      : password
    clearValue(emailInputValue)
    setInputField(method, userEmail, emailInputValue)
    setInputField(method, userPassword, passInputValue)
  }

  this.clickLoginButton = () => {
    const selector = DeviceLib.isMobile()
      ? `#loginRegisterPage-formButtonSubmit`
      : confirmButton

    clickElement('click', 'element', selector)
  }

  this.verifySignupButton = () => {
    browser.pause(2000)
    expect(isVisible(signUpButton)).to.be.true
  }

  this.verifyLoginButton = () => {
    expect(isVisible(loginButton)).to.be.true
  }

  this.verifyContinueAsGuestLink = () => {
    expect(isVisible(guestAsLoginLink)).to.be.true
  }

  this.verifyContinueAsGuestLinkisNotVisible = () => {
    expect($(guestAsLoginLink).isDisplayed()).to.be.false
  }

  this.clickLoginOrSignUpLink = (method, type) => {
    if (type == 'Sign Up') {
      clickSignUpLink(method)
    } else {
      clickSignInLink(method)
    }
  }

  let clickSignUpLink = (method) => {
    if (DeviceLib.isMobile()) {
      openWebsite(null, 'membership/register/')
      return
    }
    clickElement(method, 'element', signUpLink)
  }

  let clickSignInLink = (method) => {
    if (DeviceLib.isMobile()) {
      clickElement(method, 'element', accountMenu_Mobile)
      clickElement(method, 'element', loginLink_Mobile)
      clickElement(method, 'element', loginWithEmailButton)
    } else {
      clickElement(method, 'element', loginUserButton)
    }
  }
}
module.exports = new userLoginFlow()
