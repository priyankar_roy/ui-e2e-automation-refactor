import clickElement from '../support/action/clickElement'
import setInputField from '../support/action/setInputField'
import selectOptionByIndex from '../support/action/selectOptionByIndex'
import isDisplayed from '../support/check/isDisplayed'
import focusLastOpenedWindow from '../support/action/focusLastOpenedWindow'
import waitForDisplayed from '../support/action/waitForDisplayed'
import Devices from '../libraries/Device'
import DeviceLib from '../libraries/Device'
const expect = require('chai').expect

const paymentOption = function () {
  const cashondelivery = '#checkoutSinglePagePaymentItemWrapper-cod'
  const ccName = '#checkoutAddress-cardOwnerNameInput'
  const ccNumber = '#checkoutAddress-cardNumberInput'
  const ccMonth = '#checkoutAddress-cardExpireDateMonthInput' //dropdown
  const ccYear = '#checkoutAddress-cardExpireDateYearInput' //dropdown
  const ccCVV = '#checkoutAddress-cardSecurityCodeInput'
  const ccNameOther = 'span.adyen-checkout__input-wrapper>input'
  const ccNumberOther = "//*[@id='encryptedCardNumber']"
  const ccMonthOther = "//*[@id='encryptedExpiryDate']"
  const ccCVVOther = "//*[@id='encryptedSecurityCode']"
  const termsAndConditions =
    '.checkoutSinglePageFooter-fixed .checkoutAddress-formController>div>label>div'
  const newCheckoutTAndC =
    '//div[contains(@class,"checkout-summary")]//div[@id="terms-and-conditions"]'
  const newCheckoutPayment = `//*[contains(text(),'Continue to Payment')]`
  // const newCheckoutPayment = '.continue-payment-button'
  const newPlaceOrder = '#place-order-button'
  const message = '.v-messages__message'
  const newTermsConditionPage = '.checkout-summary'
  const thanksSection = '#thank-you'
  const distanceSalesAgreement = '#distance_sales_agreement'
  const preliminaryInformation = '#preliminary_information'
  const oldDistanceSalesAgreement =
    "(//a[text()='Distance Sales Agreement'])[2]"
  const oldPreliminaryInformation = "(//a[text()='Preliminary Information'])[2]"
  const termsErrorOldPage = '.checkoutAddress-formError'
  const savedCardSection =
    '.checkoutSinglePagePaymentItemDetailAdyen-registerPaymentItem-isSelected'
  const termsErrorMessage =
    '//div[contains(@class,"payment-list-container")]/following-sibling::div//div[contains(text(),"Required field, click the checkbox")]'
  const orderSummaryHeader =
    '//div[@id="no-overlay"]//span[contains(text(),"Order Summary")]'
  const subTotalAmount = '//div[@id="subtotal"]'
  const totalAmount =
    '(//div[@id="non-overlay-total"]//div[@id="totalPrice"])[2]'
  const couponDiscountAmount = '//div[@id="couponCode"]'
  const discountAmount = '//div[@id="discount"]'
  const shippingFeeAmount = '//div[@id="shippingFee"]//div[2]'
  const sizeText = '//span[@id="size"]'
  const quantityText = '//span[@id="quantity"]'
  const productName = '//div[@id="product-info"]/span'
  const productDescription = '//div[@id="product-info"]/div'
  const supplierInfo = '//div[@id="supplier-info"]'
  const estimatdDeliveryDate = '//div[@id="estimated-delivery"]/span'
  const estimatedDeliveryHeader = '//div[@id="estimated-delivery"]'
  const cardHolderNameInAnimation = `.jp-card-name.jp-card-display`
  const cardAnimationNumber = `.jp-card-number.jp-card-display`
  const cardAnimationCVVnumber =
    '//div[@class="jp-card-back"]//div[contains(@class,"jp-card-cvc")]'
  const cardExpiryDate = `.jp-card-expiry.jp-card-display`
  const nameFieldError = `//input[@id='cardOwnerName']/ancestor::div[@class='v-input__slot']/following-sibling::div`
  const cardNumberFieldError = `//input[@id='cardNumber']/ancestor::div[@class='v-input__slot']/following-sibling::div`
  const expiryMonthFieldError = `//*[@id="cardExpireDateMonth_wrapper"]//div[contains(text(),"This field")]`
  const expiryYearFieldError = `//*[@id="cardExpireDateYear_wrapper"]//div[contains(text(),"This field")]`
  const cvvFieldError = `//*[@id="cardExpireDateYear_wrapper"]/following-sibling::div//div[contains(text(),"is")]`
  const codOption_mobile = '#checkoutSinglePagePaymentItemWrapper-cod'
  const termsAndConditions_mobile = '.checkoutAddress-formController'
  const backEndLabel = `//div[contains(@class,"backend_error_label")]`
  const basePrice = '#price-info .base-price-info'
  const discountPrice = '#price-info .discount-info'
  const paymentCardName = '#cardOwnerName'
  const paymentCardNumber = '#cardNumber'
  const paymentDatePicker = '#cardExpireDateMonth_wrapper'
  const paymentYearPicker = '#cardExpireDateYear_wrapper'
  const paymentCVV = '#cardSecurityCode'
  const cardDate =
    '//*[@class="v-menu__content theme--light menuable__content__active"]//div[12]'
  const cardYear =
    '//*[@class="v-menu__content theme--light menuable__content__active"]//div[2]'
  const saveCardCheckBox = '#saveMyCardInformation'
  const cardNickNameBox = `#cardNickName`
  const Secure3DCheckBox = '//*[@for="securePaymentCheckbox"]'
  const smsTextBox = '#smsCode'
  const cancelOTPPageMessage =
    '//*[contains(text(),"We could not fulfil the payment at the moment. Please try again later.")]'

  var paymentForm_Warnings = {
    cardHolderName: 'This field is required',
    cardNumber: 'This field is required',
    expiryMonth: 'This field is required',
    expiryYear: 'This field is required',
    cvvNumber: 'This field is required',
  }

  this.clickCODOption = () => {
    browser.pause(3000)
    if (Devices.isMobile) {
      browser.pause(2000)
      clickElement('click', 'element', codOption_mobile)
    } else {
      clickElement('click', 'element', cashondelivery)
    }
  }

  this.validateOrderSummaryDetails = () => {
    if ($(orderSummaryHeader).isDisplayed()) {
      expect(isDisplayed(estimatedDeliveryHeader)).to.be.true
      expect(isDisplayed(estimatdDeliveryDate)).to.be.true
      expect(isDisplayed(supplierInfo)).to.be.true
      expect(isDisplayed(productName)).to.be.true
      expect(isDisplayed(productDescription)).to.be.true
      expect(isDisplayed(sizeText)).to.be.true
      expect(isDisplayed(quantityText)).to.be.true
      expect(isDisplayed(subTotalAmount)).to.be.true
      expect(isDisplayed(shippingFeeAmount)).to.be.true
      expect(isDisplayed(totalAmount)).to.be.true
    }
  }

  const getBasketValues = () => {
    const basketValues = {
      subTotal: checkSubtotalValue(),
      couponDiscount: checkDiscountCouponValue(),
      shippingFee: $(shippingFeeAmount).getText(),
      Total: $(totalAmount).getText(),
    }
    expect(isDisplayed(shippingFeeAmount)).to.be.true
    expect(isDisplayed(totalAmount)).to.be.true
    return basketValues
  }

  let checkSubtotalValue = () => {
    let subTotalValue
    if ($(couponDiscountAmount).isDisplayed()) {
      expect(isDisplayed(subTotalAmount)).to.be.true
      subTotalValue = $(subTotalAmount).getText()
    } else {
      subTotalValue = $(totalAmount).getText()
    }
    return subTotalValue
  }
  let checkDiscountCouponValue = () => {
    let discountValue
    if ($(couponDiscountAmount).isDisplayed()) {
      expect(isDisplayed(discountAmount)).to.be.true
      discountValue = $(discountAmount).getText()
    } else {
      discountValue = '0,00 TL'
    }
    return discountValue
  }

  this.checkBasketOutValues = () => {
    let basketValues = Object.create(getBasketValues())
  }

  this.checkTermsAndConditions = () => {
    if (Devices.isMobile()) {
      clickElement('click', 'element', termsAndConditions_mobile)
    } else {
      clickElement('click', 'element', termsAndConditions)
      if ($(termsErrorOldPage).isDisplayed()) {
        clickElement('click', 'element', termsAndConditions)
      }
    }
  }

  this.otherCountryCreditCardDetails = (method) => {
    $(savedCardSection).waitForDisplayed({ timeout: 30000 })
    if ($(savedCardSection).isDisplayed()) {
      browser.switchToFrame($("//iframe[@class='js-iframe']"))
      setInputField(method, '000', ccCVVOther)
      browser.switchToParentFrame()
    } else {
      setInputField(method, 'Test Test', ccNameOther)
      browser.switchToFrame($("(//iframe[@class='js-iframe'])[1]"))
      setInputField('set', '4355084355084358', ccNumberOther)
      browser.switchToParentFrame()
      browser.pause(3000)
      browser.switchToFrame($("(//iframe[@class='js-iframe'])[2]"))
      setInputField('set', '1226', ccMonthOther)
      browser.switchToParentFrame()
      browser.switchToFrame($("(//iframe[@class='js-iframe'])[3]"))
      setInputField(method, '000', ccCVVOther)
      browser.switchToParentFrame()
    }
  }

  this.creditCardDetails = (method) => {
    setInputField(method, 'Test Test', ccName)
    setInputField(method, '4355084355084358', ccNumber)
    selectOptionByIndex('1st', ccMonth)
    selectOptionByIndex('1st', ccYear)
    setInputField(method, '000', ccCVV)
  }

  this.clickNewCheckoutPayment = () => {
    clickElement('click', 'element', newCheckoutPayment)
  }

  this.clickPlaceOrder = () => {
    clickElement('click', 'element', newPlaceOrder)
  }

  this.validationClickPlaceOrder = (orderPageType) => {
    if (orderPageType == 'successfull') {
      $(thanksSection).waitForDisplayed({ timeout: 30000 })
      expect(isDisplayed(thanksSection)).to.be.true
    } else if (orderPageType == 'payment') {
      expect(isDisplayed(newTermsConditionPage)).to.be.true
      expect(isDisplayed(message)).to.be.true
    } else console.warn('Wrong page is displayed')
  }

  this.checkNewTermsAndCondition = (confirmatonType) => {
    waitForDisplayed(paymentCardName, true)
    if (confirmatonType == 'accepts') {
      this.accectTermsAndContion()
    } else if (confirmatonType == 'rejects') {
      this.rejectsTermsAndCondition()
      expect(isDisplayed(message)).to.be.true
    } else if (confirmatonType == 'deselect') {
      clickElement('click', 'element', newCheckoutTAndC)
      browser.pause(2000)
      expect(isDisplayed(message)).to.be.true
      console.log('Required message is displayed after deselection')
    } else console.warn('Wrong page is displayed')
  }

  this.accectTermsAndContion = () => {
    clickElement('click', 'element', newCheckoutTAndC)
    if ($(termsErrorMessage).isDisplayed()) {
      clickElement('click', 'element', newCheckoutTAndC)
    }
  }

  this.rejectsTermsAndCondition = () => {
    clickElement('click', 'element', newCheckoutTAndC)
    if ($(termsErrorMessage).isDisplayed()) {
    } else {
      clickElement('click', 'element', newCheckoutTAndC)
    }
    clickElement('click', 'element', newPlaceOrder)
    browser.pause(2000)
  }

  this.navigateToNewAgreement = (linktype) => {
    switch (linktype) {
      case 'distance sale agreement':
        clickElement('click', 'element', distanceSalesAgreement)
        browser.pause(3000)
        break
      case 'preliminary information':
        clickElement('click', 'element', preliminaryInformation)
        break
      default:
        console.warn('Link is not valid')
    }
  }

  this.vaidateOldAgreementPage = (pageType) => {
    if (pageType == 'distance sales agreement') {
      focusLastOpenedWindow()
      expect('Modanisa').to.equals(browser.getTitle())
    } else if (pageType == 'preliminary information') {
      focusLastOpenedWindow()
      expect('Modanisa').to.equals(browser.getTitle())
    } else console.warn('we are on wrong page')
  }

  this.vaidateNewAgreementPage = (pageType) => {
    if (pageType == 'distance sales agreement') {
      focusLastOpenedWindow()
      expect('checkout').to.equals(browser.getTitle())
    } else if (pageType == 'preliminary information') {
      focusLastOpenedWindow()
      expect('checkout').to.equals(browser.getTitle())
    } else console.warn('we are on wrong page')
  }

  this.navigateToOldAgreement = (linktype) => {
    switch (linktype) {
      case 'distance sale agreement':
        clickElement('click', 'element', oldDistanceSalesAgreement)
        browser.pause(3000)
        break
      case 'preliminary information':
        clickElement('click', 'element', oldPreliminaryInformation)
        break
      default:
        console.warn('Link is not valid')
    }
  }

  //To DO - When dynamic data implemented in aggrements
  this.validateAgreementDetails = () => {}

  this.oldTermsAndConditionAgreement = () => {
    clickElement('click', 'element', oldDistanceSalesAgreement)
  }

  this.checkEstimatedDeliveryDate = () => {
    $(estimatedDeliveryHeader).waitForDisplayed({ timeout: 30000 })
    expect(isDisplayed(estimatdDeliveryDate)).to.be.true
  }

  this.checkProductDetails = () => {
    $(orderSummaryHeader).waitForDisplayed({ timeout: 30000 })
    expect(isDisplayed(productName)).to.be.true
    expect(isDisplayed(productDescription)).to.be.true
    expect(isDisplayed(sizeText)).to.be.true
    expect(isDisplayed(quantityText)).to.be.true
    expect(isDisplayed(totalAmount)).to.be.true
  }

  this.checkProductDetails = () => {
    $(orderSummaryHeader).waitForDisplayed({ timeout: 30000 })
    expect(isDisplayed(subTotalAmount)).to.be.true
  }

  this.checkForDiscountApplied = () => {
    $(orderSummaryHeader).waitForDisplayed({ timeout: 30000 })
    expect(isDisplayed(couponDiscountAmount)).to.be.true
  }

  this.checkForDiscountApplied = (discount_applied) => {
    var action = Object.keys(discount_applied)
    var discount_coupon = Object.values(discount_applied)
    for (let i = 0; i < action.length; i++) {
      let fields = []
      if (fields[i] == 'should see') {
        expect(isDisplayed(couponDiscountAmount)).to.be.true
        assert.equal($(couponDiscountAmount).getText(), discount_coupon[0])
      } else if (fields[i] === 'should not see') {
        expect(isDisplayed(couponDiscountAmount)).to.be.false
      }
    }
  }

  this.checkSubtotalSummary = () => {
    $(orderSummaryHeader).waitForDisplayed({ timeout: 30000 })
    expect(isDisplayed(totalAmount)).to.be.true
  }

  //new payment card details
  this.enterValidCardDetails = (cardNumber) => {
    browser.refresh()
    //browser.saveScreenshot('screenshots/paymentpage.png')
    $(paymentCardName).waitForDisplayed({ timeout: 30000 })
    setInputField('add', 'Tester', paymentCardName)
    setInputField('add', cardNumber, paymentCardNumber)
    clickElement('click', 'element', paymentDatePicker)
    clickElement('click', 'element', cardDate)
    clickElement('click', 'element', paymentYearPicker)
    clickElement('click', 'element', cardYear)
    setInputField('add', '123', paymentCVV)
    //setInputField('add', 'Test Card', cardNickNameBox)
  }

  this.enterCardDetailsAndVerifyIfReflectedInAnimation = (cardNumber) => {
    while ($(paymentCardName).isDisplayed() === false) {
      console.log('refreshing page')
      browser.refresh()
      $(paymentCardName).waitForDisplayed({ timeout: 10000 })
    }
    //browser.saveScreenshot('screenshots/paymentpage.png')
    setInputField('add', 'Test', paymentCardName)
    expect($(cardHolderNameInAnimation).getText()).to.be.equal('TEST')
    setInputField('add', cardNumber, paymentCardNumber)
    clickElement('click', 'element', paymentDatePicker)
    clickElement('click', 'element', cardDate)
    clickElement('click', 'element', paymentYearPicker)
    clickElement('click', 'element', cardYear)
    setInputField('add', '123', paymentCVV)
    browser.pause(5000)
    console.log('test', $(cardAnimationCVVnumber).getText())
    expect($(cardAnimationCVVnumber).getText()).to.be.equal(`123`)
    expect($(cardAnimationNumber).getText()).to.be.equal(cardNumber)
    expect($(cardExpiryDate).getText()).to.be.equal(`12/22`)
  }

  this.checkForIncorrectCardMessage = () => {
    expect($(cardNumberFieldError).getText()).to.equal(
      'Card number is incorrect'
    )
  }

  this.esenEntersCardNumber = (cardNumber) => {
    setInputField('add', cardNumber, paymentCardNumber)
  }

  this.esenEntersCVV = (cvvNumber) => {
    setInputField('add', cvvNumber, paymentCVV)
  }

  this.checkIfOnlyNonNumericCharactersAreAllowedForCreditcardNumbers = (
    textEntered,
    textVisible
  ) => {
    browser.refresh()

    while ($(paymentCardName).isDisplayed() === false) {
      console.log('refreshing page')
      browser.refresh()
      browser.pause(5000)
    }

    setInputField('add', textEntered, paymentCardNumber)

    clickElement('click', 'element', paymentDatePicker) //Need to click outside the cardNumber to get text

    browser.pause(2000)
    expect($(cardAnimationNumber).getText()).to.equal(textVisible)
    //browser.saveScreenshot('screenshots/paymentpage1.png')
  }

  this.checkIfOnlyNonNumericCharactersAreAllowedForCVVField = (
    textEntered,
    textVisible
  ) => {
    browser.refresh()
    while ($(paymentCardName).isDisplayed() === false) {
      console.log('refreshing page')
      browser.refresh()
      browser.pause(5000)
    }

    setInputField('add', textEntered, paymentCVV)

    clickElement('click', 'element', paymentDatePicker) //Need to click outside the cardNumber to get text

    browser.pause(2000)

    expect($(cardAnimationCVVnumber).getText()).to.equal(textVisible)

    //browser.saveScreenshot('screenshots/paymentpage2.png')
  }

  this.checkMessagesUnderFields = (field, errorMessage) => {
    switch (field) {
      case 'cardHolderName':
        expect($(nameFieldError).getText()).to.equal(errorMessage)
        break
      case 'cardNumber':
        expect($(cardNumberFieldError).getText()).to.equal(errorMessage)
        break
      case 'expiryMonth':
        expect($(expiryMonthFieldError).getText()).to.equal(errorMessage)
        break
      case 'expiryYear':
        expect($(expiryYearFieldError).getText()).to.equal(errorMessage)
        break
      case 'cvvNumber':
        expect($(cvvFieldError).getText()).to.equal(errorMessage)
        break
      default:
        console.log(field + ': field is not present')
    }
  }

  this.checkMessagesInLabel = (Message) => {
    expect($(backEndLabel).getText()).to.equal(Message)
  }

  this.checkForDiscountInOrders = (productCount) => {
    // let basePrices = $$(basePrice).length
    let discountPrices = $$(discountPrice).length
    // console.log(basePrices,discountPrices)
    expect(discountPrices).to.equal(productCount)
    // expect(basePrices).to.equal(productCount)
  }

  this.click3DSecureCheckBox = () => {
    clickElement('click', 'element', Secure3DCheckBox)
  }

  this.enterOTPDetails = (pinType) => {
    switch (pinType) {
      case 'correct':
        setInputField('add', '283126', smsTextBox)
        break
      case 'incorrect':
        setInputField('add', '123456', smsTextBox)
        break
    }
  }

  this.clickButtonOn3DSecurePage = (type) => {
    switch (type) {
      case 'Submit':
        clickElement('click', 'element', `//*[text()="${type}"]`)
        break
      case 'Cancel':
        clickElement('click', 'element', `//*[text()="${type}"]`)
        browser.pause(3000)
        expect($(cancelOTPPageMessage).isDisplayed()).to.be.true
        break
      default:
        console('Button type is undefined')
    }
  }

  this.errorMessageOnPaymentPage = () => {
    const expectedMessage =
      'We could not fulfil the payment at the moment. Please try again later.'
    expect($(cancelOTPPageMessage).getText()).to.equal(expectedMessage)
  }
}
module.exports = new paymentOption()
