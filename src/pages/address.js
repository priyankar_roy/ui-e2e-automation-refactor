import setInputField from '../support/action/setInputField'
import selectOptionByIndex from '../support/action/selectOptionByIndex'
import clickElement from '../support/action/clickElement'
import selectOption from '../support/action/selectOption'
import faker from 'faker'
import DeviceLib from '../libraries/Device'
import isDisplayed from '../support/check/isDisplayed'
import moveTo from '../support/action/moveTo'
import openWebsite from '../support/action/openWebsite'
import waitForUntil from '../support/check/waitForUntil'
const expect = require('chai').expect

const address = function () {
  const firstName = '#deliveryPersonName'
  const surName = '#deliveryPersonSurname'
  const phoneNumber = '#phone'
  const email = '#email'
  const address = '#address'
  const city = '#checkout_address_city'
  const district = '.state-select'
  const postalCode = '#zip'
  const giftNote = '#giftNote'
  const saveButton = '.confirm'
  const existingBackButton = '//a[contains(text(),"Back")]'
  const addNewAddressButton = '#addressFormOpener'
  const loginAddressField = '.checkoutAddress-formTextarea'
  const loginCityDropdown = '#checkoutAddress-cityIdInput'
  const loginDistrictDropdown = '#checkoutAddress-stateInput'
  const loginPostalCode = '#checkoutAddress-zipInput'
  const loginSaveButton = '.checkoutAddressPageFooter-button'
  const countryCode = '.flag-dropdown'
  const selectTurkeyCountry = `//li//span[contains(text(),"+90")]`
  const deleteAddress = '#deleteAddressButton'
  const updateAddress = '//a[text()="Update"]'
  const registeredFirstName = '//input[@id="ship_address_delivery_person_name"]'
  const registeredSurName =
    '//input[@id="ship_address_delivery_person_surname"]'
  const updateSaveAddress = '//input[@class="update-btn"]'
  const shopmoreAddress = '#address-box-container > div:nth-child(1) > div.info'
  const addressTitle = '//input[@id="ship_address_title"]'
  const addressCompactBox = '.address-box'
  const deleteAddress_Mobile =
    '//a[contains(text(),"Delete") or contains(text(),"Sil")]'
  //const addressCompactBox = '.address-box.compact'
  const addressCompactBox_Mobile = '//div[@class="addressItem-container"]'
  const countyCodeInputForTurkey =
    '#checkoutAddress-countryNameInput[value = "Turkey"]'
  const cityInputForOtherCountries = '#checkoutAddress-cityNameInput'
  const postCodeForOtherCountries = '#checkoutAddress-zipInput'
  const registeredAddress = '.hover-registered-address'
  const addressWarning =
    '//div[contains(text(),"Please add your address first.")]'
  const noAddressMessage =
    '//p[contains(text(),"You currently have no saved addresses.")]'
  const titleOfFirstRegisteredAddress =
    '//div[@class="address-box compact"][1]//strong'
  const firstRegisteredAddress = '.address-box'

  this.enterAddressdetails = (method) => {
    setInputField(method, faker.name.firstName(), firstName)
    setInputField(method, faker.name.lastName(), surName)
    setInputField(method, faker.phone.phoneNumber('555#######'), phoneNumber)
    setInputField(method, faker.internet.email(), email)
    setInputField(method, faker.address.streetAddress(20), address)
    selectOptionByIndex(1, city)
    browser.pause(3000)
    selectOption('text', 'Akyurt', district)
    setInputField(method, faker.address.zipCode(), postalCode)
    setInputField(method, faker.random.word(), giftNote)
  }

  this.enterAddressDetailsForLogin = (method) => {
    // changeContryCode()
    setInputField(method, faker.address.streetAddress(20), loginAddressField)
    if ($(countyCodeInputForTurkey).isExisting()) {
      selectOption('text', 'Ankara', loginCityDropdown)
      browser.pause(3000)
      selectOptionByIndex(1, loginDistrictDropdown)
      setInputField(method, faker.address.zipCode(), loginPostalCode)
    } else {
      console.log('In other country')
      // $('#checkoutAddress-mobilePhoneInput').clearValue()
      // setInputField(
      //   method,
      //   faker.phone.phoneNumber('412######'),
      //   '#checkoutAddress-mobilePhoneInput'
      // ) //phone number specific to australia
      setInputField(method, 'Non-TurkishCity', cityInputForOtherCountries)
      setInputField(method, faker.address.zipCode(), postCodeForOtherCountries)
      browser.pause(5000)
    }
  }

  let changeContryCode = () => {
    clickElement('click', 'element', countryCode)
    clickElement('click', 'element', selectTurkeyCountry)
  }

  this.clickSaveButtonForLogin = () => {
    clickElement('click', 'element', loginSaveButton)
  }

  this.clickSaveButton = () => {
    clickElement('click', 'element', saveButton)
    waitForUntil('.checkoutAddressItem-selectArea')
  }

  this.clickNewAddressButton = (method) => {
    console.log('INside  click method')
    browser.pause(5000)
    clickElement('click', 'element', addNewAddressButton)
    browser.pause(5000)
    // browser.pause(10000)
    console.log('true' + $(firstName).isExisting())
  }
  this.clickRegisteredAddress = () => {
    clickElement('click', 'element', registeredAddress)
  }

  this.navigateToRegisteredAddress = () => {
    if (DeviceLib.isMobile()) {
      browser.url(`${DeviceLib.getUrl()}membership/account/addresses/`)
      browser.pause(6000)
    } else {
      browser.url(`${DeviceLib.getUrl()}membership/account/addresses/`)
      browser.pause(6000)
    }
  }

  this.removeAllSavedAddress = () => {
    if (DeviceLib.isMobile()) {
      console.log('insideremove')
      while ($(addressCompactBox_Mobile).isExisting()) {
        console.log('insidewhile')
        clickElement('click', 'element', deleteAddress_Mobile)
        browser.pause(2000)
        if ($(addressCompactBox_Mobile).isExisting() == false) {
          console.log('No registered addresses')
          break
        }
      }
    } else {
      while ($(shopmoreAddress).isExisting()) {
        $(addressCompactBox).moveTo()
        clickElement('click', 'element', deleteAddress)
        browser.pause(2000)
        if ($(addressCompactBox).isExisting() == false) {
          console.log('No registered addresses')
          break
        }
      }
    }
  }

  this.oldAddressPage = () => {
    if ($(noAddressMessage).isDisplayed()) {
      expect($(addressWarning).isDisplayed()).to.be.true
      console.log('Error is displayed')
    } else {
      expect($(addressWarning).isDisplayed()).to.be.false
      console.log('No error is displayed')
    }
  }

  let registeredName

  this.updateFirstRegisteredAddressName = () => {
    let firstName = faker.name.firstName()
    let surName = faker.name.lastName()
    $(firstRegisteredAddress).moveTo()
    $(updateAddress).waitForDisplayed({ timeout: 30000 })
    clickElement('click', 'element', updateAddress)
    $(addressTitle).waitForDisplayed({ timeout: 30000 })
    $(registeredFirstName).setValue(firstName)
    $(registeredSurName).setValue(surName)
    browser.pause(2000)
    clickElement('click', 'element', updateSaveAddress)
    this.registeredName = firstName + ' ' + surName
    return firstName + ' ' + surName
  }

  this.getAddressTitleOfFirstRegisteredAddress = () => {
    let title
    title = $(titleOfFirstRegisteredAddress).getText()
    return title
  }

  this.fillNewAddressFormInOldCheckout = () => {
    console.log($('.checkoutAddress-formContainer').isExisting())

    browser.pause(3000)
  }
}

module.exports = new address()
