import moment from 'moment'

const DEFAULT_FORMAT = 'YYYY-MM-DD HH:mm:ss'

class Timer {
  getDate(format = DEFAULT_FORMAT, { subtract = 0, addtract = 0 } = {}) {
    if (subtract) {
      return moment().subtract(subtract, 'days').format(format)
    } else if (addtract) {
      return moment().add(addtract, 'days').format(format)
    }

    return moment().format(format)
  }

  getDateWithPlaceHolder(str, format = DEFAULT_FORMAT) {
    const regex = /\*\|CURRENT_TIME\|\*\s*([+|-])*\s*(\d+)?/g
    const options = { subtract: 0, addtract: 0 }
    const matched = regex.exec(str)

    if (matched.length) {
      if (matched[1] === '-') {
        options.subtract = matched[2]
      } else if (matched[1] === '+') {
        options.addtract = matched[2]
      }
    }

    return str.replace(regex, this.getDate(format, options))
  }

  getCurrentDate(format = DEFAULT_FORMAT) {
    return moment().format(format)
  }
}

export default new Timer()
