class Device {
  constructor() {
    this.device = 'desktop'
    this.url = ''
  }

  setDevice(newDevice) {
    if (
      newDevice.toLowerCase() !== 'mobileweb' &&
      newDevice.toLowerCase() !== 'desktop'
    ) {
      return false
    }

    this.device = newDevice.toLowerCase()
    return this.device
  }

  setURL(URL) {
    this.url = URL
  }

  isMobile() {
    return this.device === 'mobileweb'
  }

  isDesktop() {
    return this.device === 'desktop'
  }

  getUrl() {
    return this.url
  }
}

const device = new Device()

export default device
