module.exports = {
  plugins: [
    "@babel/plugin-proposal-export-default-from",
    "@babel/plugin-syntax-class-properties",
  ],
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current",
        },
      },
    ],
  ],
};
